class Description {
  static const String aboutChannelAbbrev =
      'The channel abbreviation will be used to make your channel thumbnail and notification thumbnails when the thumbnail image fails to load.';
  static const String aboutChannelDescription =
      'Describe your channel to users.';
  static const String aboutChannelNotes =
      'Notes is a space for your developer notes. Users will never see these notes.';
  static const String aboutChannelSubscriptionPreview =
      'This channel will look roughly like this in the app. CHannel colors are used for the theme.';
  static const String aboutChannelSubscriptionUrl =
      'Users follow this URL to subscribe to this channel and its network. After subscribing, they will be able to view, subscribe, and unsubscribe from each channel within the network. This feature is most beneficial to networks with many channels';

  static const String aboutNetworkAbbrev =
      'The network abbreviation will be used to make your network thumbnail when the thumbnail image fails to load.';
  static const String aboutNetworkDescription =
      'Describe your network to users.';
  static const String aboutNetworkNotes =
      'Notes is a space for your developer notes. Users will never see these notes.';
  static const String aboutNetworkSubscriptionPreview =
      'This network will look roughly like this in the app. Network colors are used for the theme.';
  static const String aboutNetworkSubscriptionUrl =
      'Users follow this URL to subscribe to this network. After subscribing, they will be able to subscribe and unsubscribe from each channel.';

  static const String aboutStatPreview =
      'The static notification will look roughly like this in the app. Channel colors are used for the theme. You can use some html tags in the body such as breakline <br /> and bold <b></b>. Please test your notification formatting before sending to a large audience.';
}
