class EndpointsV1 {
  static const String developerPrivacy = 'developer/privacy.latest';
  static const String developerTerms = 'developer/terms.latest';
  static const String surferPrivacy = 'surfer/privacy.latest';

  static const String developerJwtRefresh = 'v1/developer/jwt.refresh';
  static const String surferJwtRefresh = 'v1/surfer/jwt.refresh';

  static const String developerEmailVerify = 'v1/developer/email.verify';
  static const String surferEmailVerify = 'v1/surfer/email.verify';

  static const String billGetById = 'v1/bill/get.byId';
  static const String billsGetByProjectId = 'v1/bills/get.byProjectId';

  static const String chargeGetById = 'v1/charge/get.byId';
  static const String chargesGetByBillId = 'v1/charges/get.byBillId';

  static const String devNotificationSend = 'v1/dev/notification/send';

  static const String flagBoi = 'v1/flag/boi';
  static const String plog = 'v1/plog';

  static const String boiGetById = 'v1/boi/get.byId';
  static const String boisGetByNetworkId = 'v1/bois/get.byNetworkId';
  static const String boisGetByNetworkIdAndPeriod =
      'v1/bois/get.byNetworkIdAndPeriod';
  static const String boisGetByProjectIdAndPeriod =
      'v1/bois/get.byProjectIdAndPeriod';
  static const String boiHide = 'v1/boi/hide';
  static const String boiUploadThumb = 'v1/boi/upload.thumb';

  static const String channelChangeAbbrev = 'v1/channel/change.abbrev';
  static const String channelChangeColorPrimary =
      'v1/channel/change.colorPrimary';
  static const String channelChangeColorSecondary =
      'v1/channel/change.colorSecondary';
  static const String channelChangeDescription =
      'v1/channel/change.description';
  static const String channelChangeName = 'v1/channel/change.name';
  static const String channelChangeNotes = 'v1/channel/change.notes';
  static const String channelChangeRoute = 'v1/channel/change.route';
  static const String channelChangeThumb = 'v1/channel/change.thumb';
  static const String channelCreate = 'v1/channel/create';
  static const String channelGetById = 'v1/channel/get.byId';
  static const String channelGetByRoute = 'v1/channel/get.byRoute';
  static const String channelsGetByNetworkId = 'v1/channels/get.byNetworkId';

  static const String channelChangeStaticAbbrev =
      'v1/channel/change.staticAbbrev';
  static const String channelChangeStaticBody = 'v1/channel/change.staticBody';
  static const String channelChangeStaticThumbUrl =
      'v1/channel/change.staticThumbUrl';
  static const String channelChangeStaticTitle =
      'v1/channel/change.staticTitle';

  static const String channelTokensGetByChannelId =
      'v1/channelTokens/get.byChannelId';
  static const String channelTokenCreate = 'v1/channelToken/create';
  static const String channelTokenDelete = 'v1/channelToken/delete';

  static const String developerChangeEmail = 'v1/developer/change.email';
  static const String developerChangeEmailRequest =
      'v1/developer/change.email.request';
  static const String developerChangeEmailRequestValidate =
      'v1/developer/change.email.request.validate';
  static const String developerChangeEmailValidateNewLink =
      'v1/developer/change.email.validate.new.link';
  static const String developerChangePassword = 'v1/developer/change.password';
  static const String developerChangePasswordRequest =
      'v1/developer/change.password.request';
  static const String developerChangePasswordRequestValidate =
      'v1/developer/change.password.request.validate';
  static const String developerChangePhone = 'v1/developer/change.phone';

  static const String developerGetById = 'v1/developer/get.byId';
  static const String developerRegisterWithEmail =
      'v1/developer/register.withEmail';
  static const String developerSignInWithApple =
      'v1/developer/signIn.withApple';
  static const String developerSignInWithEmail =
      'v1/developer/signIn.withEmail';
  static const String developerSignInWithGoogle =
      'v1/developer/signIn.withGoogle';
  static const String developerSignOut = 'v1/developer/signOut';

  static const String networkChangeAbbrev = 'v1/network/change.abbrev';
  static const String networkChangeColorPrimary =
      'v1/network/change.colorPrimary';
  static const String networkChangeColorSecondary =
      'v1/network/change.colorSecondary';
  static const String networkChangeDescription =
      'v1/network/change.description';
  static const String networkChangeName = 'v1/network/change.name';
  static const String networkChangeNotes = 'v1/network/change.notes';
  static const String networkChangeRoute = 'v1/network/change.route';
  static const String networkChangeThumb = 'v1/network/change.thumb';
  static const String networkCreate = 'v1/network/create';
  static const String networkGetById = 'v1/network/get.byId';
  static const String networkGetByRoute = 'v1/network/get.byRoute';
  static const String networksGetByProjectId = 'v1/networks/get.byProjectId';

  static const String networkTokensGetByNetworkId =
      'v1/networkTokens/get.byNetworkId';
  static const String networkTokenCreate = 'v1/networkToken/create';
  static const String networkTokenDelete = 'v1/networkToken/delete';

  static const String paymentGetById = 'v1/payment/get.byId';
  static const String paymentsGetSuccessfulByProjectId =
      'v1/payments/get.successful.byProjectId';
  static const String paymentsGetCcAttemptsByProjectId =
      'v1/payments/get.ccAttempts.byProjectId';

  static const String paymentCheckoutCrypto = 'payment/checkout.crypto';
  static const String paymentCheckoutStripe = 'payment/checkout.stripe';

  static const String paymentAttemptStatusGetAndHandle =
      'v1/paymentAttempt/status.getAndHandle';

  static const String planChangeGetCredit = 'v1/plan/change.getCredit';
  static const String planGetById = 'v1/plan/get.byId';
  static const String plansGetChoices = 'v1/plans/get.choices';

  static const String projectPlasticAdd = 'v1/project/plastic.add';
  static const String projectPlasticDelete = 'v1/project/plastic.delete';
  static const String projectChangeAutopay = 'v1/project/change.autopay';
  static const String projectChangeName = 'v1/project/change.name';
  static const String projectChangeNotes = 'v1/project/change.notes';
  static const String projectCreate = 'v1/project/create';
  static const String projectChangeDefaultPlasticId =
      'v1/project/change.defaultPlasticId';
  static const String projectGetById = 'v1/project/get.byId';
  static const String projectPlanIdNextChange = 'v1/project/planIdNext.change';
  static const String projectPlanIdNextCancel = 'v1/project/planIdNext.cancel';
  static const String projectsGetByDeveloperId =
      'v1/projects/get.byDeveloperId';

  static const String statChangeAbbrev = 'v1/stat/change.abbrev';
  static const String statChangeBody = 'v1/stat/change.body';
  static const String statChangeThumbUrl = 'v1/stat/change.thumbUrl';
  static const String statChangeTitle = 'v1/stat/change.title';
  static const String statChangeUrl = 'v1/stat/change.url';

  static const String statTokenBuy = 'v1/stat/token.buy';
  static const String statTokenConsume = 'v1/stat/token.consume';
  static const String statTokenStatusByProjectAndPeriod =
      'v1/stat/token.status.byProjectAndPeriod';
  static const String statTokenStatusByChannelAndPeriod =
      'v1/stat/token.status.byChannelAndPeriod';

  static const String stripePay = 'v1/stripe/pay';

  /*
   * Mobile
   */
  static const String channelSubsByNetworkSubId =
      'v1/channelSubs/get.byNetworkSubId';
  static const String networkSubsGetByInstanceIdAndSurferId =
      'v1/networkSubs/get.byInstanceIdAndSurferId';
  static const String channelSubSettingsSaveToRemote =
      'v1/channelSub/settings.saveToRemote';

  static const String channelSubSubscribe = 'v1/channelSub/subscribe';
  static const String channelSubUnsubscribe = 'v1/channelSub/unsubscribe';

  static const String networkSubUnsubscribe = 'v1/networkSub/unsubscribe';

  static const String channelSubAddNew = 'v1/channelSub/add.new';
  static const String channelSubGetByIds = 'v1/channelSub/get.byIds';
  static const String networkSubAddNew = 'v1/networkSub/add.new';
  static const String networkSubGetById = 'v1/networkSub/get.byId';
  static const String networkSubSettingsSaveToRemote =
      'v1/networkSub/settings.saveToRemote';

  static const String boiMetricReport = 'v1/boi/metric.report';

  static const String boisGetByChannelId = 'v1/bois/get.byChannelId';

  static const String surferChangeEmail = 'v1/surfer/change.email';
  static const String surferChangeEmailRequest =
      'v1/surfer/change.email.request';
  static const String surferChangeEmailRequestValidate =
      'v1/surfer/change.email.request.validate';
  static const String surferChangeEmailValidateNewLink =
      'v1/surfer/change.email.validate.new.link';
  static const String surferDelete = 'v1/surfer/delete';

  static const String surferGetById = 'v1/surfer/get.byId';
  static const String surferRegisterWithEmail = 'v1/surfer/register.withEmail';
  static const String surferSignInWithApple = 'v1/surfer/signIn.withApple';
  static const String surferSignInWithEmail = 'v1/surfer/signIn.withEmail';
  static const String surferSignInWithGoogle = 'v1/surfer/signIn.withGoogle';
  static const String surferSignOut = 'v1/surfer/signOut';

  static const String surferChangePassword = 'v1/surfer/change.password';
  static const String surferChangePasswordRequest =
      'v1/surfer/change.password.request';
  static const String surferChangePasswordRequestValidate =
      'v1/surfer/change.password.request.validate';

  static makeNetworkRouteUrl(String route) {
    return 'v1/s/$route';
  }
}
