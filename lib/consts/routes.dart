class Routes {
  static const String empty = '';
  static const String home = '/';
  static const String homeWord = '/home';
  static const String error = 'error';

  static const String account = 'account';
  static const String billing = 'billing';
  static const String console = 'console';
  static const String alerts = 'alerts';
  static const String flag = 'flag';
  static const String contact = 'contact-us';

  static const String developerPrivacy = 'developer/privacy.latest';
  static const String developerTerms = 'developer/terms.latest';
  static const String surferPrivacy = 'user/privacy.latest';

  static const String confirmEmail = 'confirmEmail';
  static const String developerEmailConfirmLink =
      'developer/email.confirm.link';
  static const String surferEmailConfirmLink = 'surfer/email.confirm.link';

  static const String boiChangeChannel = 'boi/change.channel';
  static const String boiChangeThumbUrl = 'boi/change.thumbUrl';
  static const String boiLayoutDescriptions = 'boi/layout.descriptions';
  static const String boiSend = 'boi/send';
  static const String boiStatus = 'boi/status';

  static const String channelChangeAbbrev = 'channel/change.abbrev';
  static const String channelChangeColorPrimary = 'channel/change.colorPrimary';
  static const String channelChangeColorSecondary =
      'channel/change.colorSecondary';
  static const String channelChangeDescription = 'channel/change.description';
  static const String channelChangeName = 'channel/change.name';
  static const String channelChangeNotes = 'channel/change.notes';
  static const String channelChangeRoute = 'channel/change.route';
  static const String channelChangeThumb = 'channel/change.thumb';
  static const String channelCreate = 'channel/create';
  static const String channelEdit = 'channel/edit';
  static const String channelOverview = 'channel/overview';
  static const String channelTokensOverview = 'channelTokens/overview';

  static const String developer = 'developer';
  static const String developerChangeEmailRequest =
      'developer/change.email.request';
  static const String developerChangeEmailLink = 'developer/change.email.link';
  static const String developerChangeEmailValidateNewLink =
      'developer/change.email.validate.new.link';
  static const String developerChangeLanguage = 'developer/changeLanguage';
  static const String developerChangePasswordRequest =
      'developer/change.password.request';
  static const String developerChangePasswordLink =
      'developer/change.password.link';
  static const String loggedOutDeveloperChangePasswordRequest =
      'loggedOutDeveloper/change.password.request';
  static const String developerChangePhone = 'developer/change.phone';

  static const String networkChangeAbbrev = 'network/change.abbrev';
  static const String networkChangeColorPrimary = 'network/change.colorPrimary';
  static const String networkChangeColorSecondary =
      'network/change.colorSecondary';
  static const String networkChangeDescription = 'network/change.description';
  static const String networkChangeName = 'network/change.name';
  static const String networkChangeNotes = 'network/change.notes';
  static const String networkChangeRoute = 'network/change.route';
  static const String networkChangeThumb = 'network/change.thumb';
  static const String networkCreate = 'network/create';
  static const String networkEdit = 'network/edit';
  static const String networkMetrics = 'network/metrics';
  static const String networkOverview = 'network/overview';
  static const String networkTokensOverview = 'networkTokens/overview';

  static const String paymentAddCard = 'payment/add.card';
  static const String paymentAddCredit = 'payment/add.credit';
  static const String paymentAutopay = 'payment/autopay';
  static const String paymentChangeOverage = 'payment/change.overage';
  static const String paymentCheckoutConfirm = 'payment/checkout.confirm';
  static const String paymentCheckoutCrypto = 'payment/checkout.crypto';
  static const String paymentCheckoutStripe = 'payment/checkout.stripe';
  static const String paymentCryptoAddress = 'payment/cryptoAddress';
  static const String paymentHistory = 'payment/history';
  static const String paymentMethods = 'payment/methods';

  static const String billOverview = 'bill/overview';
  static const String billPayNow = 'bill/payNow';
  static const String billingActivity = 'billingActivity';
  static const String billsView = 'billsView';
  static const String paymentsView = 'paymentsView';

  static const String paymentFailed = 'payment/failed';
  static const String paymentThankYou = 'payment/thankYou';
  static const String paymentWaitForStripe = 'payment/waitForStripe';

  static const String planChange = 'plan/change';
  static const String projectChangeName = 'project/change.name';
  static const String projectChangeNotes = 'project/change.notes';

  static const String projectCreate = 'project/create';
  static const String projectAlertsTab = 'project/alertsTab';
  static const String projectMetricsTab = 'project/metricsTab';
  static const String projectNetworksTab = 'project/networksTab';
  static const String projectPaymentsTab = 'project/paymentsTab';
  static const String projectSettingsTab = 'project/settingsTab';
  static const String projectStatementsTab = 'project/statementsTab';
  static const String projectPlanTab = 'project/planTab';

  static const String statChangeBody = 'stat/change.body';
  static const String statChangeThumbUrl = 'stat/change.thumbUrl';
  static const String statChangeTitle = 'stat/change.title';
  static const String statChangeUrl = 'stat/change.url';
  static const String statEdit = 'stat/edit';
  static const String statTokenBuy = 'stat/token.buy';
  static const String statTokenSpend = 'stat/token.spend';

  static const String signIn = 'signIn';

  /*
   * Mobile
   */
  static const String boiShow = 'boi/show';

  static const String help = 'help';
  static const String warnings = 'warnings';

  static const String channelSubAdd = 'channelSub/add';
  static const String channelSubSettings = 'channelSub/settings';
  static const String channelSubOverview = 'channelSub/overview';

  static const String networkSubAdd = 'networkSub/add';
  static const String networkSubSettings = 'networkSub/settings';
  static const String networkSubOverview = 'networkSub/overview';

  static const String surfer = 'surfer';
  static const String surferChangeEmailRequest = 'surfer/change.email.request';
  static const String surferChangeEmailLink = 'surfer/change.email.link';
  static const String surferChangeEmailValidateNewLink =
      'surfer/change.email.validate.new.link';
  static const String surferChangeLanguage = 'surfer/changeLanguage';
  static const String surferChangePasswordRequest =
      'surfer/change.password.request';
  static const String surferChangePasswordLink = 'surfer/change.password.link';
  static const String loggedOutSurferChangePasswordRequest =
      'loggedOutSurfer/change.password.request';

  static const String surferConfirmDeleteAccount =
      'surfer/confirm.delete.account';
}
