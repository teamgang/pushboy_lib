enum SoundResource {
  bigKiss,
  littleKiss,
  manEating,
  manStretching,
  none,
}

SoundResource soundResourceFromString(String input) {
  switch (input) {
    case 'bigKiss':
      return SoundResource.bigKiss;
    case 'littleKiss':
      return SoundResource.littleKiss;
    case 'manEating':
      return SoundResource.manEating;
    case 'manStretching':
      return SoundResource.manStretching;
    default:
      return SoundResource.none;
  }
}

extension SoundResourceExtensions on SoundResource {
  String get displayName {
    switch (this) {
      case SoundResource.bigKiss:
        return 'Big Kiss';
      case SoundResource.littleKiss:
        return 'Little Kiss';
      case SoundResource.manEating:
        return 'Man Eating';
      case SoundResource.manStretching:
        return 'Man Stretching';
      case SoundResource.none:
      default:
        return 'No sound selected';
    }
  }

  String get path {
    switch (this) {
      case SoundResource.bigKiss:
        return 'resource://raw/res_big_kiss';
      case SoundResource.littleKiss:
        return 'resource://raw/res_little_kiss';
      case SoundResource.manEating:
        return 'resource://raw/res_man_eating';
      case SoundResource.manStretching:
        return 'resource://raw/res_man_stretching';
      case SoundResource.none:
        return '';
    }
  }

  String get assetPath {
    switch (this) {
      case SoundResource.bigKiss:
        return 'assets/sounds/res_big_kiss.mp3';
      case SoundResource.littleKiss:
        return 'assets/sounds/res_little_kiss.mp3';
      case SoundResource.manEating:
        return 'assets/sounds/res_man_eating.mp3';
      case SoundResource.manStretching:
        return 'assets/sounds/res_man_stretching.mp3';
      case SoundResource.none:
        return '';
    }
  }
}
