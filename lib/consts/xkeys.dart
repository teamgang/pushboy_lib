class XKeys {
  static const String backButton = 'backButton';
  static const String checkButton = 'checkButton';
  static const String closeButton = 'closeButton';

  static const String ccFormCvvField = 'ccForm/cvvField';
  static const String ccFormExpirationFieldMm = 'ccForm/expirationFieldMm';
  static const String ccFormExpirationFieldDd = 'ccForm/expirationFieldDd';
  static const String ccFormNumberField = 'ccForm/numberField';

  static const String authFormRegisterTabButton = 'authForm/registerTab';
  static const String authFormSignInTabButton = 'authForm/signInTabButton';
  static const String authFormRegisterOrSignInButton =
      'authFormRegisterOrSignInButton';
  static const String authFormResetPasswordButton =
      'authFormResetPasswordButton';
  static const String authFormEmailField = 'authForm/emailField';
  static const String authFormPasswordField = 'authForm/passwordField';

  static const String developerOptionsColumn = 'developerOptionsColumn';

  /*
   * Mobile
   */
  static const String addSubscriptionLinkField = 'addSubscriptionLinkField';

  static const String surferOptionsColumn = 'surferOptionsColumn';
}
