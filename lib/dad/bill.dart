import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

part 'bill.g.dart';

class Bill = BillBase with _$Bill;

abstract class BillBase with Store {
  @observable
  String billId = '';
  @observable
  String billType = '';
  @observable
  List<String> chargeIds = [];
  @observable
  int creditApplied = 0;
  @observable
  int creditOverflow = 0;
  @observable
  String currency = '';
  @observable
  String notes = '';
  @observable
  int period = 0;
  @observable
  int price = 0;
  @observable
  String projectId = '';
  @observable
  String status = '';
  @observable
  int timeCreated = 0;
  @observable
  bool wasAutopayUsed = false;

  bool isLoaded = false;

  void unpackFromApi(Map<String, dynamic> body) {
    billId = readString(body, 'bill_id');
    billType = readString(body, 'bill_type');
    chargeIds = readListOfString(body, 'charge_ids');
    creditApplied = readInt(body, 'credit_applied');
    creditOverflow = readInt(body, 'credit_overflow');
    currency = readString(body, 'currency');
    notes = readString(body, 'notes');
    period = readInt(body, 'period');
    price = readInt(body, 'price');
    projectId = readString(body, 'project_id');
    status = readString(body, 'status');
    wasAutopayUsed = readBool(body, 'was_autopay_used');
    timeCreated = readInt(body, 'time_created');

    isLoaded = true;
  }

  Future<void> loadFromApi(
    BuildContext context,
    String loadBillId,
  ) async {
    if (loadBillId == '') {
      return;
    }

    var king = King.of(context);
    print(king.todd.developerId);
    ApiResponse ares = await king.lip.api(
      EndpointsV1.billGetById,
      payload: {'bill_id': loadBillId},
    );
    var body = ares.body;
    unpackFromApi(body);
  }

  @computed
  bool get needsPayment {
    if (status == 'created' || status == 'try_again') {
      return true;
    }
    return false;
  }
}
