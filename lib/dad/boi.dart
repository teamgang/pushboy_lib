import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:mobx/mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

part 'boi.g.dart';

class BoisCalc = BoisCalcBase with _$BoisCalc;

abstract class BoisCalcBase with Store {
  @observable
  int customSent = 0;
  @observable
  int limitlessSent = 0;
  @observable
  int staticSent = 0;
  @observable
  Plan plan = Plan();

  @observable
  int cmCreated = 0;
  @observable
  int cmDismissed = 0;
  @observable
  int cmDisplayed = 0;
  @observable
  int cmTapped = 0;
  @observable
  int cmViewedInApp = 0;

  @action
  void clear() {
    customSent = 0;
    limitlessSent = 0;
    staticSent = 0;
    cmCreated = 0;
    cmDismissed = 0;
    cmDisplayed = 0;
    cmTapped = 0;
    cmViewedInApp = 0;
  }

  @action
  void countBoi(Boi boi) {
    switch (boi.plasticity) {
      case BoiPlasticity.none:
        break;
      case BoiPlasticity.custom:
        customSent++;
        break;
      case BoiPlasticity.limitless:
        limitlessSent++;
        break;
      case BoiPlasticity.static_:
        staticSent++;
        break;
      default:
        break;
    }

    cmCreated += boi.cmCreated;
    cmDismissed += boi.cmDismissed;
    cmDisplayed += boi.cmDisplayed;
    cmTapped += boi.cmTapped;
    cmViewedInApp += boi.cmViewedInApp;
  }

  @computed
  int get customPurchases {
    return ((customSent - plan.monthlyFreeCustom) / plan.addCustomUnits).ceil();
  }

  @computed
  int get customTotalPrice {
    return customPurchases * plan.addCustomPrice;
  }

  @computed
  int get limitlessPurchases {
    return ((limitlessSent - plan.monthlyFreeLimitless) /
            plan.addLimitlessUnits)
        .ceil();
  }

  @computed
  int get limitlessTotalPrice {
    return limitlessPurchases * plan.addLimitlessPrice;
  }

  @computed
  int get staticPurchases {
    return ((staticSent - plan.monthlyFreeStatic) / plan.addStaticUnits).ceil();
  }

  @computed
  int get staticTotalPrice {
    return staticPurchases * plan.addStaticPrice;
  }

  @computed
  int get totalPrice {
    return customTotalPrice + limitlessTotalPrice + staticTotalPrice;
  }
}

class Bois {
  final ObservableMap<String, ObservableList<Boi>> byChannel = ObservableMap();
  final ObservableMap<String, ObservableList<Boi>> byNetwork = ObservableMap();
  final ObservableMap<String, ObservableList<Boi>> byNetworkAndPeriod =
      ObservableMap();
  final ObservableMap<String, ObservableList<Boi>> byProjectAndPeriod =
      ObservableMap();

  BoisCalc latestCalc = BoisCalc();

  King king;
  Bois(this.king);

  ObservableList<Boi> getBoisOfChannel(String channelId) {
    if (channelId.isEmpty) {
      return ObservableList();
    }
    if (!byChannel.containsKey(channelId)) {
      ObservableList<Boi> listOfBois = ObservableList();
      byChannel[channelId] = listOfBois;
    }
    getBoisOfChannelFromApi(channelId);
    return byChannel[channelId] ?? ObservableList();
  }

  Future<void> getBoisOfChannelFromApi(String channelId) async {
    ApiResponse ares = await king.lip.api(
      EndpointsV1.boisGetByChannelId,
      payload: {'channel_id': channelId},
    );

    var body = ares.body;

    final listOfBois = byChannel[channelId] ?? ObservableList();
    listOfBois.clear();
    if (ares.body.containsKey('collection')) {
      for (final item in body['collection'] ?? []) {
        var boi = Boi();
        boi.unpackFromApi(item);
        listOfBois.add(boi);
      }
    }
  }

  ObservableList<Boi> getBoisOfNetwork(String networkId) {
    if (networkId.isEmpty) {
      return ObservableList();
    }
    if (!byNetwork.containsKey(networkId)) {
      ObservableList<Boi> listOfBois = ObservableList();
      byNetwork[networkId] = listOfBois;
    }
    getBoisOfNetworkFromApi(networkId);
    return byNetwork[networkId] ?? ObservableList();
  }

  Future<void> getBoisOfNetworkFromApi(String networkId) async {
    ApiResponse ares = await king.lip.api(
      EndpointsV1.boisGetByNetworkId,
      payload: {'network_id': networkId},
    );

    var body = ares.body;

    final listOfBois = byNetwork[networkId] ?? ObservableList();
    listOfBois.clear();
    if (ares.body.containsKey('collection')) {
      for (final item in body['collection'] ?? []) {
        var boi = Boi();
        boi.unpackFromApi(item);
        listOfBois.add(boi);
      }
    }
  }

  ObservableList<Boi> getBoisOfNetworkAndPeriod({
    required String networkId,
    required int year,
    required int month,
  }) {
    final thisUuid = uuidWithPeriod(networkId, year, month);

    if (networkId.isEmpty) {
      return ObservableList();
    }
    if (!byNetworkAndPeriod.containsKey(networkId)) {
      ObservableList<Boi> listOfBois = ObservableList();
      byNetworkAndPeriod[thisUuid] = listOfBois;
    }
    getBoisOfNetworkAndPeriodFromApi(
      networkId: networkId,
      year: year,
      month: month,
    );
    return byNetworkAndPeriod[thisUuid] ?? ObservableList();
  }

  Future<void> getBoisOfNetworkAndPeriodFromApi({
    required String networkId,
    required int month,
    required int year,
  }) async {
    ApiResponse ares = await king.lip.api(
      EndpointsV1.boisGetByNetworkIdAndPeriod,
      payload: {
        'network_id': networkId,
        'month': month,
        'year': year,
      },
    );

    var body = ares.body;

    final thisUuid = uuidWithPeriod(networkId, year, month);
    final listOfBois = byNetworkAndPeriod[thisUuid] ?? ObservableList();
    listOfBois.clear();
    latestCalc.clear();

    if (ares.body.containsKey('collection')) {
      for (final item in body['collection'] ?? []) {
        var boi = Boi();
        boi.unpackFromApi(item);
        listOfBois.add(boi);

        latestCalc.countBoi(boi);
      }
    }
  }

  ObservableList<Boi> getBoisOfProjectAndPeriod({
    required String projectId,
    required int year,
    required int month,
  }) {
    final thisUuid = uuidWithPeriod(projectId, year, month);

    if (projectId.isEmpty) {
      return ObservableList();
    }
    if (!byProjectAndPeriod.containsKey(thisUuid)) {
      ObservableList<Boi> listOfBois = ObservableList();
      byProjectAndPeriod[thisUuid] = listOfBois;
    }
    getBoisOfProjectAndPeriodFromApi(
      projectId: projectId,
      year: year,
      month: month,
    );
    return byProjectAndPeriod[thisUuid] ?? ObservableList();
  }

  Future<void> getBoisOfProjectAndPeriodFromApi({
    required String projectId,
    required int month,
    required int year,
  }) async {
    ApiResponse ares = await king.lip.api(
      EndpointsV1.boisGetByProjectIdAndPeriod,
      payload: {
        'project_id': projectId,
        'month': month,
        'year': year,
      },
    );

    var body = ares.body;

    final thisUuid = uuidWithPeriod(projectId, year, month);
    final listOfBois = byProjectAndPeriod[thisUuid] ?? ObservableList();
    listOfBois.clear();
    latestCalc.clear();

    if (ares.body.containsKey('collection')) {
      for (final item in body['collection'] ?? []) {
        var boi = Boi();
        boi.unpackFromApi(item);
        listOfBois.add(boi);

        latestCalc.countBoi(boi);
      }
    }
  }
}

class Boi = BoiBase with _$Boi;

abstract class BoiBase with Store {
  @observable
  String boiId = '';
  @observable
  String body = '';
  @observable
  String channelAbbrev = '';
  @observable
  String channelId = '';
  @observable
  String channelName = '';
  @observable
  String channelThumbAbbrev = '';
  @observable
  String channelThumbChannel = '';
  @observable
  String channelThumbSelected = '';
  @observable
  String colorPrimaryString = '';
  @observable
  String colorSecondaryString = '';

  @observable
  int cmCreated = 0;
  @observable
  int cmDismissed = 0;
  @observable
  int cmDisplayed = 0;
  @observable
  int cmTapped = 0;
  @observable
  int cmViewedInApp = 0;
  @observable
  int countFailure = 0;
  @observable
  int countSuccess = 0;
  @observable
  int countRecipientInstanceIds = 0;

  @observable
  String error = '';
  @observable
  bool isHidden = false;
  @observable
  BoiLayout layout = BoiLayout.none;
  @observable
  String networkId = '';
  @observable
  String notes = '';
  @observable
  int period = 0;
  @observable
  int planId = 0;
  @observable
  BoiPlasticity plasticity = BoiPlasticity.none;
  @observable
  String projectId = '';
  @observable
  String status = '';
  @observable
  String thumbSelected = '';
  @observable
  String title = '';
  @observable
  String token = '';
  @observable
  String url = '';

  @observable
  int timeCreated = 0;

  @observable
  Channel channel = Channel();

  bool isLoaded = false;

  Future<void> loadFromApi(BuildContext context, String boiId) async {
    ApiResponse ares = await King.of(context).lip.api(
      EndpointsV1.boiGetById,
      payload: {'boi_id': boiId},
    );
    unpackFromApi(ares.body);
  }

  Future<void> loadFromApiWithKing(King king, String boiId) async {
    ApiResponse ares = await king.lip.api(
      EndpointsV1.boiGetById,
      payload: {'boi_id': boiId},
    );
    unpackFromApi(ares.body);
  }

  @action
  void unpackFromApi(Map<String, dynamic> body_) {
    boiId = readString(body_, 'boi_id');
    body = readString(body_, 'body');
    channelAbbrev = readString(body_, 'channel_abbrev');
    channelId = readString(body_, 'channel_id');
    channelName = readString(body_, 'channel_name');
    channelThumbAbbrev = readString(body_, 'channel_thumb_abbrev');
    channelThumbChannel = readString(body_, 'channel_thumb_channel');
    channelThumbSelected = readString(body_, 'channel_thumb_selected');
    colorPrimaryString = readString(body_, 'color_primary');
    colorSecondaryString = readString(body_, 'color_secondary');

    cmCreated = readInt(body_, 'cm_created');
    cmDismissed = readInt(body_, 'cm_dismissed');
    cmDisplayed = readInt(body_, 'cm_displayed');
    cmTapped = readInt(body_, 'cm_tapped');
    cmViewedInApp = readInt(body_, 'cm_viewed_in_app');
    countFailure = readInt(body_, 'count_failure');
    countSuccess = readInt(body_, 'count_success');
    countRecipientInstanceIds = readInt(body_, 'count_recipient_instance_ids');

    error = readString(body_, 'error');
    isHidden = readBool(body_, 'is_hidden');
    networkId = readString(body_, 'network_id');
    notes = readString(body_, 'notes');
    period = readInt(body_, 'period');
    planId = readInt(body_, 'plan_id');
    projectId = readString(body_, 'project_id');
    status = readString(body_, 'status');
    thumbSelected = readString(body_, 'thumb_selected');
    title = readString(body_, 'title');
    token = readString(body_, 'token');
    url = readString(body_, 'url');

    timeCreated = readInt(body_, 'time_created');

    String plasticityString = readString(body_, 'plasticity');
    plasticity = boiPlasticityFromString(plasticityString);
    String layoutString = readString(body_, 'layout');
    layout = boiLayoutFromString(layoutString);
    isLoaded = true;
  }

  @computed
  String get thumbSelectedUrl {
    if (thumbSelected == 'abbrev') {
      return channelThumbAbbrev;
    } else if (thumbSelected == 'chan') {
      if (channelThumbSelected == 'abbrev') {
        return channelThumbAbbrev;
      } else if (channelThumbSelected == 'chan') {
        return channelThumbChannel;
      } else {
        return channelThumbSelected;
      }
    } else {
      return thumbSelected;
    }
  }

  @computed
  Color get colorPrimary {
    return colorFromString(colorPrimaryString);
  }

  @computed
  Color get colorSecondary {
    return colorFromString(colorSecondaryString);
  }

  @computed
  ImageProvider get thumb {
    var temp = Image.network(XFile(thumbSelected).path);
    return ResizeImage(temp.image, width: 160, height: 160);
  }
}
