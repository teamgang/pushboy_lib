// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'boi.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$BoisCalc on BoisCalcBase, Store {
  Computed<int>? _$customPurchasesComputed;

  @override
  int get customPurchases =>
      (_$customPurchasesComputed ??= Computed<int>(() => super.customPurchases,
              name: 'BoisCalcBase.customPurchases'))
          .value;
  Computed<int>? _$customTotalPriceComputed;

  @override
  int get customTotalPrice => (_$customTotalPriceComputed ??= Computed<int>(
          () => super.customTotalPrice,
          name: 'BoisCalcBase.customTotalPrice'))
      .value;
  Computed<int>? _$limitlessPurchasesComputed;

  @override
  int get limitlessPurchases => (_$limitlessPurchasesComputed ??= Computed<int>(
          () => super.limitlessPurchases,
          name: 'BoisCalcBase.limitlessPurchases'))
      .value;
  Computed<int>? _$limitlessTotalPriceComputed;

  @override
  int get limitlessTotalPrice => (_$limitlessTotalPriceComputed ??=
          Computed<int>(() => super.limitlessTotalPrice,
              name: 'BoisCalcBase.limitlessTotalPrice'))
      .value;
  Computed<int>? _$staticPurchasesComputed;

  @override
  int get staticPurchases =>
      (_$staticPurchasesComputed ??= Computed<int>(() => super.staticPurchases,
              name: 'BoisCalcBase.staticPurchases'))
          .value;
  Computed<int>? _$staticTotalPriceComputed;

  @override
  int get staticTotalPrice => (_$staticTotalPriceComputed ??= Computed<int>(
          () => super.staticTotalPrice,
          name: 'BoisCalcBase.staticTotalPrice'))
      .value;
  Computed<int>? _$totalPriceComputed;

  @override
  int get totalPrice =>
      (_$totalPriceComputed ??= Computed<int>(() => super.totalPrice,
              name: 'BoisCalcBase.totalPrice'))
          .value;

  late final _$customSentAtom =
      Atom(name: 'BoisCalcBase.customSent', context: context);

  @override
  int get customSent {
    _$customSentAtom.reportRead();
    return super.customSent;
  }

  @override
  set customSent(int value) {
    _$customSentAtom.reportWrite(value, super.customSent, () {
      super.customSent = value;
    });
  }

  late final _$limitlessSentAtom =
      Atom(name: 'BoisCalcBase.limitlessSent', context: context);

  @override
  int get limitlessSent {
    _$limitlessSentAtom.reportRead();
    return super.limitlessSent;
  }

  @override
  set limitlessSent(int value) {
    _$limitlessSentAtom.reportWrite(value, super.limitlessSent, () {
      super.limitlessSent = value;
    });
  }

  late final _$staticSentAtom =
      Atom(name: 'BoisCalcBase.staticSent', context: context);

  @override
  int get staticSent {
    _$staticSentAtom.reportRead();
    return super.staticSent;
  }

  @override
  set staticSent(int value) {
    _$staticSentAtom.reportWrite(value, super.staticSent, () {
      super.staticSent = value;
    });
  }

  late final _$planAtom = Atom(name: 'BoisCalcBase.plan', context: context);

  @override
  Plan get plan {
    _$planAtom.reportRead();
    return super.plan;
  }

  @override
  set plan(Plan value) {
    _$planAtom.reportWrite(value, super.plan, () {
      super.plan = value;
    });
  }

  late final _$cmCreatedAtom =
      Atom(name: 'BoisCalcBase.cmCreated', context: context);

  @override
  int get cmCreated {
    _$cmCreatedAtom.reportRead();
    return super.cmCreated;
  }

  @override
  set cmCreated(int value) {
    _$cmCreatedAtom.reportWrite(value, super.cmCreated, () {
      super.cmCreated = value;
    });
  }

  late final _$cmDismissedAtom =
      Atom(name: 'BoisCalcBase.cmDismissed', context: context);

  @override
  int get cmDismissed {
    _$cmDismissedAtom.reportRead();
    return super.cmDismissed;
  }

  @override
  set cmDismissed(int value) {
    _$cmDismissedAtom.reportWrite(value, super.cmDismissed, () {
      super.cmDismissed = value;
    });
  }

  late final _$cmDisplayedAtom =
      Atom(name: 'BoisCalcBase.cmDisplayed', context: context);

  @override
  int get cmDisplayed {
    _$cmDisplayedAtom.reportRead();
    return super.cmDisplayed;
  }

  @override
  set cmDisplayed(int value) {
    _$cmDisplayedAtom.reportWrite(value, super.cmDisplayed, () {
      super.cmDisplayed = value;
    });
  }

  late final _$cmTappedAtom =
      Atom(name: 'BoisCalcBase.cmTapped', context: context);

  @override
  int get cmTapped {
    _$cmTappedAtom.reportRead();
    return super.cmTapped;
  }

  @override
  set cmTapped(int value) {
    _$cmTappedAtom.reportWrite(value, super.cmTapped, () {
      super.cmTapped = value;
    });
  }

  late final _$cmViewedInAppAtom =
      Atom(name: 'BoisCalcBase.cmViewedInApp', context: context);

  @override
  int get cmViewedInApp {
    _$cmViewedInAppAtom.reportRead();
    return super.cmViewedInApp;
  }

  @override
  set cmViewedInApp(int value) {
    _$cmViewedInAppAtom.reportWrite(value, super.cmViewedInApp, () {
      super.cmViewedInApp = value;
    });
  }

  late final _$BoisCalcBaseActionController =
      ActionController(name: 'BoisCalcBase', context: context);

  @override
  void clear() {
    final _$actionInfo =
        _$BoisCalcBaseActionController.startAction(name: 'BoisCalcBase.clear');
    try {
      return super.clear();
    } finally {
      _$BoisCalcBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void countBoi(Boi boi) {
    final _$actionInfo = _$BoisCalcBaseActionController.startAction(
        name: 'BoisCalcBase.countBoi');
    try {
      return super.countBoi(boi);
    } finally {
      _$BoisCalcBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
customSent: ${customSent},
limitlessSent: ${limitlessSent},
staticSent: ${staticSent},
plan: ${plan},
cmCreated: ${cmCreated},
cmDismissed: ${cmDismissed},
cmDisplayed: ${cmDisplayed},
cmTapped: ${cmTapped},
cmViewedInApp: ${cmViewedInApp},
customPurchases: ${customPurchases},
customTotalPrice: ${customTotalPrice},
limitlessPurchases: ${limitlessPurchases},
limitlessTotalPrice: ${limitlessTotalPrice},
staticPurchases: ${staticPurchases},
staticTotalPrice: ${staticTotalPrice},
totalPrice: ${totalPrice}
    ''';
  }
}

mixin _$Boi on BoiBase, Store {
  Computed<String>? _$thumbSelectedUrlComputed;

  @override
  String get thumbSelectedUrl => (_$thumbSelectedUrlComputed ??=
          Computed<String>(() => super.thumbSelectedUrl,
              name: 'BoiBase.thumbSelectedUrl'))
      .value;
  Computed<Color>? _$colorPrimaryComputed;

  @override
  Color get colorPrimary =>
      (_$colorPrimaryComputed ??= Computed<Color>(() => super.colorPrimary,
              name: 'BoiBase.colorPrimary'))
          .value;
  Computed<Color>? _$colorSecondaryComputed;

  @override
  Color get colorSecondary =>
      (_$colorSecondaryComputed ??= Computed<Color>(() => super.colorSecondary,
              name: 'BoiBase.colorSecondary'))
          .value;
  Computed<ImageProvider<Object>>? _$thumbComputed;

  @override
  ImageProvider<Object> get thumb =>
      (_$thumbComputed ??= Computed<ImageProvider<Object>>(() => super.thumb,
              name: 'BoiBase.thumb'))
          .value;

  late final _$boiIdAtom = Atom(name: 'BoiBase.boiId', context: context);

  @override
  String get boiId {
    _$boiIdAtom.reportRead();
    return super.boiId;
  }

  @override
  set boiId(String value) {
    _$boiIdAtom.reportWrite(value, super.boiId, () {
      super.boiId = value;
    });
  }

  late final _$bodyAtom = Atom(name: 'BoiBase.body', context: context);

  @override
  String get body {
    _$bodyAtom.reportRead();
    return super.body;
  }

  @override
  set body(String value) {
    _$bodyAtom.reportWrite(value, super.body, () {
      super.body = value;
    });
  }

  late final _$channelAbbrevAtom =
      Atom(name: 'BoiBase.channelAbbrev', context: context);

  @override
  String get channelAbbrev {
    _$channelAbbrevAtom.reportRead();
    return super.channelAbbrev;
  }

  @override
  set channelAbbrev(String value) {
    _$channelAbbrevAtom.reportWrite(value, super.channelAbbrev, () {
      super.channelAbbrev = value;
    });
  }

  late final _$channelIdAtom =
      Atom(name: 'BoiBase.channelId', context: context);

  @override
  String get channelId {
    _$channelIdAtom.reportRead();
    return super.channelId;
  }

  @override
  set channelId(String value) {
    _$channelIdAtom.reportWrite(value, super.channelId, () {
      super.channelId = value;
    });
  }

  late final _$channelNameAtom =
      Atom(name: 'BoiBase.channelName', context: context);

  @override
  String get channelName {
    _$channelNameAtom.reportRead();
    return super.channelName;
  }

  @override
  set channelName(String value) {
    _$channelNameAtom.reportWrite(value, super.channelName, () {
      super.channelName = value;
    });
  }

  late final _$channelThumbAbbrevAtom =
      Atom(name: 'BoiBase.channelThumbAbbrev', context: context);

  @override
  String get channelThumbAbbrev {
    _$channelThumbAbbrevAtom.reportRead();
    return super.channelThumbAbbrev;
  }

  @override
  set channelThumbAbbrev(String value) {
    _$channelThumbAbbrevAtom.reportWrite(value, super.channelThumbAbbrev, () {
      super.channelThumbAbbrev = value;
    });
  }

  late final _$channelThumbChannelAtom =
      Atom(name: 'BoiBase.channelThumbChannel', context: context);

  @override
  String get channelThumbChannel {
    _$channelThumbChannelAtom.reportRead();
    return super.channelThumbChannel;
  }

  @override
  set channelThumbChannel(String value) {
    _$channelThumbChannelAtom.reportWrite(value, super.channelThumbChannel, () {
      super.channelThumbChannel = value;
    });
  }

  late final _$channelThumbSelectedAtom =
      Atom(name: 'BoiBase.channelThumbSelected', context: context);

  @override
  String get channelThumbSelected {
    _$channelThumbSelectedAtom.reportRead();
    return super.channelThumbSelected;
  }

  @override
  set channelThumbSelected(String value) {
    _$channelThumbSelectedAtom.reportWrite(value, super.channelThumbSelected,
        () {
      super.channelThumbSelected = value;
    });
  }

  late final _$colorPrimaryStringAtom =
      Atom(name: 'BoiBase.colorPrimaryString', context: context);

  @override
  String get colorPrimaryString {
    _$colorPrimaryStringAtom.reportRead();
    return super.colorPrimaryString;
  }

  @override
  set colorPrimaryString(String value) {
    _$colorPrimaryStringAtom.reportWrite(value, super.colorPrimaryString, () {
      super.colorPrimaryString = value;
    });
  }

  late final _$colorSecondaryStringAtom =
      Atom(name: 'BoiBase.colorSecondaryString', context: context);

  @override
  String get colorSecondaryString {
    _$colorSecondaryStringAtom.reportRead();
    return super.colorSecondaryString;
  }

  @override
  set colorSecondaryString(String value) {
    _$colorSecondaryStringAtom.reportWrite(value, super.colorSecondaryString,
        () {
      super.colorSecondaryString = value;
    });
  }

  late final _$cmCreatedAtom =
      Atom(name: 'BoiBase.cmCreated', context: context);

  @override
  int get cmCreated {
    _$cmCreatedAtom.reportRead();
    return super.cmCreated;
  }

  @override
  set cmCreated(int value) {
    _$cmCreatedAtom.reportWrite(value, super.cmCreated, () {
      super.cmCreated = value;
    });
  }

  late final _$cmDismissedAtom =
      Atom(name: 'BoiBase.cmDismissed', context: context);

  @override
  int get cmDismissed {
    _$cmDismissedAtom.reportRead();
    return super.cmDismissed;
  }

  @override
  set cmDismissed(int value) {
    _$cmDismissedAtom.reportWrite(value, super.cmDismissed, () {
      super.cmDismissed = value;
    });
  }

  late final _$cmDisplayedAtom =
      Atom(name: 'BoiBase.cmDisplayed', context: context);

  @override
  int get cmDisplayed {
    _$cmDisplayedAtom.reportRead();
    return super.cmDisplayed;
  }

  @override
  set cmDisplayed(int value) {
    _$cmDisplayedAtom.reportWrite(value, super.cmDisplayed, () {
      super.cmDisplayed = value;
    });
  }

  late final _$cmTappedAtom = Atom(name: 'BoiBase.cmTapped', context: context);

  @override
  int get cmTapped {
    _$cmTappedAtom.reportRead();
    return super.cmTapped;
  }

  @override
  set cmTapped(int value) {
    _$cmTappedAtom.reportWrite(value, super.cmTapped, () {
      super.cmTapped = value;
    });
  }

  late final _$cmViewedInAppAtom =
      Atom(name: 'BoiBase.cmViewedInApp', context: context);

  @override
  int get cmViewedInApp {
    _$cmViewedInAppAtom.reportRead();
    return super.cmViewedInApp;
  }

  @override
  set cmViewedInApp(int value) {
    _$cmViewedInAppAtom.reportWrite(value, super.cmViewedInApp, () {
      super.cmViewedInApp = value;
    });
  }

  late final _$countFailureAtom =
      Atom(name: 'BoiBase.countFailure', context: context);

  @override
  int get countFailure {
    _$countFailureAtom.reportRead();
    return super.countFailure;
  }

  @override
  set countFailure(int value) {
    _$countFailureAtom.reportWrite(value, super.countFailure, () {
      super.countFailure = value;
    });
  }

  late final _$countSuccessAtom =
      Atom(name: 'BoiBase.countSuccess', context: context);

  @override
  int get countSuccess {
    _$countSuccessAtom.reportRead();
    return super.countSuccess;
  }

  @override
  set countSuccess(int value) {
    _$countSuccessAtom.reportWrite(value, super.countSuccess, () {
      super.countSuccess = value;
    });
  }

  late final _$countRecipientInstanceIdsAtom =
      Atom(name: 'BoiBase.countRecipientInstanceIds', context: context);

  @override
  int get countRecipientInstanceIds {
    _$countRecipientInstanceIdsAtom.reportRead();
    return super.countRecipientInstanceIds;
  }

  @override
  set countRecipientInstanceIds(int value) {
    _$countRecipientInstanceIdsAtom
        .reportWrite(value, super.countRecipientInstanceIds, () {
      super.countRecipientInstanceIds = value;
    });
  }

  late final _$errorAtom = Atom(name: 'BoiBase.error', context: context);

  @override
  String get error {
    _$errorAtom.reportRead();
    return super.error;
  }

  @override
  set error(String value) {
    _$errorAtom.reportWrite(value, super.error, () {
      super.error = value;
    });
  }

  late final _$isHiddenAtom = Atom(name: 'BoiBase.isHidden', context: context);

  @override
  bool get isHidden {
    _$isHiddenAtom.reportRead();
    return super.isHidden;
  }

  @override
  set isHidden(bool value) {
    _$isHiddenAtom.reportWrite(value, super.isHidden, () {
      super.isHidden = value;
    });
  }

  late final _$layoutAtom = Atom(name: 'BoiBase.layout', context: context);

  @override
  BoiLayout get layout {
    _$layoutAtom.reportRead();
    return super.layout;
  }

  @override
  set layout(BoiLayout value) {
    _$layoutAtom.reportWrite(value, super.layout, () {
      super.layout = value;
    });
  }

  late final _$networkIdAtom =
      Atom(name: 'BoiBase.networkId', context: context);

  @override
  String get networkId {
    _$networkIdAtom.reportRead();
    return super.networkId;
  }

  @override
  set networkId(String value) {
    _$networkIdAtom.reportWrite(value, super.networkId, () {
      super.networkId = value;
    });
  }

  late final _$notesAtom = Atom(name: 'BoiBase.notes', context: context);

  @override
  String get notes {
    _$notesAtom.reportRead();
    return super.notes;
  }

  @override
  set notes(String value) {
    _$notesAtom.reportWrite(value, super.notes, () {
      super.notes = value;
    });
  }

  late final _$periodAtom = Atom(name: 'BoiBase.period', context: context);

  @override
  int get period {
    _$periodAtom.reportRead();
    return super.period;
  }

  @override
  set period(int value) {
    _$periodAtom.reportWrite(value, super.period, () {
      super.period = value;
    });
  }

  late final _$planIdAtom = Atom(name: 'BoiBase.planId', context: context);

  @override
  int get planId {
    _$planIdAtom.reportRead();
    return super.planId;
  }

  @override
  set planId(int value) {
    _$planIdAtom.reportWrite(value, super.planId, () {
      super.planId = value;
    });
  }

  late final _$plasticityAtom =
      Atom(name: 'BoiBase.plasticity', context: context);

  @override
  BoiPlasticity get plasticity {
    _$plasticityAtom.reportRead();
    return super.plasticity;
  }

  @override
  set plasticity(BoiPlasticity value) {
    _$plasticityAtom.reportWrite(value, super.plasticity, () {
      super.plasticity = value;
    });
  }

  late final _$projectIdAtom =
      Atom(name: 'BoiBase.projectId', context: context);

  @override
  String get projectId {
    _$projectIdAtom.reportRead();
    return super.projectId;
  }

  @override
  set projectId(String value) {
    _$projectIdAtom.reportWrite(value, super.projectId, () {
      super.projectId = value;
    });
  }

  late final _$statusAtom = Atom(name: 'BoiBase.status', context: context);

  @override
  String get status {
    _$statusAtom.reportRead();
    return super.status;
  }

  @override
  set status(String value) {
    _$statusAtom.reportWrite(value, super.status, () {
      super.status = value;
    });
  }

  late final _$thumbSelectedAtom =
      Atom(name: 'BoiBase.thumbSelected', context: context);

  @override
  String get thumbSelected {
    _$thumbSelectedAtom.reportRead();
    return super.thumbSelected;
  }

  @override
  set thumbSelected(String value) {
    _$thumbSelectedAtom.reportWrite(value, super.thumbSelected, () {
      super.thumbSelected = value;
    });
  }

  late final _$titleAtom = Atom(name: 'BoiBase.title', context: context);

  @override
  String get title {
    _$titleAtom.reportRead();
    return super.title;
  }

  @override
  set title(String value) {
    _$titleAtom.reportWrite(value, super.title, () {
      super.title = value;
    });
  }

  late final _$tokenAtom = Atom(name: 'BoiBase.token', context: context);

  @override
  String get token {
    _$tokenAtom.reportRead();
    return super.token;
  }

  @override
  set token(String value) {
    _$tokenAtom.reportWrite(value, super.token, () {
      super.token = value;
    });
  }

  late final _$urlAtom = Atom(name: 'BoiBase.url', context: context);

  @override
  String get url {
    _$urlAtom.reportRead();
    return super.url;
  }

  @override
  set url(String value) {
    _$urlAtom.reportWrite(value, super.url, () {
      super.url = value;
    });
  }

  late final _$timeCreatedAtom =
      Atom(name: 'BoiBase.timeCreated', context: context);

  @override
  int get timeCreated {
    _$timeCreatedAtom.reportRead();
    return super.timeCreated;
  }

  @override
  set timeCreated(int value) {
    _$timeCreatedAtom.reportWrite(value, super.timeCreated, () {
      super.timeCreated = value;
    });
  }

  late final _$channelAtom = Atom(name: 'BoiBase.channel', context: context);

  @override
  Channel get channel {
    _$channelAtom.reportRead();
    return super.channel;
  }

  @override
  set channel(Channel value) {
    _$channelAtom.reportWrite(value, super.channel, () {
      super.channel = value;
    });
  }

  late final _$BoiBaseActionController =
      ActionController(name: 'BoiBase', context: context);

  @override
  void unpackFromApi(Map<String, dynamic> body_) {
    final _$actionInfo =
        _$BoiBaseActionController.startAction(name: 'BoiBase.unpackFromApi');
    try {
      return super.unpackFromApi(body_);
    } finally {
      _$BoiBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
boiId: ${boiId},
body: ${body},
channelAbbrev: ${channelAbbrev},
channelId: ${channelId},
channelName: ${channelName},
channelThumbAbbrev: ${channelThumbAbbrev},
channelThumbChannel: ${channelThumbChannel},
channelThumbSelected: ${channelThumbSelected},
colorPrimaryString: ${colorPrimaryString},
colorSecondaryString: ${colorSecondaryString},
cmCreated: ${cmCreated},
cmDismissed: ${cmDismissed},
cmDisplayed: ${cmDisplayed},
cmTapped: ${cmTapped},
cmViewedInApp: ${cmViewedInApp},
countFailure: ${countFailure},
countSuccess: ${countSuccess},
countRecipientInstanceIds: ${countRecipientInstanceIds},
error: ${error},
isHidden: ${isHidden},
layout: ${layout},
networkId: ${networkId},
notes: ${notes},
period: ${period},
planId: ${planId},
plasticity: ${plasticity},
projectId: ${projectId},
status: ${status},
thumbSelected: ${thumbSelected},
title: ${title},
token: ${token},
url: ${url},
timeCreated: ${timeCreated},
channel: ${channel},
thumbSelectedUrl: ${thumbSelectedUrl},
colorPrimary: ${colorPrimary},
colorSecondary: ${colorSecondary},
thumb: ${thumb}
    ''';
  }
}
