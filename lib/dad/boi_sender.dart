import 'dart:convert';
import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

part 'boi_sender.g.dart';

enum BoiPlasticity {
  none,
  custom,
  limitless,
  static_,
  commandSyncAllSettings,
}

const int _payloadOffset = 26 + 22 + 30 + 32 + 48 + 52 + 52 + 20 + 25;

extension BoiPlasticityExtensions on BoiPlasticity {
  String get asString {
    switch (this) {
      case BoiPlasticity.none:
        return 'none';
      case BoiPlasticity.custom:
        return 'custom';
      case BoiPlasticity.limitless:
        return 'limitless';
      case BoiPlasticity.static_:
        return 'static';
      case BoiPlasticity.commandSyncAllSettings:
        return 'commandSyncAllSettings';
    }
  }

  bool get isCommand {
    switch (this) {
      case BoiPlasticity.commandSyncAllSettings:
        return true;
      default:
        return false;
    }
  }
}

BoiPlasticity boiPlasticityFromString(String text) {
  switch (text) {
    case 'custom':
      return BoiPlasticity.custom;
    case 'limitless':
      return BoiPlasticity.limitless;
    case 'static':
      return BoiPlasticity.static_;
    case 'commandSyncAllSettings':
      return BoiPlasticity.commandSyncAllSettings;
    default:
      throw Exception('Invalid boiPlasticity value +$text+');
  }
}

enum BoiLayout {
  none,
  bigPicture,
  bigText,
  default_,
}

BoiLayout boiLayoutFromString(String input) {
  switch (input) {
    case 'bigPicture':
      return BoiLayout.bigPicture;
    case 'bigText':
      return BoiLayout.bigText;
    case 'default':
      return BoiLayout.default_;
    default:
      return BoiLayout.none;
  }
}

extension BoiLayoutExtensions on BoiLayout {
  String get asString {
    switch (this) {
      case BoiLayout.none:
        return 'none';
      case BoiLayout.bigPicture:
        return 'bigPicture';
      case BoiLayout.bigText:
        return 'bigText';
      case BoiLayout.default_:
        return 'default';
    }
  }

  NotificationLayout get asNotificationLayout {
    switch (this) {
      case BoiLayout.bigPicture:
        return NotificationLayout.BigPicture;
      case BoiLayout.bigText:
        return NotificationLayout.BigText;
      case BoiLayout.default_:
      case BoiLayout.none:
        return NotificationLayout.Default;
    }
  }
}

class BoiSender = BoiSenderBase with _$BoiSender;

abstract class BoiSenderBase with Store {
  @observable
  String boiId = '';
  @observable
  String body = '';
  @observable
  BoiLayout layout = BoiLayout.default_;
  @observable
  String notes = '';
  @observable
  BoiPlasticity plasticity = BoiPlasticity.static_;
  @observable
  bool requestInProgress = false;
  @observable
  String thumbSelected = 'chan';
  @observable
  String title = '';
  @observable
  String url = '';

  @observable
  Channel channel = Channel();

  @computed
  String get thumbSelectedUrl {
    if (thumbSelected == 'abbrev') {
      return channel.thumbAbbrev;
    } else if (thumbSelected == 'chan') {
      return channel.thumbSelectedUrl;
    } else {
      return thumbSelected;
    }
  }

  final TextEditingController bodyController = TextEditingController(text: '');
  final TextEditingController titleController = TextEditingController(text: '');
  final TextEditingController urlController = TextEditingController(text: '');

  ObservableList<String> managedUserIds = ObservableList();
  ObservableList<String> returnedRecipientList = ObservableList();

  @action
  void readThumbSelected(Map<String, dynamic> body) {
    thumbSelected = readString(body, 'thumb_selected');
  }

  @computed
  bool get canSend {
    if (channel.channelId == '' ||
        requestInProgress == true ||
        fcmPayloadTooLarge) {
      return false;
    }
    return true;
  }

  @computed
  ImageProvider get thumb {
    var temp = Image.network(thumbSelectedUrl);
    return ResizeImage(temp.image, width: 160, height: 160);
  }

  Map<String, dynamic> get payload {
    switch (plasticity) {
      case BoiPlasticity.none:
        return {};

      case BoiPlasticity.commandSyncAllSettings:
        return {
          'plasticity': plasticity.asString,
        };

      case BoiPlasticity.static_:
        return {
          'body': '',
          'channel_route': channel.route,
          'layout': BoiLayout.default_.asString,
          'notes': notes,
          'plasticity': BoiPlasticity.static_.asString,
          'thumb_selected': 'static',
          'title': '',
          'token': 'asUser',
        };

      case BoiPlasticity.custom:
        return {
          'body': body,
          'channel_route': channel.route,
          'layout': layout.asString,
          'notes': notes,
          'plasticity': BoiPlasticity.custom.asString,
          'thumb_selected': thumbSelected,
          'title': title,
          'token': 'asUser',
        };

      case BoiPlasticity.limitless:
        return {
          'body': body,
          'channel_route': channel.route,
          'layout': layout.asString,
          'notes': notes,
          'plasticity': plasticity.asString,
          'thumb_selected': thumbSelected,
          'title': title,
          'token': 'asUser',
        };
    }
  }

  @computed
  Boi get tempStaticBoi {
    final boi = Boi();
    boi.channelAbbrev = channel.abbrev;
    boi.body = body;
    boi.channelId = channel.channelId;
    boi.channelName = channel.name;
    boi.channelThumbAbbrev = channel.thumbAbbrev;
    boi.channelThumbChannel = channel.thumbChannel;
    boi.channelThumbSelected = channel.thumbSelected;
    boi.colorPrimaryString = channel.colorPrimaryString;
    boi.colorSecondaryString = channel.colorSecondaryString;
    boi.layout = layout;
    boi.networkId = channel.networkId;
    boi.thumbSelected = thumbSelected;
    boi.title = title;
    boi.url = url;
    return boi;
  }

  @computed
  int get fcmPayloadSizeEstimate {
    var fcmPayload = {
      //'channel_key': 'limitless', // max 26 chars
      //'channel_abbrev': channel.abbrev, // max 22 chars
      'channel_name': channel.name,
      //'color_primary': channel.colorPrimaryString, // max 30 chars
      //'color_secondary': channel.colorSecondaryString, // max 32 chars
      //'boi_id': channel.channelId, // 48 chars
      //'channel_id': channel.channelId, // 52 chars
      //'network_id': channel.channelId, // 52 chars
      'body': body,
      //'layout': layout.asString, // max ~20 chars
      //'plasticity': plasticity.asString, // max 25 chars
      'title': title,
      'thumb_url': thumbSelectedUrl,
      'url': url,
    };
    return jsonEncode(fcmPayload).length + _payloadOffset;
  }

  @computed
  String get fcmPayloadSizeEstimateInfo {
    return 'Payload Size: $fcmPayloadSizeEstimate/4000';
  }

  @computed
  bool get fcmPayloadTooLarge {
    return fcmPayloadSizeEstimate > 4000;
  }
}
