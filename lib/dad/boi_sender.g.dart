// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'boi_sender.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$BoiSender on BoiSenderBase, Store {
  Computed<String>? _$thumbSelectedUrlComputed;

  @override
  String get thumbSelectedUrl => (_$thumbSelectedUrlComputed ??=
          Computed<String>(() => super.thumbSelectedUrl,
              name: 'BoiSenderBase.thumbSelectedUrl'))
      .value;
  Computed<bool>? _$canSendComputed;

  @override
  bool get canSend => (_$canSendComputed ??=
          Computed<bool>(() => super.canSend, name: 'BoiSenderBase.canSend'))
      .value;
  Computed<ImageProvider<Object>>? _$thumbComputed;

  @override
  ImageProvider<Object> get thumb =>
      (_$thumbComputed ??= Computed<ImageProvider<Object>>(() => super.thumb,
              name: 'BoiSenderBase.thumb'))
          .value;
  Computed<Boi>? _$tempStaticBoiComputed;

  @override
  Boi get tempStaticBoi =>
      (_$tempStaticBoiComputed ??= Computed<Boi>(() => super.tempStaticBoi,
              name: 'BoiSenderBase.tempStaticBoi'))
          .value;
  Computed<int>? _$fcmPayloadSizeEstimateComputed;

  @override
  int get fcmPayloadSizeEstimate => (_$fcmPayloadSizeEstimateComputed ??=
          Computed<int>(() => super.fcmPayloadSizeEstimate,
              name: 'BoiSenderBase.fcmPayloadSizeEstimate'))
      .value;
  Computed<String>? _$fcmPayloadSizeEstimateInfoComputed;

  @override
  String get fcmPayloadSizeEstimateInfo =>
      (_$fcmPayloadSizeEstimateInfoComputed ??= Computed<String>(
              () => super.fcmPayloadSizeEstimateInfo,
              name: 'BoiSenderBase.fcmPayloadSizeEstimateInfo'))
          .value;
  Computed<bool>? _$fcmPayloadTooLargeComputed;

  @override
  bool get fcmPayloadTooLarge => (_$fcmPayloadTooLargeComputed ??=
          Computed<bool>(() => super.fcmPayloadTooLarge,
              name: 'BoiSenderBase.fcmPayloadTooLarge'))
      .value;

  late final _$boiIdAtom = Atom(name: 'BoiSenderBase.boiId', context: context);

  @override
  String get boiId {
    _$boiIdAtom.reportRead();
    return super.boiId;
  }

  @override
  set boiId(String value) {
    _$boiIdAtom.reportWrite(value, super.boiId, () {
      super.boiId = value;
    });
  }

  late final _$bodyAtom = Atom(name: 'BoiSenderBase.body', context: context);

  @override
  String get body {
    _$bodyAtom.reportRead();
    return super.body;
  }

  @override
  set body(String value) {
    _$bodyAtom.reportWrite(value, super.body, () {
      super.body = value;
    });
  }

  late final _$layoutAtom =
      Atom(name: 'BoiSenderBase.layout', context: context);

  @override
  BoiLayout get layout {
    _$layoutAtom.reportRead();
    return super.layout;
  }

  @override
  set layout(BoiLayout value) {
    _$layoutAtom.reportWrite(value, super.layout, () {
      super.layout = value;
    });
  }

  late final _$notesAtom = Atom(name: 'BoiSenderBase.notes', context: context);

  @override
  String get notes {
    _$notesAtom.reportRead();
    return super.notes;
  }

  @override
  set notes(String value) {
    _$notesAtom.reportWrite(value, super.notes, () {
      super.notes = value;
    });
  }

  late final _$plasticityAtom =
      Atom(name: 'BoiSenderBase.plasticity', context: context);

  @override
  BoiPlasticity get plasticity {
    _$plasticityAtom.reportRead();
    return super.plasticity;
  }

  @override
  set plasticity(BoiPlasticity value) {
    _$plasticityAtom.reportWrite(value, super.plasticity, () {
      super.plasticity = value;
    });
  }

  late final _$requestInProgressAtom =
      Atom(name: 'BoiSenderBase.requestInProgress', context: context);

  @override
  bool get requestInProgress {
    _$requestInProgressAtom.reportRead();
    return super.requestInProgress;
  }

  @override
  set requestInProgress(bool value) {
    _$requestInProgressAtom.reportWrite(value, super.requestInProgress, () {
      super.requestInProgress = value;
    });
  }

  late final _$thumbSelectedAtom =
      Atom(name: 'BoiSenderBase.thumbSelected', context: context);

  @override
  String get thumbSelected {
    _$thumbSelectedAtom.reportRead();
    return super.thumbSelected;
  }

  @override
  set thumbSelected(String value) {
    _$thumbSelectedAtom.reportWrite(value, super.thumbSelected, () {
      super.thumbSelected = value;
    });
  }

  late final _$titleAtom = Atom(name: 'BoiSenderBase.title', context: context);

  @override
  String get title {
    _$titleAtom.reportRead();
    return super.title;
  }

  @override
  set title(String value) {
    _$titleAtom.reportWrite(value, super.title, () {
      super.title = value;
    });
  }

  late final _$urlAtom = Atom(name: 'BoiSenderBase.url', context: context);

  @override
  String get url {
    _$urlAtom.reportRead();
    return super.url;
  }

  @override
  set url(String value) {
    _$urlAtom.reportWrite(value, super.url, () {
      super.url = value;
    });
  }

  late final _$channelAtom =
      Atom(name: 'BoiSenderBase.channel', context: context);

  @override
  Channel get channel {
    _$channelAtom.reportRead();
    return super.channel;
  }

  @override
  set channel(Channel value) {
    _$channelAtom.reportWrite(value, super.channel, () {
      super.channel = value;
    });
  }

  late final _$BoiSenderBaseActionController =
      ActionController(name: 'BoiSenderBase', context: context);

  @override
  void readThumbSelected(Map<String, dynamic> body) {
    final _$actionInfo = _$BoiSenderBaseActionController.startAction(
        name: 'BoiSenderBase.readThumbSelected');
    try {
      return super.readThumbSelected(body);
    } finally {
      _$BoiSenderBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
boiId: ${boiId},
body: ${body},
layout: ${layout},
notes: ${notes},
plasticity: ${plasticity},
requestInProgress: ${requestInProgress},
thumbSelected: ${thumbSelected},
title: ${title},
url: ${url},
channel: ${channel},
thumbSelectedUrl: ${thumbSelectedUrl},
canSend: ${canSend},
thumb: ${thumb},
tempStaticBoi: ${tempStaticBoi},
fcmPayloadSizeEstimate: ${fcmPayloadSizeEstimate},
fcmPayloadSizeEstimateInfo: ${fcmPayloadSizeEstimateInfo},
fcmPayloadTooLarge: ${fcmPayloadTooLarge}
    ''';
  }
}
