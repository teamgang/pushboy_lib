import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

part 'boi_tx.g.dart';

class BoiTx = BoiTxBase with _$BoiTx;

abstract class BoiTxBase with Store {
  @observable
  String boiId = '';
  @observable
  String body = '';
  @observable
  String channelAbbrev = '';
  @observable
  String channelId = '';
  @observable
  String channelName = '';
  @observable
  String colorPrimaryString = '';
  @observable
  String colorSecondaryString = '';
  @observable
  BoiLayout layout = BoiLayout.none;
  @observable
  bool isSilenced = false;

  @observable
  String networkId = '';
  @observable
  String networkSubId = '';
  @observable
  BoiPlasticity plasticity = BoiPlasticity.none;
  @observable
  SoundResource surferSound = SoundResource.none;
  @observable
  String thumbSelectedUrl = '';
  @observable
  String title = '';
  @observable
  String url = '';
  @observable
  bool useSurferSound = false;

  @observable
  String channelKey = '';

  Map<String, dynamic> data = {};

  bool isLoaded = false;

  @action
  Future<void> unpackFromMessage(Map<String, dynamic> dataIn) async {
    String plasticityString = readString(dataIn, 'plasticity');
    plasticity = boiPlasticityFromString(plasticityString);

    if (plasticity.isCommand) {
      channelKey = readString(dataIn, 'channel_key');
    } else {
      boiId = readString(dataIn, 'boi_id');
      body = readString(dataIn, 'body');
      channelAbbrev = readString(dataIn, 'channel_abbrev');
      channelId = readString(dataIn, 'channel_id');
      channelName = readString(dataIn, 'channel_name');
      colorPrimaryString = readString(dataIn, 'color_primary');
      colorSecondaryString = readString(dataIn, 'color_secondary');
      networkId = readString(dataIn, 'network_id');
      networkSubId = readString(dataIn, 'network_sub_id');
      thumbSelectedUrl = readString(dataIn, 'thumb_selected_url');
      //thumbSelectedUrl = 'https://i.imgur.com/1M3cRXh.png';
      title = readString(dataIn, 'title');
      url = readString(dataIn, 'url');

      String layoutString = readString(dataIn, 'layout');
      layout = boiLayoutFromString(layoutString);
      String surferSoundString = readString(dataIn, 'surfer_sound');
      surferSound = soundResourceFromString(surferSoundString);

      isSilenced = readBoolFromString(dataIn, 'is_silenced');
      useSurferSound = readBoolFromString(dataIn, 'use_surfer_sound');
    }

    King king = await makeDefaultKing();
    final localChannelSub =
        king.dad.channelSubProxy.getChannelSubRef(channelId: channelId);
    localChannelSub.channelId = channelId;
    await localChannelSub.loadLocal();

    final localNetworkSub =
        king.dad.networkSubProxy.getNetworkSubRef(networkSubId: networkSubId);
    localNetworkSub.networkSubId = networkSubId;
    await localNetworkSub.loadLocal();

    if (localChannelSub.useRemote) {
      // use the values from the server
    } else {
      // use the locally saved values
      isSilenced = localChannelSub.isSilenced;
      surferSound = localChannelSub.surferSound;
      useSurferSound = localChannelSub.useSurferSound;
    }

    // networkSub overrides channelSub settings
    if (localNetworkSub.useRemote) {
      // use the values from the server
    } else {
      isSilenced = localNetworkSub.isSilenced || isSilenced;
    }

    if (!useSurferSound) {
      surferSound = SoundResource.none;
    }

    data = dataIn;
    isLoaded = true;
  }

  @computed
  Color get colorPrimary {
    return colorFromString(colorPrimaryString);
  }

  @computed
  Color get colorSecondary {
    return colorFromString(colorSecondaryString);
  }

  @computed
  int get boiIdInt {
    if (boiId.isEmpty) {
      return -1;
    }
    String in32bits = boiId.split('-').join('').substring(0, 7);
    return int.parse(in32bits, radix: 16);
  }

  Future<NotificationContent> asNotification() async {
    if (isSilenced) {
      //TOD:O: record that notification was silenced
      return NotificationContent(
        id: boiIdInt,
        channelKey: 'silent',
        body: 'should be silent',
        displayOnBackground: false,
        displayOnForeground: false,
        payload: notificationPayload,
      );
    }

    //NOTE: bigPicture and largeIcon cannot be emptyStrings
    switch (plasticity) {
      case BoiPlasticity.commandSyncAllSettings:
        return NotificationContent(
          id: boiIdInt,
          channelKey: 'command',
          displayOnBackground: false,
          displayOnForeground: false,
          payload: notificationPayload,
        );

      case BoiPlasticity.custom:
      case BoiPlasticity.static_:
      case BoiPlasticity.limitless:
        return NotificationContent(
          id: boiIdInt,
          channelKey: surferSound.name,
          title: titleHtml,
          ticker: title,
          body: bodyHtml,
          displayOnBackground: true,
          displayOnForeground: true,
          bigPicture: thumbSelectedUrl,
          //icon: thumbSelectedUrl,
          largeIcon: thumbSelectedUrl,
          category: NotificationCategory.Message,
          notificationLayout: layout.asNotificationLayout,
          payload: notificationPayload,
        );

      case BoiPlasticity.none:
        throw Exception('BoiPlasticity is none! boiId; $boiId');
    }
  }

  @computed
  String get bodyHtml {
    return '<br />$body';
  }

  @computed
  String get titleHtml {
    return '<b>$title</b>';
  }

  @computed
  Map<String, String> get notificationPayload {
    return {
      'boi_id': boiId,
      'channel_id': channelId,
      'network_id': networkId,
      'layout': layout.asString,
      'plasticity': plasticity.asString,
    };
  }
}
