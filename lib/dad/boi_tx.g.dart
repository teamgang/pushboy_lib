// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'boi_tx.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$BoiTx on BoiTxBase, Store {
  Computed<Color>? _$colorPrimaryComputed;

  @override
  Color get colorPrimary =>
      (_$colorPrimaryComputed ??= Computed<Color>(() => super.colorPrimary,
              name: 'BoiTxBase.colorPrimary'))
          .value;
  Computed<Color>? _$colorSecondaryComputed;

  @override
  Color get colorSecondary =>
      (_$colorSecondaryComputed ??= Computed<Color>(() => super.colorSecondary,
              name: 'BoiTxBase.colorSecondary'))
          .value;
  Computed<int>? _$boiIdIntComputed;

  @override
  int get boiIdInt => (_$boiIdIntComputed ??=
          Computed<int>(() => super.boiIdInt, name: 'BoiTxBase.boiIdInt'))
      .value;
  Computed<String>? _$bodyHtmlComputed;

  @override
  String get bodyHtml => (_$bodyHtmlComputed ??=
          Computed<String>(() => super.bodyHtml, name: 'BoiTxBase.bodyHtml'))
      .value;
  Computed<String>? _$titleHtmlComputed;

  @override
  String get titleHtml => (_$titleHtmlComputed ??=
          Computed<String>(() => super.titleHtml, name: 'BoiTxBase.titleHtml'))
      .value;
  Computed<Map<String, String>>? _$notificationPayloadComputed;

  @override
  Map<String, String> get notificationPayload =>
      (_$notificationPayloadComputed ??= Computed<Map<String, String>>(
              () => super.notificationPayload,
              name: 'BoiTxBase.notificationPayload'))
          .value;

  late final _$boiIdAtom = Atom(name: 'BoiTxBase.boiId', context: context);

  @override
  String get boiId {
    _$boiIdAtom.reportRead();
    return super.boiId;
  }

  @override
  set boiId(String value) {
    _$boiIdAtom.reportWrite(value, super.boiId, () {
      super.boiId = value;
    });
  }

  late final _$bodyAtom = Atom(name: 'BoiTxBase.body', context: context);

  @override
  String get body {
    _$bodyAtom.reportRead();
    return super.body;
  }

  @override
  set body(String value) {
    _$bodyAtom.reportWrite(value, super.body, () {
      super.body = value;
    });
  }

  late final _$channelAbbrevAtom =
      Atom(name: 'BoiTxBase.channelAbbrev', context: context);

  @override
  String get channelAbbrev {
    _$channelAbbrevAtom.reportRead();
    return super.channelAbbrev;
  }

  @override
  set channelAbbrev(String value) {
    _$channelAbbrevAtom.reportWrite(value, super.channelAbbrev, () {
      super.channelAbbrev = value;
    });
  }

  late final _$channelIdAtom =
      Atom(name: 'BoiTxBase.channelId', context: context);

  @override
  String get channelId {
    _$channelIdAtom.reportRead();
    return super.channelId;
  }

  @override
  set channelId(String value) {
    _$channelIdAtom.reportWrite(value, super.channelId, () {
      super.channelId = value;
    });
  }

  late final _$channelNameAtom =
      Atom(name: 'BoiTxBase.channelName', context: context);

  @override
  String get channelName {
    _$channelNameAtom.reportRead();
    return super.channelName;
  }

  @override
  set channelName(String value) {
    _$channelNameAtom.reportWrite(value, super.channelName, () {
      super.channelName = value;
    });
  }

  late final _$colorPrimaryStringAtom =
      Atom(name: 'BoiTxBase.colorPrimaryString', context: context);

  @override
  String get colorPrimaryString {
    _$colorPrimaryStringAtom.reportRead();
    return super.colorPrimaryString;
  }

  @override
  set colorPrimaryString(String value) {
    _$colorPrimaryStringAtom.reportWrite(value, super.colorPrimaryString, () {
      super.colorPrimaryString = value;
    });
  }

  late final _$colorSecondaryStringAtom =
      Atom(name: 'BoiTxBase.colorSecondaryString', context: context);

  @override
  String get colorSecondaryString {
    _$colorSecondaryStringAtom.reportRead();
    return super.colorSecondaryString;
  }

  @override
  set colorSecondaryString(String value) {
    _$colorSecondaryStringAtom.reportWrite(value, super.colorSecondaryString,
        () {
      super.colorSecondaryString = value;
    });
  }

  late final _$layoutAtom = Atom(name: 'BoiTxBase.layout', context: context);

  @override
  BoiLayout get layout {
    _$layoutAtom.reportRead();
    return super.layout;
  }

  @override
  set layout(BoiLayout value) {
    _$layoutAtom.reportWrite(value, super.layout, () {
      super.layout = value;
    });
  }

  late final _$isSilencedAtom =
      Atom(name: 'BoiTxBase.isSilenced', context: context);

  @override
  bool get isSilenced {
    _$isSilencedAtom.reportRead();
    return super.isSilenced;
  }

  @override
  set isSilenced(bool value) {
    _$isSilencedAtom.reportWrite(value, super.isSilenced, () {
      super.isSilenced = value;
    });
  }

  late final _$networkIdAtom =
      Atom(name: 'BoiTxBase.networkId', context: context);

  @override
  String get networkId {
    _$networkIdAtom.reportRead();
    return super.networkId;
  }

  @override
  set networkId(String value) {
    _$networkIdAtom.reportWrite(value, super.networkId, () {
      super.networkId = value;
    });
  }

  late final _$networkSubIdAtom =
      Atom(name: 'BoiTxBase.networkSubId', context: context);

  @override
  String get networkSubId {
    _$networkSubIdAtom.reportRead();
    return super.networkSubId;
  }

  @override
  set networkSubId(String value) {
    _$networkSubIdAtom.reportWrite(value, super.networkSubId, () {
      super.networkSubId = value;
    });
  }

  late final _$plasticityAtom =
      Atom(name: 'BoiTxBase.plasticity', context: context);

  @override
  BoiPlasticity get plasticity {
    _$plasticityAtom.reportRead();
    return super.plasticity;
  }

  @override
  set plasticity(BoiPlasticity value) {
    _$plasticityAtom.reportWrite(value, super.plasticity, () {
      super.plasticity = value;
    });
  }

  late final _$surferSoundAtom =
      Atom(name: 'BoiTxBase.surferSound', context: context);

  @override
  SoundResource get surferSound {
    _$surferSoundAtom.reportRead();
    return super.surferSound;
  }

  @override
  set surferSound(SoundResource value) {
    _$surferSoundAtom.reportWrite(value, super.surferSound, () {
      super.surferSound = value;
    });
  }

  late final _$thumbSelectedUrlAtom =
      Atom(name: 'BoiTxBase.thumbSelectedUrl', context: context);

  @override
  String get thumbSelectedUrl {
    _$thumbSelectedUrlAtom.reportRead();
    return super.thumbSelectedUrl;
  }

  @override
  set thumbSelectedUrl(String value) {
    _$thumbSelectedUrlAtom.reportWrite(value, super.thumbSelectedUrl, () {
      super.thumbSelectedUrl = value;
    });
  }

  late final _$titleAtom = Atom(name: 'BoiTxBase.title', context: context);

  @override
  String get title {
    _$titleAtom.reportRead();
    return super.title;
  }

  @override
  set title(String value) {
    _$titleAtom.reportWrite(value, super.title, () {
      super.title = value;
    });
  }

  late final _$urlAtom = Atom(name: 'BoiTxBase.url', context: context);

  @override
  String get url {
    _$urlAtom.reportRead();
    return super.url;
  }

  @override
  set url(String value) {
    _$urlAtom.reportWrite(value, super.url, () {
      super.url = value;
    });
  }

  late final _$useSurferSoundAtom =
      Atom(name: 'BoiTxBase.useSurferSound', context: context);

  @override
  bool get useSurferSound {
    _$useSurferSoundAtom.reportRead();
    return super.useSurferSound;
  }

  @override
  set useSurferSound(bool value) {
    _$useSurferSoundAtom.reportWrite(value, super.useSurferSound, () {
      super.useSurferSound = value;
    });
  }

  late final _$channelKeyAtom =
      Atom(name: 'BoiTxBase.channelKey', context: context);

  @override
  String get channelKey {
    _$channelKeyAtom.reportRead();
    return super.channelKey;
  }

  @override
  set channelKey(String value) {
    _$channelKeyAtom.reportWrite(value, super.channelKey, () {
      super.channelKey = value;
    });
  }

  late final _$unpackFromMessageAsyncAction =
      AsyncAction('BoiTxBase.unpackFromMessage', context: context);

  @override
  Future<void> unpackFromMessage(Map<String, dynamic> dataIn) {
    return _$unpackFromMessageAsyncAction
        .run(() => super.unpackFromMessage(dataIn));
  }

  @override
  String toString() {
    return '''
boiId: ${boiId},
body: ${body},
channelAbbrev: ${channelAbbrev},
channelId: ${channelId},
channelName: ${channelName},
colorPrimaryString: ${colorPrimaryString},
colorSecondaryString: ${colorSecondaryString},
layout: ${layout},
isSilenced: ${isSilenced},
networkId: ${networkId},
networkSubId: ${networkSubId},
plasticity: ${plasticity},
surferSound: ${surferSound},
thumbSelectedUrl: ${thumbSelectedUrl},
title: ${title},
url: ${url},
useSurferSound: ${useSurferSound},
channelKey: ${channelKey},
colorPrimary: ${colorPrimary},
colorSecondary: ${colorSecondary},
boiIdInt: ${boiIdInt},
bodyHtml: ${bodyHtml},
titleHtml: ${titleHtml},
notificationPayload: ${notificationPayload}
    ''';
  }
}
