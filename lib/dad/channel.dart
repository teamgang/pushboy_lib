import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

part 'channel.g.dart';

class Channels {
  final ObservableMap<String, ObservableList<Channel>> byNetwork =
      ObservableMap();
  King king;
  Channels(this.king);

  void refreshChannelsOfNetwork(String networkId) async {
    byNetwork[networkId]!.clear();
    getChannelsOfNetwork(networkId);
  }

  ObservableList<Channel> getChannelsOfNetwork(String networkId) {
    if (!byNetwork.containsKey(networkId)) {
      ObservableList<Channel> listOfChannels = ObservableList();
      byNetwork[networkId] = listOfChannels;
    }
    getChannelsOfNetworkFromApi(networkId);
    return byNetwork[networkId] ?? ObservableList();
  }

  Future<void> getChannelsOfNetworkFromApi(String networkId) async {
    if (networkId == '') {
      return;
    }
    ApiResponse ares = await king.lip.api(
      EndpointsV1.channelsGetByNetworkId,
      payload: {'network_id': networkId},
    );

    final listOfChannels = byNetwork[networkId] ?? ObservableList();
    listOfChannels.clear();
    if (ares.body.containsKey('collection')) {
      for (final item in ares.body['collection'] ?? []) {
        var channel = Channel();
        channel.unpackFromApi(item);
        listOfChannels.add(channel);
      }
    }
  }
}

class Channel = ChannelBase with _$Channel;

abstract class ChannelBase with Store {
  @observable
  ObservableList<ChannelToken> tokens = ObservableList();
  @observable
  String abbrev = '';
  @observable
  String channelId = '';
  @observable
  String colorPrimaryString = '';
  @observable
  String colorSecondaryString = '';
  @observable
  String description = '';
  @observable
  String name = '';
  @observable
  String networkId = '';
  @observable
  String notes = '';
  @observable
  String page1 = '';
  @observable
  String route = '';

  @observable
  String staticBody = '';
  @observable
  int staticEditExpires = 0;
  @observable
  String staticThumbSelected = '';
  @observable
  String staticTitle = '';
  @observable
  String staticUrl = '';

  @observable
  String thumbAbbrev = '';
  @observable
  String thumbChannel = '';
  @observable
  String thumbSelected = '';

  @observable
  int statMonthlyTokensLeft = 0;
  @observable
  int statTokensConsumed = 0;
  @observable
  int statTokensPurchased = 0;

  bool isLoaded = false;

  void clear() {
    tokens.clear();
    abbrev = '';
    channelId = '';
    colorPrimaryString = '';
    colorSecondaryString = '';
    description = '';
    name = '';
    networkId = '';
    notes = '';
    page1 = '';
    route = '';
    staticBody = '';
    staticEditExpires = 0;
    staticThumbSelected = '';
    staticTitle = '';
    staticUrl = '';
    thumbAbbrev = '';
    thumbChannel = '';
    thumbSelected = '';
  }

  Future<void> loadFromApi(
    BuildContext context,
    String loadChannelId,
  ) async {
    if (loadChannelId == '') {
      return;
    }

    var king = King.of(context);
    ApiResponse ares = await king.lip.api(
      EndpointsV1.channelGetById,
      payload: {'channel_id': loadChannelId},
    );
    var body = ares.body;
    unpackFromApi(body);
  }

  void unpackFromApi(Map<String, dynamic> body) {
    channelId = readString(body, 'channel_id');
    abbrev = readString(body, 'abbrev');
    colorPrimaryString = readString(body, 'color_primary');
    colorSecondaryString = readString(body, 'color_secondary');
    description = readString(body, 'description');
    name = readString(body, 'name');
    networkId = readString(body, 'network_id');
    notes = readString(body, 'notes');
    page1 = readString(body, 'page1');
    route = readString(body, 'route');
    staticBody = readString(body, 'static_body');
    staticEditExpires = readInt(body, 'static_edit_expires');
    staticThumbSelected = readString(body, 'static_thumb_selected');
    staticTitle = readString(body, 'static_title');
    staticUrl = readString(body, 'static_url');

    thumbAbbrev = readString(body, 'thumb_abbrev');
    thumbChannel = readString(body, 'thumb_channel');
    thumbSelected = readString(body, 'thumb_selected');
    isLoaded = true;
  }

  void loadFromAltChannel(Channel alt) {
    channelId = alt.channelId;
    abbrev = alt.abbrev;
    colorPrimaryString = alt.colorPrimaryString;
    colorSecondaryString = alt.colorSecondaryString;
    description = alt.description;
    name = alt.name;
    networkId = alt.networkId;
    notes = alt.notes;
    page1 = alt.page1;
    route = alt.route;
    staticBody = alt.staticBody;
    staticEditExpires = alt.staticEditExpires;
    staticThumbSelected = alt.staticThumbSelected;
    staticTitle = alt.staticTitle;
    staticUrl = alt.staticUrl;
    thumbAbbrev = alt.thumbAbbrev;
    thumbChannel = alt.thumbChannel;
    thumbSelected = alt.thumbSelected;
    isLoaded = true;
  }

  Future<void> loadFromApiWithRoute(
    BuildContext context, {
    required String networkRoute,
    required String channelRoute,
    bool clearData = false,
  }) async {
    if (clearData) {
      clear();
    }
    if (networkRoute == '' || channelRoute == '') {
      return;
    }
    ApiResponse ares = await King.of(context).lip.api(
      EndpointsV1.channelGetByRoute,
      payload: {
        'network_route': networkRoute,
        'channel_route': channelRoute,
      },
    );
    if (ares.isOk) {
      unpackFromApi(ares.body);
    }
  }

  @action
  Future<void> loadTokensFromApi(BuildContext context) async {
    if (channelId == '') {
      return;
    }
    var king = King.of(context);
    ApiResponse ares = await king.lip.api(
      EndpointsV1.channelTokensGetByChannelId,
      payload: {'channel_id': channelId},
    );
    tokens.clear();
    if (ares.body.containsKey('collection')) {
      for (final item in ares.body['collection']) {
        var token = ChannelToken(
          channelId: item['channel_id'],
          token: item['token'],
        );
        tokens.add(token);
      }
    }
  }

  @action
  Future<void> loadStaticEditTokenInfo(BuildContext context) async {
    if (channelId == '') {
      return;
    }
    final now = DateTime.now();

    var king = King.of(context);
    ApiResponse ares = await king.lip.api(
      EndpointsV1.statTokenStatusByChannelAndPeriod,
      payload: {
        'month': now.month,
        'year': now.year,
        'channel_id': channelId,
      },
    );

    if (ares.isOk) {
      var temp = readInt(ares.body, 'monthly_tokens_left');
      if (statMonthlyTokensLeft != temp && statMonthlyTokensLeft >= 0) {
        statMonthlyTokensLeft = temp;
      }

      temp = readInt(ares.body, 'tokens_consumed');
      if (statTokensConsumed != temp) {
        statTokensConsumed = temp;
      }

      temp = readInt(ares.body, 'tokens_purchased');
      if (statTokensPurchased != temp) {
        statTokensPurchased = temp;
      }
    }
  }

  @computed
  Color get colorPrimary {
    return colorFromString(colorPrimaryString);
  }

  @computed
  Color get colorSecondary {
    return colorFromString(colorSecondaryString);
  }

  Network getNetwork(BuildContext context) {
    return King.of(context).dad.networks.getNetworkById(networkId);
  }

  @computed
  Boi get tempStaticBoi {
    final boi = Boi();
    boi.channelAbbrev = abbrev;
    boi.body = staticBody;
    boi.channelId = channelId;
    boi.channelName = name;
    boi.colorPrimaryString = colorPrimaryString;
    boi.colorSecondaryString = colorSecondaryString;
    boi.layout = BoiLayout.default_;
    boi.networkId = networkId;
    boi.channelThumbAbbrev = thumbAbbrev;
    boi.channelThumbChannel = thumbChannel;
    boi.channelThumbSelected = thumbSelected;
    boi.thumbSelected = staticThumbSelected;
    boi.title = staticTitle;
    boi.url = staticUrl;
    return boi;
  }

  @computed
  bool get isStaticEditActivated {
    return (DateTime.now().millisecondsSinceEpoch / 1000) < staticEditExpires;
  }

  @computed
  String get editStaticTimeRemaining {
    int sLeft = staticEditExpires -
        (DateTime.now().millisecondsSinceEpoch / 1000).ceil();
    int hh = (sLeft / 3600).floor();
    sLeft -= (hh * 3600);
    int mm = (sLeft / 60).floor();
    sLeft -= (mm * 60);
    return '$hh:$mm:$sLeft';
  }

  @computed
  String get thumbSelectedUrl {
    if (thumbSelected == 'abbrev') {
      return thumbAbbrev;
    } else if (thumbSelected == 'chan') {
      return thumbChannel;
    } else {
      return thumbSelected;
    }
  }

  @computed
  String get staticThumbSelectedUrl {
    if (staticThumbSelected == 'abbrev') {
      return thumbAbbrev;
    } else if (staticThumbSelected == 'chan') {
      return thumbChannel;
    } else {
      return thumbSelected;
    }
  }
}

class ChannelToken {
  ChannelToken({required this.channelId, required this.token});
  String channelId;
  String token;
}
