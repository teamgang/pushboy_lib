// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'channel.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$Channel on ChannelBase, Store {
  Computed<Color>? _$colorPrimaryComputed;

  @override
  Color get colorPrimary =>
      (_$colorPrimaryComputed ??= Computed<Color>(() => super.colorPrimary,
              name: 'ChannelBase.colorPrimary'))
          .value;
  Computed<Color>? _$colorSecondaryComputed;

  @override
  Color get colorSecondary =>
      (_$colorSecondaryComputed ??= Computed<Color>(() => super.colorSecondary,
              name: 'ChannelBase.colorSecondary'))
          .value;
  Computed<Boi>? _$tempStaticBoiComputed;

  @override
  Boi get tempStaticBoi =>
      (_$tempStaticBoiComputed ??= Computed<Boi>(() => super.tempStaticBoi,
              name: 'ChannelBase.tempStaticBoi'))
          .value;
  Computed<bool>? _$isStaticEditActivatedComputed;

  @override
  bool get isStaticEditActivated => (_$isStaticEditActivatedComputed ??=
          Computed<bool>(() => super.isStaticEditActivated,
              name: 'ChannelBase.isStaticEditActivated'))
      .value;
  Computed<String>? _$editStaticTimeRemainingComputed;

  @override
  String get editStaticTimeRemaining => (_$editStaticTimeRemainingComputed ??=
          Computed<String>(() => super.editStaticTimeRemaining,
              name: 'ChannelBase.editStaticTimeRemaining'))
      .value;
  Computed<String>? _$thumbSelectedUrlComputed;

  @override
  String get thumbSelectedUrl => (_$thumbSelectedUrlComputed ??=
          Computed<String>(() => super.thumbSelectedUrl,
              name: 'ChannelBase.thumbSelectedUrl'))
      .value;
  Computed<String>? _$staticThumbSelectedUrlComputed;

  @override
  String get staticThumbSelectedUrl => (_$staticThumbSelectedUrlComputed ??=
          Computed<String>(() => super.staticThumbSelectedUrl,
              name: 'ChannelBase.staticThumbSelectedUrl'))
      .value;

  late final _$tokensAtom = Atom(name: 'ChannelBase.tokens', context: context);

  @override
  ObservableList<ChannelToken> get tokens {
    _$tokensAtom.reportRead();
    return super.tokens;
  }

  @override
  set tokens(ObservableList<ChannelToken> value) {
    _$tokensAtom.reportWrite(value, super.tokens, () {
      super.tokens = value;
    });
  }

  late final _$abbrevAtom = Atom(name: 'ChannelBase.abbrev', context: context);

  @override
  String get abbrev {
    _$abbrevAtom.reportRead();
    return super.abbrev;
  }

  @override
  set abbrev(String value) {
    _$abbrevAtom.reportWrite(value, super.abbrev, () {
      super.abbrev = value;
    });
  }

  late final _$channelIdAtom =
      Atom(name: 'ChannelBase.channelId', context: context);

  @override
  String get channelId {
    _$channelIdAtom.reportRead();
    return super.channelId;
  }

  @override
  set channelId(String value) {
    _$channelIdAtom.reportWrite(value, super.channelId, () {
      super.channelId = value;
    });
  }

  late final _$colorPrimaryStringAtom =
      Atom(name: 'ChannelBase.colorPrimaryString', context: context);

  @override
  String get colorPrimaryString {
    _$colorPrimaryStringAtom.reportRead();
    return super.colorPrimaryString;
  }

  @override
  set colorPrimaryString(String value) {
    _$colorPrimaryStringAtom.reportWrite(value, super.colorPrimaryString, () {
      super.colorPrimaryString = value;
    });
  }

  late final _$colorSecondaryStringAtom =
      Atom(name: 'ChannelBase.colorSecondaryString', context: context);

  @override
  String get colorSecondaryString {
    _$colorSecondaryStringAtom.reportRead();
    return super.colorSecondaryString;
  }

  @override
  set colorSecondaryString(String value) {
    _$colorSecondaryStringAtom.reportWrite(value, super.colorSecondaryString,
        () {
      super.colorSecondaryString = value;
    });
  }

  late final _$descriptionAtom =
      Atom(name: 'ChannelBase.description', context: context);

  @override
  String get description {
    _$descriptionAtom.reportRead();
    return super.description;
  }

  @override
  set description(String value) {
    _$descriptionAtom.reportWrite(value, super.description, () {
      super.description = value;
    });
  }

  late final _$nameAtom = Atom(name: 'ChannelBase.name', context: context);

  @override
  String get name {
    _$nameAtom.reportRead();
    return super.name;
  }

  @override
  set name(String value) {
    _$nameAtom.reportWrite(value, super.name, () {
      super.name = value;
    });
  }

  late final _$networkIdAtom =
      Atom(name: 'ChannelBase.networkId', context: context);

  @override
  String get networkId {
    _$networkIdAtom.reportRead();
    return super.networkId;
  }

  @override
  set networkId(String value) {
    _$networkIdAtom.reportWrite(value, super.networkId, () {
      super.networkId = value;
    });
  }

  late final _$notesAtom = Atom(name: 'ChannelBase.notes', context: context);

  @override
  String get notes {
    _$notesAtom.reportRead();
    return super.notes;
  }

  @override
  set notes(String value) {
    _$notesAtom.reportWrite(value, super.notes, () {
      super.notes = value;
    });
  }

  late final _$page1Atom = Atom(name: 'ChannelBase.page1', context: context);

  @override
  String get page1 {
    _$page1Atom.reportRead();
    return super.page1;
  }

  @override
  set page1(String value) {
    _$page1Atom.reportWrite(value, super.page1, () {
      super.page1 = value;
    });
  }

  late final _$routeAtom = Atom(name: 'ChannelBase.route', context: context);

  @override
  String get route {
    _$routeAtom.reportRead();
    return super.route;
  }

  @override
  set route(String value) {
    _$routeAtom.reportWrite(value, super.route, () {
      super.route = value;
    });
  }

  late final _$staticBodyAtom =
      Atom(name: 'ChannelBase.staticBody', context: context);

  @override
  String get staticBody {
    _$staticBodyAtom.reportRead();
    return super.staticBody;
  }

  @override
  set staticBody(String value) {
    _$staticBodyAtom.reportWrite(value, super.staticBody, () {
      super.staticBody = value;
    });
  }

  late final _$staticEditExpiresAtom =
      Atom(name: 'ChannelBase.staticEditExpires', context: context);

  @override
  int get staticEditExpires {
    _$staticEditExpiresAtom.reportRead();
    return super.staticEditExpires;
  }

  @override
  set staticEditExpires(int value) {
    _$staticEditExpiresAtom.reportWrite(value, super.staticEditExpires, () {
      super.staticEditExpires = value;
    });
  }

  late final _$staticThumbSelectedAtom =
      Atom(name: 'ChannelBase.staticThumbSelected', context: context);

  @override
  String get staticThumbSelected {
    _$staticThumbSelectedAtom.reportRead();
    return super.staticThumbSelected;
  }

  @override
  set staticThumbSelected(String value) {
    _$staticThumbSelectedAtom.reportWrite(value, super.staticThumbSelected, () {
      super.staticThumbSelected = value;
    });
  }

  late final _$staticTitleAtom =
      Atom(name: 'ChannelBase.staticTitle', context: context);

  @override
  String get staticTitle {
    _$staticTitleAtom.reportRead();
    return super.staticTitle;
  }

  @override
  set staticTitle(String value) {
    _$staticTitleAtom.reportWrite(value, super.staticTitle, () {
      super.staticTitle = value;
    });
  }

  late final _$staticUrlAtom =
      Atom(name: 'ChannelBase.staticUrl', context: context);

  @override
  String get staticUrl {
    _$staticUrlAtom.reportRead();
    return super.staticUrl;
  }

  @override
  set staticUrl(String value) {
    _$staticUrlAtom.reportWrite(value, super.staticUrl, () {
      super.staticUrl = value;
    });
  }

  late final _$thumbAbbrevAtom =
      Atom(name: 'ChannelBase.thumbAbbrev', context: context);

  @override
  String get thumbAbbrev {
    _$thumbAbbrevAtom.reportRead();
    return super.thumbAbbrev;
  }

  @override
  set thumbAbbrev(String value) {
    _$thumbAbbrevAtom.reportWrite(value, super.thumbAbbrev, () {
      super.thumbAbbrev = value;
    });
  }

  late final _$thumbChannelAtom =
      Atom(name: 'ChannelBase.thumbChannel', context: context);

  @override
  String get thumbChannel {
    _$thumbChannelAtom.reportRead();
    return super.thumbChannel;
  }

  @override
  set thumbChannel(String value) {
    _$thumbChannelAtom.reportWrite(value, super.thumbChannel, () {
      super.thumbChannel = value;
    });
  }

  late final _$thumbSelectedAtom =
      Atom(name: 'ChannelBase.thumbSelected', context: context);

  @override
  String get thumbSelected {
    _$thumbSelectedAtom.reportRead();
    return super.thumbSelected;
  }

  @override
  set thumbSelected(String value) {
    _$thumbSelectedAtom.reportWrite(value, super.thumbSelected, () {
      super.thumbSelected = value;
    });
  }

  late final _$statMonthlyTokensLeftAtom =
      Atom(name: 'ChannelBase.statMonthlyTokensLeft', context: context);

  @override
  int get statMonthlyTokensLeft {
    _$statMonthlyTokensLeftAtom.reportRead();
    return super.statMonthlyTokensLeft;
  }

  @override
  set statMonthlyTokensLeft(int value) {
    _$statMonthlyTokensLeftAtom.reportWrite(value, super.statMonthlyTokensLeft,
        () {
      super.statMonthlyTokensLeft = value;
    });
  }

  late final _$statTokensConsumedAtom =
      Atom(name: 'ChannelBase.statTokensConsumed', context: context);

  @override
  int get statTokensConsumed {
    _$statTokensConsumedAtom.reportRead();
    return super.statTokensConsumed;
  }

  @override
  set statTokensConsumed(int value) {
    _$statTokensConsumedAtom.reportWrite(value, super.statTokensConsumed, () {
      super.statTokensConsumed = value;
    });
  }

  late final _$statTokensPurchasedAtom =
      Atom(name: 'ChannelBase.statTokensPurchased', context: context);

  @override
  int get statTokensPurchased {
    _$statTokensPurchasedAtom.reportRead();
    return super.statTokensPurchased;
  }

  @override
  set statTokensPurchased(int value) {
    _$statTokensPurchasedAtom.reportWrite(value, super.statTokensPurchased, () {
      super.statTokensPurchased = value;
    });
  }

  late final _$loadTokensFromApiAsyncAction =
      AsyncAction('ChannelBase.loadTokensFromApi', context: context);

  @override
  Future<void> loadTokensFromApi(BuildContext context) {
    return _$loadTokensFromApiAsyncAction
        .run(() => super.loadTokensFromApi(context));
  }

  late final _$loadStaticEditTokenInfoAsyncAction =
      AsyncAction('ChannelBase.loadStaticEditTokenInfo', context: context);

  @override
  Future<void> loadStaticEditTokenInfo(BuildContext context) {
    return _$loadStaticEditTokenInfoAsyncAction
        .run(() => super.loadStaticEditTokenInfo(context));
  }

  @override
  String toString() {
    return '''
tokens: ${tokens},
abbrev: ${abbrev},
channelId: ${channelId},
colorPrimaryString: ${colorPrimaryString},
colorSecondaryString: ${colorSecondaryString},
description: ${description},
name: ${name},
networkId: ${networkId},
notes: ${notes},
page1: ${page1},
route: ${route},
staticBody: ${staticBody},
staticEditExpires: ${staticEditExpires},
staticThumbSelected: ${staticThumbSelected},
staticTitle: ${staticTitle},
staticUrl: ${staticUrl},
thumbAbbrev: ${thumbAbbrev},
thumbChannel: ${thumbChannel},
thumbSelected: ${thumbSelected},
statMonthlyTokensLeft: ${statMonthlyTokensLeft},
statTokensConsumed: ${statTokensConsumed},
statTokensPurchased: ${statTokensPurchased},
colorPrimary: ${colorPrimary},
colorSecondary: ${colorSecondary},
tempStaticBoi: ${tempStaticBoi},
isStaticEditActivated: ${isStaticEditActivated},
editStaticTimeRemaining: ${editStaticTimeRemaining},
thumbSelectedUrl: ${thumbSelectedUrl},
staticThumbSelectedUrl: ${staticThumbSelectedUrl}
    ''';
  }
}
