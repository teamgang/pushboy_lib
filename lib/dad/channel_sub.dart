import 'dart:convert';
import 'package:mobx/mobx.dart';
import 'package:universal_io/io.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

part 'channel_sub.g.dart';

class ChannelSubProxy {
  ChannelSubProxy({
    required this.channelSubCache,
    required this.channelSubIdsByNetworkSubIdCache,
    required this.king,
  });
  final Cache<ChannelSub> channelSubCache;
  final Cache<ObservableList<String>> channelSubIdsByNetworkSubIdCache;
  final King king;

  //NOTE: we do not need a unique key that includes networkSubId, because we
  //are using UUIDs, so channelId is unique enough

  ObservableList<String> getChannelSubIdsRef({required String networkSubId}) {
    return channelSubIdsByNetworkSubIdCache.getItemRef(networkSubId);
  }

  ChannelSub getChannelSubRef({required String channelId}) {
    //NOTE: we should always call this function instead of getItemRef
    var sub = channelSubCache.getItemRef(channelId);
    sub.loadLocal();
    return sub;
  }

  Future<ObservableList<String>> getAndLoadChannelSubIdsByNetworkSubId(
      {required String networkSubId}) async {
    ApiResponse ares = await king.lip.api(
      EndpointsV1.channelSubsByNetworkSubId,
      payload: {
        'network_sub_id': networkSubId,
      },
    );

    var idList = channelSubIdsByNetworkSubIdCache.getItemRef(networkSubId);
    idList.clear();
    final body = ares.body;
    if (ares.body.containsKey('collection')) {
      for (final item in body['collection'] ?? []) {
        var newData = ChannelSub();
        newData.unpackFromApi(item);

        var oldSub = getChannelSubRef(channelId: newData.channelId);
        oldSub.loadFromAlt(newData);

        idList.add(newData.channelId);
      }
    }
    return idList;
  }

  Future<ChannelSub> loadChannelSubByIds({
    required String networkSubId,
    required String channelId,
  }) async {
    ApiResponse ares = await king.lip.api(
      EndpointsV1.channelSubGetByIds,
      payload: {
        'channel_id': channelId,
        'network_sub_id': networkSubId,
      },
    );

    final sub = getChannelSubRef(channelId: channelId);
    sub.unpackFromApi(ares.body);
    return sub;
  }
}

class ChannelSub = ChannelSubBase with _$ChannelSub;

abstract class ChannelSubBase with Store {
  King? king;

  @observable
  String channelId = '';
  @observable
  String networkSubId = '';
  @observable
  String networkId = '';
  @observable
  bool isSubscribed = false;
  @observable
  bool syncEnabled = true; // default true for no settings

  @observable
  bool _localIsSilenced = false;
  @observable
  String _localSurferSound = '';
  @observable
  bool _localUseSurferSound = false;
  @observable
  bool _remoteIsSilenced = false;
  @observable
  String _remoteSurferSound = '';
  @observable
  bool _remoteUseSurferSound = false;

  final Channel channel = Channel();

  @computed
  String get path {
    return king!.conf.networkSubDir;
  }

  @computed
  bool get isLinkedToSurfer {
    return king!.dad.networkSubProxy
        .getNetworkSubRef(networkSubId: networkSubId)
        .isLinkedToSurfer;
  }

  bool get isIoWorking {
    return channelId.isNotEmpty;
  }

  @computed
  bool get useRemote {
    return syncEnabled && (king?.todd.isSignedIn ?? false) && isLinkedToSurfer;
  }

  @computed
  bool get isUnsubscribed {
    return !isSubscribed;
  }

  // isSilenced
  @computed
  bool get isSilenced {
    if (useRemote) {
      return _remoteIsSilenced;
    } else {
      return _localIsSilenced;
    }
  }

  set isSilenced(bool value) {
    if (useRemote) {
      _remoteIsSilenced = value;
    } else {
      _localIsSilenced = value;
    }
  }

  // surferSound
  @computed
  SoundResource get surferSound {
    if (useRemote) {
      return soundResourceFromString(_remoteSurferSound);
    } else {
      return soundResourceFromString(_localSurferSound);
    }
  }

  set surferSound(SoundResource res) {
    if (useRemote) {
      _remoteSurferSound = res.name;
    } else {
      _localSurferSound = res.name;
    }
  }

  // useSurferSound
  @computed
  bool get useSurferSound {
    if (useRemote) {
      return _remoteUseSurferSound;
    } else {
      return _localUseSurferSound;
    }
  }

  set useSurferSound(bool value) {
    if (useRemote) {
      _remoteUseSurferSound = value;
    } else {
      _localUseSurferSound = value;
    }
  }

  syncValues() async {
    loadLocal();
    loadFromApi();
  }

  @action
  unpackFromApi(Map<String, dynamic> body) {
    channelId = readString(body, 'channel_id');
    networkSubId = readString(body, 'network_sub_id');
    networkId = readString(body, 'network_id');
    isSubscribed = readBool(body, 'is_subscribed');

    _remoteIsSilenced = readBool(body, 'is_silenced');
    _remoteSurferSound = readString(body, 'surfer_sound');
    _remoteUseSurferSound = readBool(body, 'use_surfer_sound');

    final altChannel = Channel();
    altChannel.unpackFromApi(readMap(body, 'channel'));
    channel.loadFromAltChannel(altChannel);
  }

  @action
  loadFromAlt(ChannelSub alt) {
    channelId = alt.channelId;
    networkSubId = alt.networkSubId;
    networkId = alt.networkId;
    isSilenced = alt.isSilenced;
    isSubscribed = alt.isSubscribed;

    _localSurferSound = alt._localSurferSound;
    _localUseSurferSound = alt._localUseSurferSound;
    _remoteSurferSound = alt._remoteSurferSound;
    _remoteUseSurferSound = alt._remoteUseSurferSound;

    channel.loadFromAltChannel(alt.channel);
  }

  loadFromApi() async {
    ApiResponse ares = await king!.lip.api(
      EndpointsV1.channelSubGetByIds,
      payload: {
        'channel_id': channelId,
        'network_sub_id': networkSubId,
      },
    );

    unpackFromApi(ares.body);
  }

  Future<ApiResponse> saveToRemote() async {
    var ares = ApiResponse();
    if (syncEnabled) {
      // send new settings to api
      ares = await king!.lip.api(
        EndpointsV1.channelSubSettingsSaveToRemote,
        payload: {
          'channel_id': channelId,
          'network_sub_id': networkSubId,
          'is_silenced': _remoteIsSilenced,
          'surfer_sound': _remoteSurferSound,
          'use_surfer_sound': _remoteUseSurferSound,
        },
      );
    }
    return ares;
  }

  bool deepEquals(ChannelSub alt) {
    if (_localIsSilenced == alt._localIsSilenced &&
        _localSurferSound == alt._localSurferSound &&
        _localUseSurferSound == alt._localUseSurferSound &&
        _remoteIsSilenced == alt._remoteIsSilenced &&
        _remoteSurferSound == alt._remoteSurferSound &&
        _remoteUseSurferSound == alt._remoteUseSurferSound &&
        syncEnabled == alt.syncEnabled) {
      return true;
    }
    return false;
  }

  Map<String, dynamic> asJson() {
    return {
      'localIsSilenced': _localIsSilenced,
      'localSurferSound': _localSurferSound,
      'localUseSurferSound': _localUseSurferSound,
      'syncEnabled': syncEnabled,
    };
  }

  loadLocal() async {
    if (isIoWorking) {
      final file = await getFile();
      try {
        String contents = await file.readAsString();
        if (contents.isEmpty) {
          setDefaults();
          return;
        }
        var jason = jsonDecode(contents);
        _localIsSilenced = jason['localIsSilenced'];
        _localSurferSound = jason['localSurferSound'];
        _localUseSurferSound = jason['localUseSurferSound'];
        syncEnabled = jason['syncEnabled'];
      } catch (e, s) {
        king!.log.e('Failed to read from in loadLocal:\n$e\n$s');
        setDefaults();
        await file.delete();
        saveLocal();
      }
    } else {
      setDefaults();
    }
  }

  saveLocal() async {
    if (isIoWorking) {
      final file = await getFile();
      file.writeAsStringSync(json.encode(asJson()));
    }
  }

  Future<File> getFile() async {
    final file = File('$path/$channelId');
    if (!await file.exists()) {
      await file.create(recursive: true);
    }
    return file;
  }

  setDefaults() {
    _localIsSilenced = false;
    _localSurferSound = '';
    _localUseSurferSound = false;
    syncEnabled = true;
  }
}
