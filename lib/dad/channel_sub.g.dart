// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'channel_sub.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$ChannelSub on ChannelSubBase, Store {
  Computed<String>? _$pathComputed;

  @override
  String get path => (_$pathComputed ??=
          Computed<String>(() => super.path, name: 'ChannelSubBase.path'))
      .value;
  Computed<bool>? _$isLinkedToSurferComputed;

  @override
  bool get isLinkedToSurfer => (_$isLinkedToSurferComputed ??= Computed<bool>(
          () => super.isLinkedToSurfer,
          name: 'ChannelSubBase.isLinkedToSurfer'))
      .value;
  Computed<bool>? _$useRemoteComputed;

  @override
  bool get useRemote =>
      (_$useRemoteComputed ??= Computed<bool>(() => super.useRemote,
              name: 'ChannelSubBase.useRemote'))
          .value;
  Computed<bool>? _$isUnsubscribedComputed;

  @override
  bool get isUnsubscribed =>
      (_$isUnsubscribedComputed ??= Computed<bool>(() => super.isUnsubscribed,
              name: 'ChannelSubBase.isUnsubscribed'))
          .value;
  Computed<bool>? _$isSilencedComputed;

  @override
  bool get isSilenced =>
      (_$isSilencedComputed ??= Computed<bool>(() => super.isSilenced,
              name: 'ChannelSubBase.isSilenced'))
          .value;
  Computed<SoundResource>? _$surferSoundComputed;

  @override
  SoundResource get surferSound => (_$surferSoundComputed ??=
          Computed<SoundResource>(() => super.surferSound,
              name: 'ChannelSubBase.surferSound'))
      .value;
  Computed<bool>? _$useSurferSoundComputed;

  @override
  bool get useSurferSound =>
      (_$useSurferSoundComputed ??= Computed<bool>(() => super.useSurferSound,
              name: 'ChannelSubBase.useSurferSound'))
          .value;

  late final _$channelIdAtom =
      Atom(name: 'ChannelSubBase.channelId', context: context);

  @override
  String get channelId {
    _$channelIdAtom.reportRead();
    return super.channelId;
  }

  @override
  set channelId(String value) {
    _$channelIdAtom.reportWrite(value, super.channelId, () {
      super.channelId = value;
    });
  }

  late final _$networkSubIdAtom =
      Atom(name: 'ChannelSubBase.networkSubId', context: context);

  @override
  String get networkSubId {
    _$networkSubIdAtom.reportRead();
    return super.networkSubId;
  }

  @override
  set networkSubId(String value) {
    _$networkSubIdAtom.reportWrite(value, super.networkSubId, () {
      super.networkSubId = value;
    });
  }

  late final _$networkIdAtom =
      Atom(name: 'ChannelSubBase.networkId', context: context);

  @override
  String get networkId {
    _$networkIdAtom.reportRead();
    return super.networkId;
  }

  @override
  set networkId(String value) {
    _$networkIdAtom.reportWrite(value, super.networkId, () {
      super.networkId = value;
    });
  }

  late final _$isSubscribedAtom =
      Atom(name: 'ChannelSubBase.isSubscribed', context: context);

  @override
  bool get isSubscribed {
    _$isSubscribedAtom.reportRead();
    return super.isSubscribed;
  }

  @override
  set isSubscribed(bool value) {
    _$isSubscribedAtom.reportWrite(value, super.isSubscribed, () {
      super.isSubscribed = value;
    });
  }

  late final _$syncEnabledAtom =
      Atom(name: 'ChannelSubBase.syncEnabled', context: context);

  @override
  bool get syncEnabled {
    _$syncEnabledAtom.reportRead();
    return super.syncEnabled;
  }

  @override
  set syncEnabled(bool value) {
    _$syncEnabledAtom.reportWrite(value, super.syncEnabled, () {
      super.syncEnabled = value;
    });
  }

  late final _$_localIsSilencedAtom =
      Atom(name: 'ChannelSubBase._localIsSilenced', context: context);

  @override
  bool get _localIsSilenced {
    _$_localIsSilencedAtom.reportRead();
    return super._localIsSilenced;
  }

  @override
  set _localIsSilenced(bool value) {
    _$_localIsSilencedAtom.reportWrite(value, super._localIsSilenced, () {
      super._localIsSilenced = value;
    });
  }

  late final _$_localSurferSoundAtom =
      Atom(name: 'ChannelSubBase._localSurferSound', context: context);

  @override
  String get _localSurferSound {
    _$_localSurferSoundAtom.reportRead();
    return super._localSurferSound;
  }

  @override
  set _localSurferSound(String value) {
    _$_localSurferSoundAtom.reportWrite(value, super._localSurferSound, () {
      super._localSurferSound = value;
    });
  }

  late final _$_localUseSurferSoundAtom =
      Atom(name: 'ChannelSubBase._localUseSurferSound', context: context);

  @override
  bool get _localUseSurferSound {
    _$_localUseSurferSoundAtom.reportRead();
    return super._localUseSurferSound;
  }

  @override
  set _localUseSurferSound(bool value) {
    _$_localUseSurferSoundAtom.reportWrite(value, super._localUseSurferSound,
        () {
      super._localUseSurferSound = value;
    });
  }

  late final _$_remoteIsSilencedAtom =
      Atom(name: 'ChannelSubBase._remoteIsSilenced', context: context);

  @override
  bool get _remoteIsSilenced {
    _$_remoteIsSilencedAtom.reportRead();
    return super._remoteIsSilenced;
  }

  @override
  set _remoteIsSilenced(bool value) {
    _$_remoteIsSilencedAtom.reportWrite(value, super._remoteIsSilenced, () {
      super._remoteIsSilenced = value;
    });
  }

  late final _$_remoteSurferSoundAtom =
      Atom(name: 'ChannelSubBase._remoteSurferSound', context: context);

  @override
  String get _remoteSurferSound {
    _$_remoteSurferSoundAtom.reportRead();
    return super._remoteSurferSound;
  }

  @override
  set _remoteSurferSound(String value) {
    _$_remoteSurferSoundAtom.reportWrite(value, super._remoteSurferSound, () {
      super._remoteSurferSound = value;
    });
  }

  late final _$_remoteUseSurferSoundAtom =
      Atom(name: 'ChannelSubBase._remoteUseSurferSound', context: context);

  @override
  bool get _remoteUseSurferSound {
    _$_remoteUseSurferSoundAtom.reportRead();
    return super._remoteUseSurferSound;
  }

  @override
  set _remoteUseSurferSound(bool value) {
    _$_remoteUseSurferSoundAtom.reportWrite(value, super._remoteUseSurferSound,
        () {
      super._remoteUseSurferSound = value;
    });
  }

  late final _$ChannelSubBaseActionController =
      ActionController(name: 'ChannelSubBase', context: context);

  @override
  dynamic unpackFromApi(Map<String, dynamic> body) {
    final _$actionInfo = _$ChannelSubBaseActionController.startAction(
        name: 'ChannelSubBase.unpackFromApi');
    try {
      return super.unpackFromApi(body);
    } finally {
      _$ChannelSubBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic loadFromAlt(ChannelSub alt) {
    final _$actionInfo = _$ChannelSubBaseActionController.startAction(
        name: 'ChannelSubBase.loadFromAlt');
    try {
      return super.loadFromAlt(alt);
    } finally {
      _$ChannelSubBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
channelId: ${channelId},
networkSubId: ${networkSubId},
networkId: ${networkId},
isSubscribed: ${isSubscribed},
syncEnabled: ${syncEnabled},
path: ${path},
isLinkedToSurfer: ${isLinkedToSurfer},
useRemote: ${useRemote},
isUnsubscribed: ${isUnsubscribed},
isSilenced: ${isSilenced},
surferSound: ${surferSound},
useSurferSound: ${useSurferSound}
    ''';
  }
}
