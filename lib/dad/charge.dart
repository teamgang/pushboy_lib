import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

part 'charge.g.dart';

enum ChargeType { none, plan, custom, limitless, static_, editToken }

extension FotTitleExtension on ChargeType {
  String get forTitle {
    switch (this) {
      case ChargeType.none:
        return 'None';
      case ChargeType.plan:
        return 'Plan Change';
      case ChargeType.custom:
        return 'Custom Notifications';
      case ChargeType.limitless:
        return 'Limitless Notifications';
      case ChargeType.static_:
        return 'Static Notifications';
      case ChargeType.editToken:
        return 'Edit Token';
    }
  }
}

class Charge = ChargeBase with _$Charge;

abstract class ChargeBase with Store {
  @observable
  String chargeId = '';
  @observable
  String boiId = '';
  @observable
  ChargeType chargeType = ChargeType.none;
  @observable
  String currency = '';
  @observable
  int period = 0;
  @observable
  int planId = 0;
  @observable
  int price = 0;
  @observable
  String projectId = '';
  @observable
  int timeCreated = 0;
  @observable
  int units = 0;

  bool isLoaded = false;

  void unpackFromApi(Map<String, dynamic> body) {
    chargeId = readString(body, 'charge_id');
    boiId = readString(body, 'boi_id');
    currency = readString(body, 'currency');
    period = readInt(body, 'period');
    planId = readInt(body, 'plan_id');
    price = readInt(body, 'price');
    projectId = readString(body, 'project_id');
    timeCreated = readInt(body, 'time_created');
    units = readInt(body, 'units');

    // for some reason charge_type fails to decode as an int
    chargeType = stringToChargeType(readString(body, 'charge_type'));

    isLoaded = true;
  }

  Future<void> loadFromApi(
    BuildContext context,
    String loadChargeId,
  ) async {
    if (loadChargeId == '') {
      return;
    }

    var king = King.of(context);
    ApiResponse ares = await king.lip.api(
      EndpointsV1.chargeGetById,
      payload: {'charge_id': loadChargeId},
    );
    var body = ares.body;
    unpackFromApi(body);
  }

  @computed
  String get title {
    return 'Charged at ${stampToDateAndTime(timeCreated)} for ${chargeType.forTitle}';
  }

  @computed
  bool get isBoiCharge {
    switch (chargeType) {
      case ChargeType.none:
      case ChargeType.plan:
      case ChargeType.editToken:
        return false;

      case ChargeType.custom:
      case ChargeType.limitless:
      case ChargeType.static_:
        return true;
    }
  }

  ChargeType stringToChargeType(String jin) {
    switch (jin) {
      case '1':
        return ChargeType.plan;
      case '2':
        return ChargeType.custom;
      case '3':
        return ChargeType.limitless;
      case '4':
        return ChargeType.static_;
      case '5':
        return ChargeType.editToken;
      default:
        return ChargeType.none;
    }
  }
}
