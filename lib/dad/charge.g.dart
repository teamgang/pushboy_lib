// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'charge.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$Charge on ChargeBase, Store {
  Computed<String>? _$titleComputed;

  @override
  String get title => (_$titleComputed ??=
          Computed<String>(() => super.title, name: 'ChargeBase.title'))
      .value;
  Computed<bool>? _$isBoiChargeComputed;

  @override
  bool get isBoiCharge =>
      (_$isBoiChargeComputed ??= Computed<bool>(() => super.isBoiCharge,
              name: 'ChargeBase.isBoiCharge'))
          .value;

  late final _$chargeIdAtom =
      Atom(name: 'ChargeBase.chargeId', context: context);

  @override
  String get chargeId {
    _$chargeIdAtom.reportRead();
    return super.chargeId;
  }

  @override
  set chargeId(String value) {
    _$chargeIdAtom.reportWrite(value, super.chargeId, () {
      super.chargeId = value;
    });
  }

  late final _$boiIdAtom = Atom(name: 'ChargeBase.boiId', context: context);

  @override
  String get boiId {
    _$boiIdAtom.reportRead();
    return super.boiId;
  }

  @override
  set boiId(String value) {
    _$boiIdAtom.reportWrite(value, super.boiId, () {
      super.boiId = value;
    });
  }

  late final _$chargeTypeAtom =
      Atom(name: 'ChargeBase.chargeType', context: context);

  @override
  ChargeType get chargeType {
    _$chargeTypeAtom.reportRead();
    return super.chargeType;
  }

  @override
  set chargeType(ChargeType value) {
    _$chargeTypeAtom.reportWrite(value, super.chargeType, () {
      super.chargeType = value;
    });
  }

  late final _$currencyAtom =
      Atom(name: 'ChargeBase.currency', context: context);

  @override
  String get currency {
    _$currencyAtom.reportRead();
    return super.currency;
  }

  @override
  set currency(String value) {
    _$currencyAtom.reportWrite(value, super.currency, () {
      super.currency = value;
    });
  }

  late final _$periodAtom = Atom(name: 'ChargeBase.period', context: context);

  @override
  int get period {
    _$periodAtom.reportRead();
    return super.period;
  }

  @override
  set period(int value) {
    _$periodAtom.reportWrite(value, super.period, () {
      super.period = value;
    });
  }

  late final _$planIdAtom = Atom(name: 'ChargeBase.planId', context: context);

  @override
  int get planId {
    _$planIdAtom.reportRead();
    return super.planId;
  }

  @override
  set planId(int value) {
    _$planIdAtom.reportWrite(value, super.planId, () {
      super.planId = value;
    });
  }

  late final _$priceAtom = Atom(name: 'ChargeBase.price', context: context);

  @override
  int get price {
    _$priceAtom.reportRead();
    return super.price;
  }

  @override
  set price(int value) {
    _$priceAtom.reportWrite(value, super.price, () {
      super.price = value;
    });
  }

  late final _$projectIdAtom =
      Atom(name: 'ChargeBase.projectId', context: context);

  @override
  String get projectId {
    _$projectIdAtom.reportRead();
    return super.projectId;
  }

  @override
  set projectId(String value) {
    _$projectIdAtom.reportWrite(value, super.projectId, () {
      super.projectId = value;
    });
  }

  late final _$timeCreatedAtom =
      Atom(name: 'ChargeBase.timeCreated', context: context);

  @override
  int get timeCreated {
    _$timeCreatedAtom.reportRead();
    return super.timeCreated;
  }

  @override
  set timeCreated(int value) {
    _$timeCreatedAtom.reportWrite(value, super.timeCreated, () {
      super.timeCreated = value;
    });
  }

  late final _$unitsAtom = Atom(name: 'ChargeBase.units', context: context);

  @override
  int get units {
    _$unitsAtom.reportRead();
    return super.units;
  }

  @override
  set units(int value) {
    _$unitsAtom.reportWrite(value, super.units, () {
      super.units = value;
    });
  }

  @override
  String toString() {
    return '''
chargeId: ${chargeId},
boiId: ${boiId},
chargeType: ${chargeType},
currency: ${currency},
period: ${period},
planId: ${planId},
price: ${price},
projectId: ${projectId},
timeCreated: ${timeCreated},
units: ${units},
title: ${title},
isBoiCharge: ${isBoiCharge}
    ''';
  }
}
