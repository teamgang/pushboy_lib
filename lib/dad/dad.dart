import 'package:mobx/mobx.dart';

import 'package:pushboy_lib/consts/endpoints.dart';
import 'package:pushboy_lib/king.dart';
import 'package:pushboy_lib/services/lip.dart';

import 'package:pushboy_lib/dad/bill.dart';
import 'package:pushboy_lib/dad/boi.dart';
import 'package:pushboy_lib/dad/cache.dart';
import 'package:pushboy_lib/dad/channel.dart';
import 'package:pushboy_lib/dad/charge.dart';
import 'package:pushboy_lib/dad/network.dart';
import 'package:pushboy_lib/dad/network_sub.dart';
import 'package:pushboy_lib/dad/channel_sub.dart';
import 'package:pushboy_lib/dad/legal.dart';
import 'package:pushboy_lib/dad/payment.dart';
import 'package:pushboy_lib/dad/plan.dart';
import 'package:pushboy_lib/dad/project.dart';
import 'package:pushboy_lib/dad/stripe_payment_methods.dart';

export 'package:pushboy_lib/dad/bill.dart';
export 'package:pushboy_lib/dad/boi.dart';
export 'package:pushboy_lib/dad/boi_sender.dart';
export 'package:pushboy_lib/dad/boi_tx.dart';
export 'package:pushboy_lib/dad/channel.dart';
export 'package:pushboy_lib/dad/channel_sub.dart';
export 'package:pushboy_lib/dad/charge.dart';
export 'package:pushboy_lib/dad/developer.dart';
export 'package:pushboy_lib/dad/legal.dart';
export 'package:pushboy_lib/dad/network.dart';
export 'package:pushboy_lib/dad/network_sub.dart';
export 'package:pushboy_lib/dad/payment.dart';
export 'package:pushboy_lib/dad/plan.dart';
export 'package:pushboy_lib/dad/project.dart';
export 'package:pushboy_lib/dad/request_change.dart';
export 'package:pushboy_lib/dad/stripe_payment_methods.dart';
export 'package:pushboy_lib/dad/surfer.dart';

class Dad {
  final King king;
  // channelId to list of bois
  //final ObservableMap<String, ObservableList<Boi>> bois = ObservableMap();
  late final Bois bois;
  final ObservableList<Bill> bills = ObservableList();
  late final Channels channels;
  final ObservableList<Charge> charges = ObservableList();
  late final Networks networks;
  final Plan currentPlan = Plan();
  final PaymentMethods paymentMethods = PaymentMethods();
  final ObservableList<Payment> payments = ObservableList();
  final ObservableMap<int, Plan> plans = ObservableMap();
  final ObservableMap<String, Project> projects = ObservableMap();
  final Legal legal = Legal();

  late final Cache<ChannelSub> channelSubCache;
  late final Cache<ObservableList<String>> channelSubIdsByNetworkSubIdCache;
  late final ChannelSubProxy channelSubProxy;

  late final Cache<NetworkSub> networkSubCache;
  late final Cache<ObservableList<String>> networkSubIdsByInstanceIdCache;
  late final NetworkSubProxy networkSubProxy;

  Dad(this.king) {
    bois = Bois(king);
    channels = Channels(king);
    networks = Networks(king);

    channelSubCache = Cache<ChannelSub>(king, ChannelSub.new);
    channelSubIdsByNetworkSubIdCache =
        Cache<ObservableList<String>>(king, ObservableList.new);
    channelSubProxy = ChannelSubProxy(
      channelSubCache: channelSubCache,
      channelSubIdsByNetworkSubIdCache: channelSubIdsByNetworkSubIdCache,
      king: king,
    );

    networkSubCache = Cache<NetworkSub>(king, NetworkSub.new);
    networkSubIdsByInstanceIdCache =
        Cache<ObservableList<String>>(king, ObservableList.new);
    networkSubProxy = NetworkSubProxy(
      networkSubCache: networkSubCache,
      networkSubIdsByInstanceIdCache: networkSubIdsByInstanceIdCache,
      king: king,
    );
  }

  Future<void> fetchProjects() async {
    if (king.todd.developerId.isEmpty) {
      print('not fetching; no developerId');
      return;
    }
    ApiResponse ares = await king.lip.api(
      EndpointsV1.projectsGetByDeveloperId,
      payload: {'developer_id': king.todd.developerId},
    );
    projects.clear();
    if (ares.body.containsKey('collection')) {
      for (final item in ares.body['collection']) {
        var project = Project();
        project.unpackFromApi(item);
        if (projects.containsKey(project.projectId)) {
          projects[project.projectId]?.loadFromAltProject(project);
        } else {
          projects[project.projectId] = project;
        }
      }
    }
  }

  Future<void> fetchCurrentPlan(int currentPlanId) async {
    ApiResponse ares = await king.lip.api(
      EndpointsV1.planGetById,
      payload: {'plan_id': currentPlanId},
    );
    currentPlan.unpackFromApi(ares.body);
  }

  Future<void> fetchPlanChoicesWithCurrentPlan(int currentPlanId) async {
    ApiResponse ares = await king.lip.api(
      EndpointsV1.plansGetChoices,
      payload: {'plan_id': currentPlanId},
    );
    if (ares.body.containsKey('collection')) {
      for (final item in ares.body['collection']) {
        var plan = Plan();
        plan.unpackFromApi(item);
        if (plans.containsKey(plan.planId)) {
          plans[plan.planId]?.loadFromAltPlan(plan);
        } else {
          plans[plan.planId] = plan;
        }
      }
    }
  }

  ObservableList<Plan> planChoicesWithCurrentPlan(int currentPlanId) {
    ObservableList<Plan> planChoices = ObservableList();
    var planChoicesIndices = [1, 2, 3, 4];
    if (!planChoicesIndices.contains(currentPlanId) && currentPlanId != 0) {
      planChoicesIndices.insert(0, currentPlanId);
    }

    for (var index in planChoicesIndices) {
      var plan = plans[index];
      if (plan == null) {
        plan = Plan();
        plans[index] = plan;
      }
      planChoices.add(plan);
    }
    return planChoices;
  }

  Plan getPlan(int planId) {
    Plan plan = plans[planId] ?? Plan();
    if (!plans.containsKey(planId)) {
      plan.planId = planId;
      plans[planId] = plan;
    }
    plan.loadFromApi(king, planId);
    return plan;
  }

  Plan getPlanFromId(int planId) {
    var plan = Plan();
    plan.loadFromApi(king, planId);
    return plan;
  }

  void getPaymentMethods() {
    paymentMethods.loadFromStripe(king);
  }

  Future<void> loadBillsOfProject(String projectId) async {
    if (projectId == '') {
      return;
    }
    ApiResponse ares = await king.lip.api(
      EndpointsV1.billsGetByProjectId,
      payload: {'project_id': projectId},
    );

    bills.clear();
    if (ares.body.containsKey('collection')) {
      for (final item in ares.body['collection'] ?? []) {
        var bill = Bill();
        bill.unpackFromApi(item);
        bills.add(bill);
      }
    }
  }

  Future<void> loadChargesOfBill(String billId) async {
    if (billId == '') {
      return;
    }
    ApiResponse ares = await king.lip.api(
      EndpointsV1.chargesGetByBillId,
      payload: {'bill_id': billId},
    );

    charges.clear();
    if (ares.body.containsKey('collection')) {
      for (final item in ares.body['collection'] ?? []) {
        var charge = Charge();
        charge.unpackFromApi(item);
        charges.add(charge);
      }
    }
  }

  Future<void> loadPaymentsOfProject(
    String projectId,
    PaymentAttemptType type,
  ) async {
    if (projectId == '') {
      return;
    }
    ApiResponse ares;
    switch (type) {
      case PaymentAttemptType.successful:
        ares = await king.lip.api(
          EndpointsV1.paymentsGetSuccessfulByProjectId,
          payload: {'project_id': projectId},
        );
        break;
      case PaymentAttemptType.ccAttempt:
        ares = await king.lip.api(
          EndpointsV1.paymentsGetCcAttemptsByProjectId,
          payload: {'project_id': projectId},
        );
        break;
      case PaymentAttemptType.cryptoAttempt:
        throw Exception('unimplemented PaymentAttempt type cryptoAttempt');
    }

    payments.clear();
    if (ares.body.containsKey('collection')) {
      for (final item in ares.body['collection'] ?? []) {
        var payment = Payment();
        payment.unpackFromApi(item);
        payments.add(payment);
      }
    }
  }
}
