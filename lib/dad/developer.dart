import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

part 'developer.g.dart';

class Developer = DeveloperBase with _$Developer;

abstract class DeveloperBase with Store {
  @observable
  String developerId = '';
  @observable
  String email = '';
  @observable
  String phone = '';

  Future<void> loadFromApi(
    BuildContext context,
    String developerId,
  ) async {
    if (developerId == '') {
      return;
    }

    var king = King.of(context);
    print(king.todd.developerId);
    ApiResponse ares = await king.lip.api(
      EndpointsV1.developerGetById,
      payload: {'developer_id': developerId},
    );
    var body = ares.body;
    unpackFromApi(body);
  }

  void unpackFromApi(Map<String, dynamic> body) {
    var temp = readString(body, 'developer_id');
    if (developerId != temp) {
      developerId = temp;
    }
    temp = readString(body, 'email');
    if (email != temp) {
      email = temp;
    }
    temp = readString(body, 'phone');
    if (phone != temp) {
      phone = temp;
    }
  }

  void loadFromAltDeveloper(Developer alt) {
    developerId = alt.developerId;
    email = alt.email;
    phone = alt.phone;
  }
}
