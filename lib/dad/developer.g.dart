// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'developer.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$Developer on DeveloperBase, Store {
  late final _$developerIdAtom =
      Atom(name: 'DeveloperBase.developerId', context: context);

  @override
  String get developerId {
    _$developerIdAtom.reportRead();
    return super.developerId;
  }

  @override
  set developerId(String value) {
    _$developerIdAtom.reportWrite(value, super.developerId, () {
      super.developerId = value;
    });
  }

  late final _$emailAtom = Atom(name: 'DeveloperBase.email', context: context);

  @override
  String get email {
    _$emailAtom.reportRead();
    return super.email;
  }

  @override
  set email(String value) {
    _$emailAtom.reportWrite(value, super.email, () {
      super.email = value;
    });
  }

  late final _$phoneAtom = Atom(name: 'DeveloperBase.phone', context: context);

  @override
  String get phone {
    _$phoneAtom.reportRead();
    return super.phone;
  }

  @override
  set phone(String value) {
    _$phoneAtom.reportWrite(value, super.phone, () {
      super.phone = value;
    });
  }

  @override
  String toString() {
    return '''
developerId: ${developerId},
email: ${email},
phone: ${phone}
    ''';
  }
}
