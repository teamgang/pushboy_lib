import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

part 'legal.g.dart';

class LegalDoc = LegalDocBase with _$LegalDoc;

abstract class LegalDocBase with Store {
  @observable
  String audience = '';
  @observable
  String policy = '';
  @observable
  String dated = '';
  @observable
  String fulltext = '';

  Future<void> loadLatestFromApi(
    BuildContext context,
    String audience,
    String policy,
  ) async {
    String path = 'v1/$audience/$policy.latest';

    var king = King.of(context);
    print(king.todd.developerId);
    ApiResponse ares = await king.lip.api(
      path,
      method: HttpMethod.get,
    );
    var body = ares.body;
    unpackFromApi(body);
  }

  void unpackFromApi(Map<String, dynamic> body) {
    audience = readString(body, 'audience');
    policy = readString(body, 'policy');
    dated = readString(body, 'dated');
    fulltext = readString(body, 'fulltext');
  }
}

class Legal = LegalBase with _$Legal;

abstract class LegalBase with Store {
  @observable
  String developerPrivacy = '';
  @observable
  String developerPrivacyVersion = '';
  @observable
  String developerTerms = '';
  @observable
  String developerTermsVersion = '';
  @observable
  String surferPrivacy = '';
  @observable
  String surferPrivacyVersion = '';

  bool isLoaded = false;

  Future<void> loadFromApi(BuildContext context) async {
    var king = King.of(context);

    ApiResponse ares1 = await king.lip.api(EndpointsV1.developerPrivacy);
    developerPrivacy = readString(ares1.body, 'fulltext');
    developerPrivacyVersion = readString(ares1.body, 'dated');

    ApiResponse ares2 = await king.lip.api(EndpointsV1.developerTerms);
    developerTerms = readString(ares2.body, 'fulltext');
    developerTermsVersion = readString(ares2.body, 'dated');

    ApiResponse ares3 = await king.lip.api(EndpointsV1.developerTerms);
    developerTerms = readString(ares3.body, 'fulltext');
    developerTermsVersion = readString(ares3.body, 'dated');

    isLoaded = true;
  }
}
