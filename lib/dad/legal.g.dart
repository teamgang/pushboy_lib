// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'legal.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$LegalDoc on LegalDocBase, Store {
  late final _$audienceAtom =
      Atom(name: 'LegalDocBase.audience', context: context);

  @override
  String get audience {
    _$audienceAtom.reportRead();
    return super.audience;
  }

  @override
  set audience(String value) {
    _$audienceAtom.reportWrite(value, super.audience, () {
      super.audience = value;
    });
  }

  late final _$policyAtom = Atom(name: 'LegalDocBase.policy', context: context);

  @override
  String get policy {
    _$policyAtom.reportRead();
    return super.policy;
  }

  @override
  set policy(String value) {
    _$policyAtom.reportWrite(value, super.policy, () {
      super.policy = value;
    });
  }

  late final _$datedAtom = Atom(name: 'LegalDocBase.dated', context: context);

  @override
  String get dated {
    _$datedAtom.reportRead();
    return super.dated;
  }

  @override
  set dated(String value) {
    _$datedAtom.reportWrite(value, super.dated, () {
      super.dated = value;
    });
  }

  late final _$fulltextAtom =
      Atom(name: 'LegalDocBase.fulltext', context: context);

  @override
  String get fulltext {
    _$fulltextAtom.reportRead();
    return super.fulltext;
  }

  @override
  set fulltext(String value) {
    _$fulltextAtom.reportWrite(value, super.fulltext, () {
      super.fulltext = value;
    });
  }

  @override
  String toString() {
    return '''
audience: ${audience},
policy: ${policy},
dated: ${dated},
fulltext: ${fulltext}
    ''';
  }
}

mixin _$Legal on LegalBase, Store {
  late final _$developerPrivacyAtom =
      Atom(name: 'LegalBase.developerPrivacy', context: context);

  @override
  String get developerPrivacy {
    _$developerPrivacyAtom.reportRead();
    return super.developerPrivacy;
  }

  @override
  set developerPrivacy(String value) {
    _$developerPrivacyAtom.reportWrite(value, super.developerPrivacy, () {
      super.developerPrivacy = value;
    });
  }

  late final _$developerPrivacyVersionAtom =
      Atom(name: 'LegalBase.developerPrivacyVersion', context: context);

  @override
  String get developerPrivacyVersion {
    _$developerPrivacyVersionAtom.reportRead();
    return super.developerPrivacyVersion;
  }

  @override
  set developerPrivacyVersion(String value) {
    _$developerPrivacyVersionAtom
        .reportWrite(value, super.developerPrivacyVersion, () {
      super.developerPrivacyVersion = value;
    });
  }

  late final _$developerTermsAtom =
      Atom(name: 'LegalBase.developerTerms', context: context);

  @override
  String get developerTerms {
    _$developerTermsAtom.reportRead();
    return super.developerTerms;
  }

  @override
  set developerTerms(String value) {
    _$developerTermsAtom.reportWrite(value, super.developerTerms, () {
      super.developerTerms = value;
    });
  }

  late final _$developerTermsVersionAtom =
      Atom(name: 'LegalBase.developerTermsVersion', context: context);

  @override
  String get developerTermsVersion {
    _$developerTermsVersionAtom.reportRead();
    return super.developerTermsVersion;
  }

  @override
  set developerTermsVersion(String value) {
    _$developerTermsVersionAtom.reportWrite(value, super.developerTermsVersion,
        () {
      super.developerTermsVersion = value;
    });
  }

  late final _$surferPrivacyAtom =
      Atom(name: 'LegalBase.surferPrivacy', context: context);

  @override
  String get surferPrivacy {
    _$surferPrivacyAtom.reportRead();
    return super.surferPrivacy;
  }

  @override
  set surferPrivacy(String value) {
    _$surferPrivacyAtom.reportWrite(value, super.surferPrivacy, () {
      super.surferPrivacy = value;
    });
  }

  late final _$surferPrivacyVersionAtom =
      Atom(name: 'LegalBase.surferPrivacyVersion', context: context);

  @override
  String get surferPrivacyVersion {
    _$surferPrivacyVersionAtom.reportRead();
    return super.surferPrivacyVersion;
  }

  @override
  set surferPrivacyVersion(String value) {
    _$surferPrivacyVersionAtom.reportWrite(value, super.surferPrivacyVersion,
        () {
      super.surferPrivacyVersion = value;
    });
  }

  @override
  String toString() {
    return '''
developerPrivacy: ${developerPrivacy},
developerPrivacyVersion: ${developerPrivacyVersion},
developerTerms: ${developerTerms},
developerTermsVersion: ${developerTermsVersion},
surferPrivacy: ${surferPrivacy},
surferPrivacyVersion: ${surferPrivacyVersion}
    ''';
  }
}
