import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';

import 'package:pushboy_lib/consts/endpoints.dart';
import 'package:pushboy_lib/king.dart';
import 'package:pushboy_lib/helpers/parsers.dart';
import 'package:pushboy_lib/services/lip.dart';

part 'network.g.dart';

class Networks {
  final ObservableMap<String, Network> byId = ObservableMap();
  final ObservableMap<String, ObservableList<Network>> byProject =
      ObservableMap();
  King king;
  Networks(this.king);

  Network getNetworkById(String projectId) {
    if (!byId.containsKey(projectId)) {
      byId[projectId] = Network();
    }
    getNetworkByIdFromApi(projectId);
    return byId[projectId] ?? Network();
  }

  Future<void> getNetworkByIdFromApi(String projectId) async {
    if (projectId == '') {
      return;
    }
    ApiResponse ares = await king.lip.api(
      EndpointsV1.networkGetById,
      payload: {'network_id': projectId},
    );

    final network = byId[projectId];
    network!.unpackFromApi(ares.body);
  }

  void refreshNetworksOfProject(String projectId) async {
    byProject[projectId]!.clear();
    getNetworksOfProject(projectId);
  }

  ObservableList<Network> getNetworksOfProject(String projectId) {
    if (!byProject.containsKey(projectId)) {
      ObservableList<Network> listOfNetworks = ObservableList();
      byProject[projectId] = listOfNetworks;
    }
    getNetworksOfProjectFromApi(projectId);
    return byProject[projectId] ?? ObservableList();
  }

  Future<void> getNetworksOfProjectFromApi(String projectId) async {
    ApiResponse ares = await king.lip.api(
      EndpointsV1.networksGetByProjectId,
      payload: {'project_id': projectId},
    );

    final listOfNetworks = byProject[projectId] ?? ObservableList();
    listOfNetworks.clear();
    if (ares.body.containsKey('collection')) {
      for (final item in ares.body['collection'] ?? []) {
        var network = Network();
        network.unpackFromApi(item);
        listOfNetworks.add(network);
      }
    }
  }
}

class Network = NetworkBase with _$Network;

abstract class NetworkBase with Store {
  @observable
  ObservableList<NetworkToken> tokens = ObservableList();
  @observable
  String abbrev = '';
  @observable
  String colorPrimaryString = '';
  @observable
  String colorSecondaryString = '';
  @observable
  String description = '';
  @observable
  String name = '';
  @observable
  String networkId = '';
  @observable
  String notes = '';
  @observable
  String page1 = '';
  @observable
  String page2 = '';
  @observable
  String projectId = '';
  @observable
  String route = '';
  @observable
  String thumbAbbrev = '';
  @observable
  String thumbNetwork = '';
  @observable
  String thumbSelected = '';
  bool isLoaded = false;

  @action
  void clear() {
    tokens.clear();
    abbrev = '';
    colorPrimaryString = '';
    colorSecondaryString = '';
    description = '';
    name = '';
    networkId = '';
    notes = '';
    page1 = '';
    page2 = '';
    projectId = '';
    route = '';
    thumbAbbrev = '';
    thumbNetwork = '';
    thumbSelected = '';
  }

  Future<void> loadFromApi(BuildContext context, String networkId_) async {
    if (networkId_ == '') {
      return;
    }
    ApiResponse ares = await King.of(context).lip.api(
      EndpointsV1.networkGetById,
      payload: {'network_id': networkId_},
    );
    unpackFromApi(ares.body);
  }

  Future<void> loadFromApiWithRoute(
    BuildContext context,
    String route_, {
    bool clearData = false,
  }) async {
    if (clearData) {
      clear();
    }
    if (route_ == '') {
      return;
    }
    ApiResponse ares = await King.of(context).lip.api(
      EndpointsV1.networkGetByRoute,
      payload: {'route': route_},
    );
    if (ares.isOk) {
      unpackFromApi(ares.body);
    }
  }

  @action
  void unpackFromApi(Map<String, dynamic> body) {
    abbrev = readString(body, 'abbrev');
    colorPrimaryString = readString(body, 'color_primary');
    colorSecondaryString = readString(body, 'color_secondary');
    description = readString(body, 'description');
    name = readString(body, 'name');
    networkId = readString(body, 'network_id');
    name = readString(body, 'name');
    notes = readString(body, 'notes');
    page1 = readString(body, 'page1');
    page2 = readString(body, 'page2');
    projectId = readString(body, 'project_id');
    route = readString(body, 'route');
    thumbAbbrev = readString(body, 'thumb_abbrev');
    thumbNetwork = readString(body, 'thumb_network');
    thumbSelected = readString(body, 'thumb_selected');
    isLoaded = true;
  }

  @action
  void loadFromAltNetwork(Network alt) {
    abbrev = alt.abbrev;
    colorPrimaryString = alt.colorPrimaryString;
    colorSecondaryString = alt.colorSecondaryString;
    description = alt.description;
    name = alt.name;
    networkId = alt.networkId;
    name = alt.name;
    notes = alt.notes;
    page1 = alt.page1;
    page2 = alt.page2;
    projectId = alt.projectId;
    route = alt.route;
    thumbAbbrev = alt.thumbAbbrev;
    thumbNetwork = alt.thumbNetwork;
    thumbSelected = alt.thumbSelected;
    isLoaded = true;
  }

  @action
  Future<void> loadTokensFromApi(BuildContext context) async {
    if (networkId == '') {
      return;
    }
    var king = King.of(context);
    ApiResponse ares = await king.lip.api(
      EndpointsV1.networkTokensGetByNetworkId,
      payload: {'network_id': networkId},
    );
    tokens.clear();
    if (ares.body.containsKey('collection')) {
      for (final item in ares.body['collection']) {
        var token = NetworkToken(
          networkId: item['network_id'],
          token: item['token'],
        );
        tokens.add(token);
      }
    }
  }

  @computed
  Color get colorPrimary {
    return colorFromString(colorPrimaryString);
  }

  @computed
  Color get colorSecondary {
    return colorFromString(colorSecondaryString);
  }

  @computed
  String get thumbSelectedUrl {
    if (thumbSelected == 'abbrev') {
      return thumbAbbrev;
    } else if (thumbSelected == 'net') {
      return thumbNetwork;
    } else {
      return thumbSelected;
    }
  }
}

class NetworkToken {
  NetworkToken({required this.networkId, required this.token});
  String networkId;
  String token;
}
