// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'network.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$Network on NetworkBase, Store {
  Computed<Color>? _$colorPrimaryComputed;

  @override
  Color get colorPrimary =>
      (_$colorPrimaryComputed ??= Computed<Color>(() => super.colorPrimary,
              name: 'NetworkBase.colorPrimary'))
          .value;
  Computed<Color>? _$colorSecondaryComputed;

  @override
  Color get colorSecondary =>
      (_$colorSecondaryComputed ??= Computed<Color>(() => super.colorSecondary,
              name: 'NetworkBase.colorSecondary'))
          .value;
  Computed<String>? _$thumbSelectedUrlComputed;

  @override
  String get thumbSelectedUrl => (_$thumbSelectedUrlComputed ??=
          Computed<String>(() => super.thumbSelectedUrl,
              name: 'NetworkBase.thumbSelectedUrl'))
      .value;

  late final _$tokensAtom = Atom(name: 'NetworkBase.tokens', context: context);

  @override
  ObservableList<NetworkToken> get tokens {
    _$tokensAtom.reportRead();
    return super.tokens;
  }

  @override
  set tokens(ObservableList<NetworkToken> value) {
    _$tokensAtom.reportWrite(value, super.tokens, () {
      super.tokens = value;
    });
  }

  late final _$abbrevAtom = Atom(name: 'NetworkBase.abbrev', context: context);

  @override
  String get abbrev {
    _$abbrevAtom.reportRead();
    return super.abbrev;
  }

  @override
  set abbrev(String value) {
    _$abbrevAtom.reportWrite(value, super.abbrev, () {
      super.abbrev = value;
    });
  }

  late final _$colorPrimaryStringAtom =
      Atom(name: 'NetworkBase.colorPrimaryString', context: context);

  @override
  String get colorPrimaryString {
    _$colorPrimaryStringAtom.reportRead();
    return super.colorPrimaryString;
  }

  @override
  set colorPrimaryString(String value) {
    _$colorPrimaryStringAtom.reportWrite(value, super.colorPrimaryString, () {
      super.colorPrimaryString = value;
    });
  }

  late final _$colorSecondaryStringAtom =
      Atom(name: 'NetworkBase.colorSecondaryString', context: context);

  @override
  String get colorSecondaryString {
    _$colorSecondaryStringAtom.reportRead();
    return super.colorSecondaryString;
  }

  @override
  set colorSecondaryString(String value) {
    _$colorSecondaryStringAtom.reportWrite(value, super.colorSecondaryString,
        () {
      super.colorSecondaryString = value;
    });
  }

  late final _$descriptionAtom =
      Atom(name: 'NetworkBase.description', context: context);

  @override
  String get description {
    _$descriptionAtom.reportRead();
    return super.description;
  }

  @override
  set description(String value) {
    _$descriptionAtom.reportWrite(value, super.description, () {
      super.description = value;
    });
  }

  late final _$nameAtom = Atom(name: 'NetworkBase.name', context: context);

  @override
  String get name {
    _$nameAtom.reportRead();
    return super.name;
  }

  @override
  set name(String value) {
    _$nameAtom.reportWrite(value, super.name, () {
      super.name = value;
    });
  }

  late final _$networkIdAtom =
      Atom(name: 'NetworkBase.networkId', context: context);

  @override
  String get networkId {
    _$networkIdAtom.reportRead();
    return super.networkId;
  }

  @override
  set networkId(String value) {
    _$networkIdAtom.reportWrite(value, super.networkId, () {
      super.networkId = value;
    });
  }

  late final _$notesAtom = Atom(name: 'NetworkBase.notes', context: context);

  @override
  String get notes {
    _$notesAtom.reportRead();
    return super.notes;
  }

  @override
  set notes(String value) {
    _$notesAtom.reportWrite(value, super.notes, () {
      super.notes = value;
    });
  }

  late final _$page1Atom = Atom(name: 'NetworkBase.page1', context: context);

  @override
  String get page1 {
    _$page1Atom.reportRead();
    return super.page1;
  }

  @override
  set page1(String value) {
    _$page1Atom.reportWrite(value, super.page1, () {
      super.page1 = value;
    });
  }

  late final _$page2Atom = Atom(name: 'NetworkBase.page2', context: context);

  @override
  String get page2 {
    _$page2Atom.reportRead();
    return super.page2;
  }

  @override
  set page2(String value) {
    _$page2Atom.reportWrite(value, super.page2, () {
      super.page2 = value;
    });
  }

  late final _$projectIdAtom =
      Atom(name: 'NetworkBase.projectId', context: context);

  @override
  String get projectId {
    _$projectIdAtom.reportRead();
    return super.projectId;
  }

  @override
  set projectId(String value) {
    _$projectIdAtom.reportWrite(value, super.projectId, () {
      super.projectId = value;
    });
  }

  late final _$routeAtom = Atom(name: 'NetworkBase.route', context: context);

  @override
  String get route {
    _$routeAtom.reportRead();
    return super.route;
  }

  @override
  set route(String value) {
    _$routeAtom.reportWrite(value, super.route, () {
      super.route = value;
    });
  }

  late final _$thumbAbbrevAtom =
      Atom(name: 'NetworkBase.thumbAbbrev', context: context);

  @override
  String get thumbAbbrev {
    _$thumbAbbrevAtom.reportRead();
    return super.thumbAbbrev;
  }

  @override
  set thumbAbbrev(String value) {
    _$thumbAbbrevAtom.reportWrite(value, super.thumbAbbrev, () {
      super.thumbAbbrev = value;
    });
  }

  late final _$thumbNetworkAtom =
      Atom(name: 'NetworkBase.thumbNetwork', context: context);

  @override
  String get thumbNetwork {
    _$thumbNetworkAtom.reportRead();
    return super.thumbNetwork;
  }

  @override
  set thumbNetwork(String value) {
    _$thumbNetworkAtom.reportWrite(value, super.thumbNetwork, () {
      super.thumbNetwork = value;
    });
  }

  late final _$thumbSelectedAtom =
      Atom(name: 'NetworkBase.thumbSelected', context: context);

  @override
  String get thumbSelected {
    _$thumbSelectedAtom.reportRead();
    return super.thumbSelected;
  }

  @override
  set thumbSelected(String value) {
    _$thumbSelectedAtom.reportWrite(value, super.thumbSelected, () {
      super.thumbSelected = value;
    });
  }

  late final _$loadTokensFromApiAsyncAction =
      AsyncAction('NetworkBase.loadTokensFromApi', context: context);

  @override
  Future<void> loadTokensFromApi(BuildContext context) {
    return _$loadTokensFromApiAsyncAction
        .run(() => super.loadTokensFromApi(context));
  }

  late final _$NetworkBaseActionController =
      ActionController(name: 'NetworkBase', context: context);

  @override
  void clear() {
    final _$actionInfo =
        _$NetworkBaseActionController.startAction(name: 'NetworkBase.clear');
    try {
      return super.clear();
    } finally {
      _$NetworkBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void unpackFromApi(Map<String, dynamic> body) {
    final _$actionInfo = _$NetworkBaseActionController.startAction(
        name: 'NetworkBase.unpackFromApi');
    try {
      return super.unpackFromApi(body);
    } finally {
      _$NetworkBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  void loadFromAltNetwork(Network alt) {
    final _$actionInfo = _$NetworkBaseActionController.startAction(
        name: 'NetworkBase.loadFromAltNetwork');
    try {
      return super.loadFromAltNetwork(alt);
    } finally {
      _$NetworkBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
tokens: ${tokens},
abbrev: ${abbrev},
colorPrimaryString: ${colorPrimaryString},
colorSecondaryString: ${colorSecondaryString},
description: ${description},
name: ${name},
networkId: ${networkId},
notes: ${notes},
page1: ${page1},
page2: ${page2},
projectId: ${projectId},
route: ${route},
thumbAbbrev: ${thumbAbbrev},
thumbNetwork: ${thumbNetwork},
thumbSelected: ${thumbSelected},
colorPrimary: ${colorPrimary},
colorSecondary: ${colorSecondary},
thumbSelectedUrl: ${thumbSelectedUrl}
    ''';
  }
}
