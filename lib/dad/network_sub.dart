import 'dart:convert';
import 'package:mobx/mobx.dart';
import 'package:universal_io/io.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

part 'network_sub.g.dart';

class NetworkSubProxy {
  NetworkSubProxy({
    required this.networkSubCache,
    required this.networkSubIdsByInstanceIdCache,
    required this.king,
  });
  final Cache<NetworkSub> networkSubCache;
  final Cache<ObservableList<String>> networkSubIdsByInstanceIdCache;
  final King king;

  ObservableList<String> getNetworkSubIdsRef() {
    return networkSubIdsByInstanceIdCache.getItemRef(king.conf.firebaseToken);
  }

  NetworkSub getNetworkSubRef({required String networkSubId}) {
    //NOTE: we should always call this function instead of getItemRef
    var sub = networkSubCache.getItemRef(networkSubId);
    sub.loadLocal();
    return sub;
  }

  Future<ObservableList<String>> getAndLoadNetworkSubIdsForInstanceId() async {
    var instanceId = king.conf.firebaseToken;
    ApiResponse ares = await king.lip.api(
      EndpointsV1.networkSubsGetByInstanceIdAndSurferId,
      payload: {'instance_id': instanceId},
    );

    var idList = networkSubIdsByInstanceIdCache.getItemRef(instanceId);
    idList.clear();
    if (ares.body.containsKey('collection')) {
      for (final item in ares.body['collection'] ?? []) {
        var newData = NetworkSub();
        newData.unpackFromApi(item);

        var oldSub = getNetworkSubRef(networkSubId: newData.networkSubId);
        oldSub.loadFromAlt(newData);

        idList.add(newData.networkSubId);
      }
    }
    return idList;
  }
}

class NetworkSub = NetworkSubBase with _$NetworkSub;

abstract class NetworkSubBase with Store {
  King? king;

  @observable
  String networkSubId = '';
  @observable
  String surferOrInstanceId = '';
  @observable
  bool syncEnabled = true; // default true for no settings

  @observable
  bool _localIsSilenced = false;
  @observable
  bool _remoteIsSilenced = false;

  final Network network = Network();

  @computed
  String get path {
    return king!.conf.networkSubDir;
  }

  @computed
  bool get useRemote {
    return syncEnabled && (king?.todd.isSignedIn ?? false) && isLinkedToSurfer;
  }

  // not used in widgets
  bool get isIoWorking {
    return networkSubId.isNotEmpty;
  }

  @computed
  bool get isLinkedToSurfer {
    return (surferOrInstanceId.split('-').length == 5 &&
        surferOrInstanceId.length < 40);
  }

  @action
  toggleIsLinkedToSurfer() {
    if (isLinkedToSurfer) {
      surferOrInstanceId = king!.conf.firebaseToken;
    } else {
      surferOrInstanceId = king!.todd.surferId;
    }
  }

  // isSilenced
  @computed
  bool get isSilenced {
    if (useRemote) {
      return _remoteIsSilenced;
    } else {
      return _localIsSilenced;
    }
  }

  set isSilenced(bool value) {
    if (useRemote) {
      _remoteIsSilenced = value;
    } else {
      _localIsSilenced = value;
    }
  }

  syncValues() async {
    loadLocal();
    loadFromApi();
  }

  @action
  unpackFromApi(Map<String, dynamic> body) async {
    networkSubId = readString(body, 'network_sub_id');
    surferOrInstanceId = readString(body, 'surfer_or_instance_id');
    _remoteIsSilenced = readBool(body, 'is_silenced');

    final altNetwork = Network();
    altNetwork.unpackFromApi(readMap(body, 'network'));
    network.loadFromAltNetwork(altNetwork);
  }

  @action
  loadFromAlt(NetworkSub alt) {
    networkSubId = alt.networkSubId;
    surferOrInstanceId = alt.surferOrInstanceId;
    _localIsSilenced = alt._localIsSilenced;
    _remoteIsSilenced = alt._remoteIsSilenced;
    syncEnabled = alt.syncEnabled;

    network.loadFromAltNetwork(alt.network);
  }

  loadFromApi() async {
    ApiResponse ares = await king!.lip.api(
      EndpointsV1.networkSubGetById,
      payload: {
        'network_sub_id': networkSubId,
      },
    );

    unpackFromApi(ares.body);
  }

  Future<ApiResponse> saveToRemote() async {
    var ares = ApiResponse();
    if (syncEnabled) {
      // send new settings to api
      ares = await king!.lip.api(
        EndpointsV1.networkSubSettingsSaveToRemote,
        payload: {
          'is_silenced': _remoteIsSilenced,
          'surfer_or_instance_id': surferOrInstanceId,
          'network_sub_id': networkSubId,
        },
      );
    }
    return ares;
  }

  bool deepEquals(NetworkSub alt) {
    if (_localIsSilenced == alt._localIsSilenced &&
        _remoteIsSilenced == alt._remoteIsSilenced &&
        surferOrInstanceId == alt.surferOrInstanceId &&
        syncEnabled == alt.syncEnabled) {
      return true;
    }
    return false;
  }

  Map<String, dynamic> asJson() {
    return {
      'localIsSilenced': _localIsSilenced,
      'syncEnabled': syncEnabled,
    };
  }

  loadLocal() async {
    if (isIoWorking) {
      final file = await getFile();
      try {
        String contents = await file.readAsString();
        if (contents.isEmpty) {
          setDefaults();
          return;
        }
        var jason = jsonDecode(contents);
        _localIsSilenced = jason['localIsSilenced'];
        syncEnabled = jason['syncEnabled'];
      } catch (e, s) {
        king!.log.e('Failed to read from in loadLocal:\n$e\n$s');
        setDefaults();
        await file.delete();
        saveLocal();
      }
    } else {
      setDefaults();
    }
  }

  saveLocal() async {
    if (isIoWorking) {
      final file = await getFile();
      await file.writeAsString(json.encode(asJson()));
    }
  }

  Future<File> getFile() async {
    final file = File('$path/$networkSubId');
    if (!await file.exists()) {
      await file.create(recursive: true);
    }
    return file;
  }

  setDefaults() {
    _localIsSilenced = false;
    syncEnabled = true;
  }
}
