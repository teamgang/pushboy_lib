// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'network_sub.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$NetworkSub on NetworkSubBase, Store {
  Computed<String>? _$pathComputed;

  @override
  String get path => (_$pathComputed ??=
          Computed<String>(() => super.path, name: 'NetworkSubBase.path'))
      .value;
  Computed<bool>? _$useRemoteComputed;

  @override
  bool get useRemote =>
      (_$useRemoteComputed ??= Computed<bool>(() => super.useRemote,
              name: 'NetworkSubBase.useRemote'))
          .value;
  Computed<bool>? _$isLinkedToSurferComputed;

  @override
  bool get isLinkedToSurfer => (_$isLinkedToSurferComputed ??= Computed<bool>(
          () => super.isLinkedToSurfer,
          name: 'NetworkSubBase.isLinkedToSurfer'))
      .value;
  Computed<bool>? _$isSilencedComputed;

  @override
  bool get isSilenced =>
      (_$isSilencedComputed ??= Computed<bool>(() => super.isSilenced,
              name: 'NetworkSubBase.isSilenced'))
          .value;

  late final _$networkSubIdAtom =
      Atom(name: 'NetworkSubBase.networkSubId', context: context);

  @override
  String get networkSubId {
    _$networkSubIdAtom.reportRead();
    return super.networkSubId;
  }

  @override
  set networkSubId(String value) {
    _$networkSubIdAtom.reportWrite(value, super.networkSubId, () {
      super.networkSubId = value;
    });
  }

  late final _$surferOrInstanceIdAtom =
      Atom(name: 'NetworkSubBase.surferOrInstanceId', context: context);

  @override
  String get surferOrInstanceId {
    _$surferOrInstanceIdAtom.reportRead();
    return super.surferOrInstanceId;
  }

  @override
  set surferOrInstanceId(String value) {
    _$surferOrInstanceIdAtom.reportWrite(value, super.surferOrInstanceId, () {
      super.surferOrInstanceId = value;
    });
  }

  late final _$syncEnabledAtom =
      Atom(name: 'NetworkSubBase.syncEnabled', context: context);

  @override
  bool get syncEnabled {
    _$syncEnabledAtom.reportRead();
    return super.syncEnabled;
  }

  @override
  set syncEnabled(bool value) {
    _$syncEnabledAtom.reportWrite(value, super.syncEnabled, () {
      super.syncEnabled = value;
    });
  }

  late final _$_localIsSilencedAtom =
      Atom(name: 'NetworkSubBase._localIsSilenced', context: context);

  @override
  bool get _localIsSilenced {
    _$_localIsSilencedAtom.reportRead();
    return super._localIsSilenced;
  }

  @override
  set _localIsSilenced(bool value) {
    _$_localIsSilencedAtom.reportWrite(value, super._localIsSilenced, () {
      super._localIsSilenced = value;
    });
  }

  late final _$_remoteIsSilencedAtom =
      Atom(name: 'NetworkSubBase._remoteIsSilenced', context: context);

  @override
  bool get _remoteIsSilenced {
    _$_remoteIsSilencedAtom.reportRead();
    return super._remoteIsSilenced;
  }

  @override
  set _remoteIsSilenced(bool value) {
    _$_remoteIsSilencedAtom.reportWrite(value, super._remoteIsSilenced, () {
      super._remoteIsSilenced = value;
    });
  }

  late final _$unpackFromApiAsyncAction =
      AsyncAction('NetworkSubBase.unpackFromApi', context: context);

  @override
  Future unpackFromApi(Map<String, dynamic> body) {
    return _$unpackFromApiAsyncAction.run(() => super.unpackFromApi(body));
  }

  late final _$NetworkSubBaseActionController =
      ActionController(name: 'NetworkSubBase', context: context);

  @override
  dynamic toggleIsLinkedToSurfer() {
    final _$actionInfo = _$NetworkSubBaseActionController.startAction(
        name: 'NetworkSubBase.toggleIsLinkedToSurfer');
    try {
      return super.toggleIsLinkedToSurfer();
    } finally {
      _$NetworkSubBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  dynamic loadFromAlt(NetworkSub alt) {
    final _$actionInfo = _$NetworkSubBaseActionController.startAction(
        name: 'NetworkSubBase.loadFromAlt');
    try {
      return super.loadFromAlt(alt);
    } finally {
      _$NetworkSubBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
networkSubId: ${networkSubId},
surferOrInstanceId: ${surferOrInstanceId},
syncEnabled: ${syncEnabled},
path: ${path},
useRemote: ${useRemote},
isLinkedToSurfer: ${isLinkedToSurfer},
isSilenced: ${isSilenced}
    ''';
  }
}
