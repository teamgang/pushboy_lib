import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

part 'payment.g.dart';

enum PaymentSilo {
  none,
  addCredit,
  basket,
  billPay,
  plan,
}

enum PaymentStatus {
  none,
  created,
  failed,
  waitingOnStripe,
  waitingToProcess,
  processing,
  processed,
}

enum PaymentAttemptType {
  ccAttempt,
  cryptoAttempt,
  successful,
}

extension PaymentSiloExtensions on PaymentSilo {
  String get human {
    switch (this) {
      case PaymentSilo.none:
        return 'none';
      case PaymentSilo.addCredit:
        return 'credit';
      case PaymentSilo.basket:
        return 'basket';
      case PaymentSilo.billPay:
        return 'monthly bill';
      case PaymentSilo.plan:
        return 'plan';
    }
  }

  String get papi {
    switch (this) {
      case PaymentSilo.none:
        return 'none';
      case PaymentSilo.addCredit:
        return 'add_credit';
      case PaymentSilo.basket:
        return 'basket';
      case PaymentSilo.billPay:
        return 'bill_pay';
      case PaymentSilo.plan:
        return 'plan';
    }
  }
}

extension PaymentStatusExtensions on PaymentStatus {
  String get human {
    switch (this) {
      case PaymentStatus.none:
        return 'none';
      case PaymentStatus.created:
        return 'created';
      case PaymentStatus.failed:
        return 'failed';
      case PaymentStatus.waitingOnStripe:
        return 'waiting on Stripe';
      case PaymentStatus.waitingToProcess:
        return 'waiting to process (should be fast)';
      case PaymentStatus.processing:
        return 'processing (should be fast)';
      case PaymentStatus.processed:
        return 'success';
    }
  }
}

class Payment = PaymentBase with _$Payment;

abstract class PaymentBase with Store {
  @observable
  String attemptId = '';
  @observable
  String paymentId = '';
  @observable
  String adminNotes = '';
  @observable
  String basket = '';
  @observable
  String billId = '';
  @observable
  int creditApplied = 0;
  @observable
  String currency = '';
  @observable
  bool isAutopaySelected = false;
  @observable
  String plasticId = '';
  @observable
  String projectId = '';
  @observable
  PaymentSilo silo = PaymentSilo.none;
  @observable
  PaymentStatus status = PaymentStatus.none;
  @observable
  int totalPrice = 0;
  @observable
  int totalPriceAfterCredit = 0;
  @observable
  int timeCreated = 0;

  bool isLoaded = false;

  void unpackFromApi(Map<String, dynamic> body) {
    attemptId = readString(body, 'attempt_id');
    paymentId = readString(body, 'payment_id');
    adminNotes = readString(body, 'admin_notes');
    basket = readString(body, 'basket');
    billId = readString(body, 'bill_id');
    creditApplied = readInt(body, 'credit_applied');
    currency = readString(body, 'currency');
    isAutopaySelected = readBool(body, 'is_autopay_selected');
    plasticId = readString(body, 'plastic_id');
    projectId = readString(body, 'project_id');
    totalPrice = readInt(body, 'total_price');
    totalPriceAfterCredit = readInt(body, 'total_price_after_credit');
    timeCreated = readInt(body, 'time_created');

    silo = stringToPaymentSilo(readString(body, 'silo'));
    status = stringToPaymentStatus(readString(body, 'status'));

    isLoaded = true;
  }

  Future<void> loadFromApi(
    BuildContext context,
    String loadPaymentId,
  ) async {
    if (loadPaymentId == '') {
      return;
    }

    var king = King.of(context);
    print(king.todd.developerId);
    ApiResponse ares = await king.lip.api(
      EndpointsV1.paymentGetById,
      payload: {'payment_id': loadPaymentId},
    );
    var body = ares.body;
    unpackFromApi(body);
  }

  PaymentSilo stringToPaymentSilo(String jin) {
    switch (jin) {
      case 'add_credit':
        return PaymentSilo.addCredit;
      case 'bill_pay':
        return PaymentSilo.billPay;
      case 'plan':
        return PaymentSilo.plan;
      default:
        return PaymentSilo.none;
    }
  }

  PaymentStatus stringToPaymentStatus(String jin) {
    switch (jin) {
      case '5':
        return PaymentStatus.created;
      case '10':
        return PaymentStatus.failed;
      case '30':
        return PaymentStatus.waitingOnStripe;
      case '50':
        return PaymentStatus.waitingToProcess;
      case '60':
        return PaymentStatus.processing;
      case '70':
        return PaymentStatus.processed;
      default:
        return PaymentStatus.none;
    }
  }
}
