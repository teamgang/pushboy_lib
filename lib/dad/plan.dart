import 'package:mobx/mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

part 'plan.g.dart';

class Plan = PlanBase with _$Plan;

abstract class PlanBase with Store {
  @observable
  int planId = 0;
  @observable
  int addCustomPrice = 0;
  @observable
  int addCustomUnits = 1;
  @observable
  int addLimitlessPrice = 0;
  @observable
  int addLimitlessUnits = 1;
  @observable
  int addStaticPrice = 0;
  @observable
  int addStaticUnits = 1;
  @observable
  int bundledCustom = 0;
  @observable
  int bundledLimitless = 0;
  @observable
  int bundledStatic = 0;

  @observable
  int monthlyCredit = 0;
  @observable
  int monthlyFreeCustom = 1;
  @observable
  int monthlyFreeLimitless = 1;
  @observable
  int monthlyFreeStatic = 1;
  @observable
  int monthlyFreeStaticEditTokens = 1;
  @observable
  int months = 12;
  @observable
  int price = 0;
  @observable
  String priceCurrency = 'USD';
  @observable
  int staticEditTokenPrice = 0;

  @observable
  String adminNotes = '';
  @observable
  int channelLimit = 0;
  @observable
  String description = '';
  @observable
  bool isAvailable = false;
  @observable
  bool isTailored = false;
  @observable
  String rules = '';
  @observable
  int subscriptionLimit = 0;
  @observable
  String title = '';

  bool isLoaded = false;

  Future<void> loadFromApi(King king, int planId) async {
    if (planId == 0) {
      return;
    }
    ApiResponse ares = await king.lip.api(
      EndpointsV1.planGetById,
      payload: {'plan_id': planId},
    );
    var body = ares.body;
    unpackFromApi(body);
  }

  void unpackFromApi(Map<String, dynamic> body) {
    planId = readInt(body, 'plan_id');
    addCustomPrice = readInt(body, 'add_custom_price');
    addCustomUnits = readInt(body, 'add_custom_units');
    addLimitlessPrice = readInt(body, 'add_limitless_price');
    addLimitlessUnits = readInt(body, 'add_limitless_units');
    addStaticPrice = readInt(body, 'add_static_price');
    addStaticUnits = readInt(body, 'add_static_units');
    monthlyCredit = readInt(body, 'monthly_credit');
    monthlyFreeCustom = readInt(body, 'monthly_free_custom');
    monthlyFreeLimitless = readInt(body, 'monthly_free_limitless');
    monthlyFreeStatic = readInt(body, 'monthly_free_static');
    monthlyFreeStaticEditTokens =
        readInt(body, 'monthly_free_static_edit_tokens');
    months = readInt(body, 'months');
    price = readInt(body, 'price');
    priceCurrency = readString(body, 'price_currency');
    staticEditTokenPrice = readInt(body, 'static_edit_token_price');

    adminNotes = readString(body, 'admin_notes');
    channelLimit = readInt(body, 'channel_limit');
    description = readString(body, 'description');
    isAvailable = readBool(body, 'is_available');
    isTailored = readBool(body, 'is_tailored');
    rules = readString(body, 'rules');
    subscriptionLimit = readInt(body, 'subscription_limit');
    title = readString(body, 'title');

    isLoaded = true;
  }

  void loadFromAltPlan(Plan alt) {
    planId = alt.planId;
    addCustomPrice = alt.addCustomPrice;
    addCustomUnits = alt.addCustomUnits;
    addLimitlessPrice = alt.addLimitlessPrice;
    addLimitlessUnits = alt.addLimitlessUnits;
    addStaticPrice = alt.addStaticPrice;
    addStaticUnits = alt.addStaticUnits;
    monthlyCredit = alt.monthlyCredit;
    monthlyFreeCustom = alt.monthlyFreeCustom;
    monthlyFreeLimitless = alt.monthlyFreeLimitless;
    monthlyFreeStatic = alt.monthlyFreeStatic;
    monthlyFreeStaticEditTokens = alt.monthlyFreeStaticEditTokens;
    months = alt.months;
    price = alt.price;
    priceCurrency = alt.priceCurrency;
    staticEditTokenPrice = alt.staticEditTokenPrice;

    adminNotes = alt.adminNotes;
    channelLimit = alt.channelLimit;
    description = alt.description;
    isAvailable = alt.isAvailable;
    isTailored = alt.isTailored;
    rules = alt.rules;
    subscriptionLimit = alt.subscriptionLimit;
    title = alt.title;

    isLoaded = true;
  }

  @computed
  bool get isFreemium {
    return planId == 1;
  }

  @computed
  String get subscriptionLimitForDisplay {
    if (price == 0) {
      return 'unlimited';
    }
    return subscriptionLimit.display;
  }

  @computed
  String get priceForDisplaySentence {
    if (price == 0) {
      return 'The plan you have selected is free.';
    }
    return 'The plan you have selected will cost \$$priceAsUsdString every $months.';
  }

  @computed
  String get priceAsUsdString {
    return (price / 100).toStringAsFixed(2);
  }

  @computed
  String get priceForDisplayTable {
    if (price == 0) {
      return 'Free';
    }
    String per = '';
    if (months == 12) {
      per = 'year';
    } else {
      throw Exception('Unknown period of months');
    }
    return '\$$priceAsUsdString / $per';
  }

  @computed
  String get displayPriceCustom {
    return '\$$addCustomPrice / ${10000.display}';
  }

  @computed
  String get displayPriceLimitless {
    return '\$$addLimitlessPrice / ${10000.display}';
  }

  @computed
  String get displayPriceStatic {
    return '\$$addStaticPrice / ${100000.display}';
  }
}
