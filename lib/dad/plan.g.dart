// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'plan.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$Plan on PlanBase, Store {
  Computed<bool>? _$isFreemiumComputed;

  @override
  bool get isFreemium => (_$isFreemiumComputed ??=
          Computed<bool>(() => super.isFreemium, name: 'PlanBase.isFreemium'))
      .value;
  Computed<String>? _$subscriptionLimitForDisplayComputed;

  @override
  String get subscriptionLimitForDisplay =>
      (_$subscriptionLimitForDisplayComputed ??= Computed<String>(
              () => super.subscriptionLimitForDisplay,
              name: 'PlanBase.subscriptionLimitForDisplay'))
          .value;
  Computed<String>? _$priceForDisplaySentenceComputed;

  @override
  String get priceForDisplaySentence => (_$priceForDisplaySentenceComputed ??=
          Computed<String>(() => super.priceForDisplaySentence,
              name: 'PlanBase.priceForDisplaySentence'))
      .value;
  Computed<String>? _$priceAsUsdStringComputed;

  @override
  String get priceAsUsdString => (_$priceAsUsdStringComputed ??=
          Computed<String>(() => super.priceAsUsdString,
              name: 'PlanBase.priceAsUsdString'))
      .value;
  Computed<String>? _$priceForDisplayTableComputed;

  @override
  String get priceForDisplayTable => (_$priceForDisplayTableComputed ??=
          Computed<String>(() => super.priceForDisplayTable,
              name: 'PlanBase.priceForDisplayTable'))
      .value;
  Computed<String>? _$displayPriceCustomComputed;

  @override
  String get displayPriceCustom => (_$displayPriceCustomComputed ??=
          Computed<String>(() => super.displayPriceCustom,
              name: 'PlanBase.displayPriceCustom'))
      .value;
  Computed<String>? _$displayPriceLimitlessComputed;

  @override
  String get displayPriceLimitless => (_$displayPriceLimitlessComputed ??=
          Computed<String>(() => super.displayPriceLimitless,
              name: 'PlanBase.displayPriceLimitless'))
      .value;
  Computed<String>? _$displayPriceStaticComputed;

  @override
  String get displayPriceStatic => (_$displayPriceStaticComputed ??=
          Computed<String>(() => super.displayPriceStatic,
              name: 'PlanBase.displayPriceStatic'))
      .value;

  late final _$planIdAtom = Atom(name: 'PlanBase.planId', context: context);

  @override
  int get planId {
    _$planIdAtom.reportRead();
    return super.planId;
  }

  @override
  set planId(int value) {
    _$planIdAtom.reportWrite(value, super.planId, () {
      super.planId = value;
    });
  }

  late final _$addCustomPriceAtom =
      Atom(name: 'PlanBase.addCustomPrice', context: context);

  @override
  int get addCustomPrice {
    _$addCustomPriceAtom.reportRead();
    return super.addCustomPrice;
  }

  @override
  set addCustomPrice(int value) {
    _$addCustomPriceAtom.reportWrite(value, super.addCustomPrice, () {
      super.addCustomPrice = value;
    });
  }

  late final _$addCustomUnitsAtom =
      Atom(name: 'PlanBase.addCustomUnits', context: context);

  @override
  int get addCustomUnits {
    _$addCustomUnitsAtom.reportRead();
    return super.addCustomUnits;
  }

  @override
  set addCustomUnits(int value) {
    _$addCustomUnitsAtom.reportWrite(value, super.addCustomUnits, () {
      super.addCustomUnits = value;
    });
  }

  late final _$addLimitlessPriceAtom =
      Atom(name: 'PlanBase.addLimitlessPrice', context: context);

  @override
  int get addLimitlessPrice {
    _$addLimitlessPriceAtom.reportRead();
    return super.addLimitlessPrice;
  }

  @override
  set addLimitlessPrice(int value) {
    _$addLimitlessPriceAtom.reportWrite(value, super.addLimitlessPrice, () {
      super.addLimitlessPrice = value;
    });
  }

  late final _$addLimitlessUnitsAtom =
      Atom(name: 'PlanBase.addLimitlessUnits', context: context);

  @override
  int get addLimitlessUnits {
    _$addLimitlessUnitsAtom.reportRead();
    return super.addLimitlessUnits;
  }

  @override
  set addLimitlessUnits(int value) {
    _$addLimitlessUnitsAtom.reportWrite(value, super.addLimitlessUnits, () {
      super.addLimitlessUnits = value;
    });
  }

  late final _$addStaticPriceAtom =
      Atom(name: 'PlanBase.addStaticPrice', context: context);

  @override
  int get addStaticPrice {
    _$addStaticPriceAtom.reportRead();
    return super.addStaticPrice;
  }

  @override
  set addStaticPrice(int value) {
    _$addStaticPriceAtom.reportWrite(value, super.addStaticPrice, () {
      super.addStaticPrice = value;
    });
  }

  late final _$addStaticUnitsAtom =
      Atom(name: 'PlanBase.addStaticUnits', context: context);

  @override
  int get addStaticUnits {
    _$addStaticUnitsAtom.reportRead();
    return super.addStaticUnits;
  }

  @override
  set addStaticUnits(int value) {
    _$addStaticUnitsAtom.reportWrite(value, super.addStaticUnits, () {
      super.addStaticUnits = value;
    });
  }

  late final _$bundledCustomAtom =
      Atom(name: 'PlanBase.bundledCustom', context: context);

  @override
  int get bundledCustom {
    _$bundledCustomAtom.reportRead();
    return super.bundledCustom;
  }

  @override
  set bundledCustom(int value) {
    _$bundledCustomAtom.reportWrite(value, super.bundledCustom, () {
      super.bundledCustom = value;
    });
  }

  late final _$bundledLimitlessAtom =
      Atom(name: 'PlanBase.bundledLimitless', context: context);

  @override
  int get bundledLimitless {
    _$bundledLimitlessAtom.reportRead();
    return super.bundledLimitless;
  }

  @override
  set bundledLimitless(int value) {
    _$bundledLimitlessAtom.reportWrite(value, super.bundledLimitless, () {
      super.bundledLimitless = value;
    });
  }

  late final _$bundledStaticAtom =
      Atom(name: 'PlanBase.bundledStatic', context: context);

  @override
  int get bundledStatic {
    _$bundledStaticAtom.reportRead();
    return super.bundledStatic;
  }

  @override
  set bundledStatic(int value) {
    _$bundledStaticAtom.reportWrite(value, super.bundledStatic, () {
      super.bundledStatic = value;
    });
  }

  late final _$monthlyCreditAtom =
      Atom(name: 'PlanBase.monthlyCredit', context: context);

  @override
  int get monthlyCredit {
    _$monthlyCreditAtom.reportRead();
    return super.monthlyCredit;
  }

  @override
  set monthlyCredit(int value) {
    _$monthlyCreditAtom.reportWrite(value, super.monthlyCredit, () {
      super.monthlyCredit = value;
    });
  }

  late final _$monthlyFreeCustomAtom =
      Atom(name: 'PlanBase.monthlyFreeCustom', context: context);

  @override
  int get monthlyFreeCustom {
    _$monthlyFreeCustomAtom.reportRead();
    return super.monthlyFreeCustom;
  }

  @override
  set monthlyFreeCustom(int value) {
    _$monthlyFreeCustomAtom.reportWrite(value, super.monthlyFreeCustom, () {
      super.monthlyFreeCustom = value;
    });
  }

  late final _$monthlyFreeLimitlessAtom =
      Atom(name: 'PlanBase.monthlyFreeLimitless', context: context);

  @override
  int get monthlyFreeLimitless {
    _$monthlyFreeLimitlessAtom.reportRead();
    return super.monthlyFreeLimitless;
  }

  @override
  set monthlyFreeLimitless(int value) {
    _$monthlyFreeLimitlessAtom.reportWrite(value, super.monthlyFreeLimitless,
        () {
      super.monthlyFreeLimitless = value;
    });
  }

  late final _$monthlyFreeStaticAtom =
      Atom(name: 'PlanBase.monthlyFreeStatic', context: context);

  @override
  int get monthlyFreeStatic {
    _$monthlyFreeStaticAtom.reportRead();
    return super.monthlyFreeStatic;
  }

  @override
  set monthlyFreeStatic(int value) {
    _$monthlyFreeStaticAtom.reportWrite(value, super.monthlyFreeStatic, () {
      super.monthlyFreeStatic = value;
    });
  }

  late final _$monthlyFreeStaticEditTokensAtom =
      Atom(name: 'PlanBase.monthlyFreeStaticEditTokens', context: context);

  @override
  int get monthlyFreeStaticEditTokens {
    _$monthlyFreeStaticEditTokensAtom.reportRead();
    return super.monthlyFreeStaticEditTokens;
  }

  @override
  set monthlyFreeStaticEditTokens(int value) {
    _$monthlyFreeStaticEditTokensAtom
        .reportWrite(value, super.monthlyFreeStaticEditTokens, () {
      super.monthlyFreeStaticEditTokens = value;
    });
  }

  late final _$monthsAtom = Atom(name: 'PlanBase.months', context: context);

  @override
  int get months {
    _$monthsAtom.reportRead();
    return super.months;
  }

  @override
  set months(int value) {
    _$monthsAtom.reportWrite(value, super.months, () {
      super.months = value;
    });
  }

  late final _$priceAtom = Atom(name: 'PlanBase.price', context: context);

  @override
  int get price {
    _$priceAtom.reportRead();
    return super.price;
  }

  @override
  set price(int value) {
    _$priceAtom.reportWrite(value, super.price, () {
      super.price = value;
    });
  }

  late final _$priceCurrencyAtom =
      Atom(name: 'PlanBase.priceCurrency', context: context);

  @override
  String get priceCurrency {
    _$priceCurrencyAtom.reportRead();
    return super.priceCurrency;
  }

  @override
  set priceCurrency(String value) {
    _$priceCurrencyAtom.reportWrite(value, super.priceCurrency, () {
      super.priceCurrency = value;
    });
  }

  late final _$staticEditTokenPriceAtom =
      Atom(name: 'PlanBase.staticEditTokenPrice', context: context);

  @override
  int get staticEditTokenPrice {
    _$staticEditTokenPriceAtom.reportRead();
    return super.staticEditTokenPrice;
  }

  @override
  set staticEditTokenPrice(int value) {
    _$staticEditTokenPriceAtom.reportWrite(value, super.staticEditTokenPrice,
        () {
      super.staticEditTokenPrice = value;
    });
  }

  late final _$adminNotesAtom =
      Atom(name: 'PlanBase.adminNotes', context: context);

  @override
  String get adminNotes {
    _$adminNotesAtom.reportRead();
    return super.adminNotes;
  }

  @override
  set adminNotes(String value) {
    _$adminNotesAtom.reportWrite(value, super.adminNotes, () {
      super.adminNotes = value;
    });
  }

  late final _$channelLimitAtom =
      Atom(name: 'PlanBase.channelLimit', context: context);

  @override
  int get channelLimit {
    _$channelLimitAtom.reportRead();
    return super.channelLimit;
  }

  @override
  set channelLimit(int value) {
    _$channelLimitAtom.reportWrite(value, super.channelLimit, () {
      super.channelLimit = value;
    });
  }

  late final _$descriptionAtom =
      Atom(name: 'PlanBase.description', context: context);

  @override
  String get description {
    _$descriptionAtom.reportRead();
    return super.description;
  }

  @override
  set description(String value) {
    _$descriptionAtom.reportWrite(value, super.description, () {
      super.description = value;
    });
  }

  late final _$isAvailableAtom =
      Atom(name: 'PlanBase.isAvailable', context: context);

  @override
  bool get isAvailable {
    _$isAvailableAtom.reportRead();
    return super.isAvailable;
  }

  @override
  set isAvailable(bool value) {
    _$isAvailableAtom.reportWrite(value, super.isAvailable, () {
      super.isAvailable = value;
    });
  }

  late final _$isTailoredAtom =
      Atom(name: 'PlanBase.isTailored', context: context);

  @override
  bool get isTailored {
    _$isTailoredAtom.reportRead();
    return super.isTailored;
  }

  @override
  set isTailored(bool value) {
    _$isTailoredAtom.reportWrite(value, super.isTailored, () {
      super.isTailored = value;
    });
  }

  late final _$rulesAtom = Atom(name: 'PlanBase.rules', context: context);

  @override
  String get rules {
    _$rulesAtom.reportRead();
    return super.rules;
  }

  @override
  set rules(String value) {
    _$rulesAtom.reportWrite(value, super.rules, () {
      super.rules = value;
    });
  }

  late final _$subscriptionLimitAtom =
      Atom(name: 'PlanBase.subscriptionLimit', context: context);

  @override
  int get subscriptionLimit {
    _$subscriptionLimitAtom.reportRead();
    return super.subscriptionLimit;
  }

  @override
  set subscriptionLimit(int value) {
    _$subscriptionLimitAtom.reportWrite(value, super.subscriptionLimit, () {
      super.subscriptionLimit = value;
    });
  }

  late final _$titleAtom = Atom(name: 'PlanBase.title', context: context);

  @override
  String get title {
    _$titleAtom.reportRead();
    return super.title;
  }

  @override
  set title(String value) {
    _$titleAtom.reportWrite(value, super.title, () {
      super.title = value;
    });
  }

  @override
  String toString() {
    return '''
planId: ${planId},
addCustomPrice: ${addCustomPrice},
addCustomUnits: ${addCustomUnits},
addLimitlessPrice: ${addLimitlessPrice},
addLimitlessUnits: ${addLimitlessUnits},
addStaticPrice: ${addStaticPrice},
addStaticUnits: ${addStaticUnits},
bundledCustom: ${bundledCustom},
bundledLimitless: ${bundledLimitless},
bundledStatic: ${bundledStatic},
monthlyCredit: ${monthlyCredit},
monthlyFreeCustom: ${monthlyFreeCustom},
monthlyFreeLimitless: ${monthlyFreeLimitless},
monthlyFreeStatic: ${monthlyFreeStatic},
monthlyFreeStaticEditTokens: ${monthlyFreeStaticEditTokens},
months: ${months},
price: ${price},
priceCurrency: ${priceCurrency},
staticEditTokenPrice: ${staticEditTokenPrice},
adminNotes: ${adminNotes},
channelLimit: ${channelLimit},
description: ${description},
isAvailable: ${isAvailable},
isTailored: ${isTailored},
rules: ${rules},
subscriptionLimit: ${subscriptionLimit},
title: ${title},
isFreemium: ${isFreemium},
subscriptionLimitForDisplay: ${subscriptionLimitForDisplay},
priceForDisplaySentence: ${priceForDisplaySentence},
priceAsUsdString: ${priceAsUsdString},
priceForDisplayTable: ${priceForDisplayTable},
displayPriceCustom: ${displayPriceCustom},
displayPriceLimitless: ${displayPriceLimitless},
displayPriceStatic: ${displayPriceStatic}
    ''';
  }
}
