import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:mobx/mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

part 'project.g.dart';

class Project = ProjectBase with _$Project;

abstract class ProjectBase with Store {
  @observable
  String creatorId = '';
  @observable
  int credit = 0;
  @observable
  String defaultPlasticId = '';
  @observable
  bool isAutopayEnabled = false;
  @observable
  bool isClosed = false;
  @observable
  String name = '';
  @observable
  String notes = '';
  @observable
  int planId = 0;
  @observable
  int planIdNext = 0;
  @observable
  int timePlanExpires = 0;
  @observable
  String projectId = '';
  @observable
  int timeCreated = 0;

  @observable
  int customSent = 0;
  @observable
  int customBought = 0;
  @observable
  int limitlessSent = 0;
  @observable
  int limitlessBought = 0;
  @observable
  int staticSent = 0;
  @observable
  int staticBought = 0;
  @observable
  int subscribers = 0;

  @observable
  int statTokensConsumed = 0;
  @observable
  int statTokensPurchased = 0;

  bool isLoaded = false;

  int getMaxCredit(int price) {
    if (price > credit) {
      return credit;
    } else {
      return price;
    }
  }

  @computed
  int get statEditTokensBoughtTotal {
    return statTokensPurchased + statTokensConsumed;
  }

  int statEditTokensBoughtPrice(Plan plan) {
    return statEditTokensBoughtTotal * plan.staticEditTokenPrice;
  }

  @computed
  int get planExpiresYear {
    return DateTime.fromMillisecondsSinceEpoch(timePlanExpires).year;
  }

  @computed
  int get planExpiresMonth {
    return DateTime.fromMillisecondsSinceEpoch(timePlanExpires).month;
  }

  @computed
  String get timePlanExpiresForDisplay {
    if (timePlanExpires == 0) {
      return 'N/A';
    }
    return DateFormat.yMMMMd('en_US')
        .format(DateTime.fromMillisecondsSinceEpoch(timePlanExpires * 1000));
  }

  Future<void> loadFromApi(BuildContext context, String projectId) async {
    if (projectId == '') {
      return;
    }
    var king = King.of(context);
    print(king.todd.developerId);
    ApiResponse ares = await king.lip.api(
      EndpointsV1.projectGetById,
      payload: {'project_id': projectId},
    );
    var body = ares.body;
    unpackFromApi(body);
  }

  void unpackFromApi(Map<String, dynamic> body) {
    creatorId = readString(body, 'creator_id');
    credit = readInt(body, 'credit');
    defaultPlasticId = readString(body, 'default_plastic_id');
    isAutopayEnabled = readBool(body, 'is_autopay_enabled');
    isClosed = readBool(body, 'is_closed');
    name = readString(body, 'name');
    notes = readString(body, 'notes');
    planId = readInt(body, 'plan_id');
    planIdNext = readInt(body, 'plan_id_next');
    projectId = readString(body, 'project_id');
    timePlanExpires = readInt(body, 'time_plan_expires');
    timeCreated = readInt(body, 'time_created');

    customBought = readInt(body, 'custom_bought');
    customSent = readInt(body, 'custom_sent');
    limitlessBought = readInt(body, 'limitless_bought');
    limitlessSent = readInt(body, 'limitless_sent');
    staticBought = readInt(body, 'static_bought');
    staticSent = readInt(body, 'static_sent');
    subscribers = readInt(body, 'subscribers');
    isLoaded = true;
  }

  @action
  void loadFromAltProject(Project alt) {
    creatorId = alt.creatorId;
    credit = alt.credit;
    isAutopayEnabled = alt.isAutopayEnabled;
    isClosed = alt.isClosed;
    name = alt.name;
    notes = alt.notes;
    timePlanExpires = alt.timePlanExpires;
    planId = alt.planId;
    planIdNext = alt.planIdNext;
    projectId = alt.projectId;
    timeCreated = alt.timeCreated;

    customBought = alt.customBought;
    customSent = alt.customSent;
    limitlessBought = alt.limitlessBought;
    limitlessSent = alt.limitlessSent;
    staticBought = alt.staticBought;
    staticSent = alt.staticSent;
    subscribers = alt.subscribers;
    isLoaded = true;
  }

  @action
  Future<void> loadStaticEditTokenInfo(BuildContext context) async {
    if (projectId == '') {
      return;
    }
    final now = DateTime.now();

    var king = King.of(context);
    ApiResponse ares = await king.lip.api(
      EndpointsV1.statTokenStatusByProjectAndPeriod,
      payload: {
        'month': now.month,
        'year': now.year,
        'project_id': projectId,
      },
    );

    if (ares.isOk) {
      var temp = readInt(ares.body, 'tokens_consumed');
      if (statTokensConsumed != temp) {
        statTokensConsumed = temp;
      }

      temp = readInt(ares.body, 'tokens_purchased');
      if (statTokensPurchased != temp) {
        statTokensPurchased = temp;
      }
    }
  }

  @computed
  String get firstBillingPeriodAsString {
    return DateFormat('y-m')
        .format(DateTime.fromMillisecondsSinceEpoch(timeCreated * 1000));
  }

  @computed
  int get firstBillingMonth {
    return DateTime.fromMillisecondsSinceEpoch(timeCreated * 1000).month;
  }

  @computed
  int get firstBillingYear {
    final year = DateTime.fromMillisecondsSinceEpoch(timeCreated * 1000).year;
    return year >= 2020 ? year : 2020;
    //return DateTime.fromMillisecondsSinceEpoch(timeCreated * 1000).year;
  }

  @computed
  String get isAutopayEnabledString {
    return isAutopayEnabled ? 'Enabled' : 'Disabled';
  }

  @computed
  bool get mustChangePlanNow {
    return (planId == 0) || (planId == 1);
  }

  @computed
  bool get isPlanChanging {
    return (planId != planIdNext);
  }

  @computed
  int get customRemaining {
    return customBought - customSent;
  }

  @computed
  int get limitlessRemaining {
    return limitlessBought - limitlessSent;
  }

  @computed
  int get staticRemaining {
    return staticBought - staticSent;
  }

  int customRemainingWithFree(Plan plan) {
    return customRemaining + plan.monthlyFreeCustom;
  }

  int limitlessRemainingWithFree(Plan plan) {
    return limitlessRemaining + plan.monthlyFreeLimitless;
  }

  int staticRemainingWithFree(Plan plan) {
    return staticRemaining + plan.monthlyFreeStatic;
  }
}
