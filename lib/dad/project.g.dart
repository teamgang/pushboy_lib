// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'project.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$Project on ProjectBase, Store {
  Computed<int>? _$statEditTokensBoughtTotalComputed;

  @override
  int get statEditTokensBoughtTotal => (_$statEditTokensBoughtTotalComputed ??=
          Computed<int>(() => super.statEditTokensBoughtTotal,
              name: 'ProjectBase.statEditTokensBoughtTotal'))
      .value;
  Computed<int>? _$planExpiresYearComputed;

  @override
  int get planExpiresYear =>
      (_$planExpiresYearComputed ??= Computed<int>(() => super.planExpiresYear,
              name: 'ProjectBase.planExpiresYear'))
          .value;
  Computed<int>? _$planExpiresMonthComputed;

  @override
  int get planExpiresMonth => (_$planExpiresMonthComputed ??= Computed<int>(
          () => super.planExpiresMonth,
          name: 'ProjectBase.planExpiresMonth'))
      .value;
  Computed<String>? _$timePlanExpiresForDisplayComputed;

  @override
  String get timePlanExpiresForDisplay =>
      (_$timePlanExpiresForDisplayComputed ??= Computed<String>(
              () => super.timePlanExpiresForDisplay,
              name: 'ProjectBase.timePlanExpiresForDisplay'))
          .value;
  Computed<String>? _$firstBillingPeriodAsStringComputed;

  @override
  String get firstBillingPeriodAsString =>
      (_$firstBillingPeriodAsStringComputed ??= Computed<String>(
              () => super.firstBillingPeriodAsString,
              name: 'ProjectBase.firstBillingPeriodAsString'))
          .value;
  Computed<int>? _$firstBillingMonthComputed;

  @override
  int get firstBillingMonth => (_$firstBillingMonthComputed ??= Computed<int>(
          () => super.firstBillingMonth,
          name: 'ProjectBase.firstBillingMonth'))
      .value;
  Computed<int>? _$firstBillingYearComputed;

  @override
  int get firstBillingYear => (_$firstBillingYearComputed ??= Computed<int>(
          () => super.firstBillingYear,
          name: 'ProjectBase.firstBillingYear'))
      .value;
  Computed<String>? _$isAutopayEnabledStringComputed;

  @override
  String get isAutopayEnabledString => (_$isAutopayEnabledStringComputed ??=
          Computed<String>(() => super.isAutopayEnabledString,
              name: 'ProjectBase.isAutopayEnabledString'))
      .value;
  Computed<bool>? _$mustChangePlanNowComputed;

  @override
  bool get mustChangePlanNow => (_$mustChangePlanNowComputed ??= Computed<bool>(
          () => super.mustChangePlanNow,
          name: 'ProjectBase.mustChangePlanNow'))
      .value;
  Computed<bool>? _$isPlanChangingComputed;

  @override
  bool get isPlanChanging =>
      (_$isPlanChangingComputed ??= Computed<bool>(() => super.isPlanChanging,
              name: 'ProjectBase.isPlanChanging'))
          .value;
  Computed<int>? _$customRemainingComputed;

  @override
  int get customRemaining =>
      (_$customRemainingComputed ??= Computed<int>(() => super.customRemaining,
              name: 'ProjectBase.customRemaining'))
          .value;
  Computed<int>? _$limitlessRemainingComputed;

  @override
  int get limitlessRemaining => (_$limitlessRemainingComputed ??= Computed<int>(
          () => super.limitlessRemaining,
          name: 'ProjectBase.limitlessRemaining'))
      .value;
  Computed<int>? _$staticRemainingComputed;

  @override
  int get staticRemaining =>
      (_$staticRemainingComputed ??= Computed<int>(() => super.staticRemaining,
              name: 'ProjectBase.staticRemaining'))
          .value;

  late final _$creatorIdAtom =
      Atom(name: 'ProjectBase.creatorId', context: context);

  @override
  String get creatorId {
    _$creatorIdAtom.reportRead();
    return super.creatorId;
  }

  @override
  set creatorId(String value) {
    _$creatorIdAtom.reportWrite(value, super.creatorId, () {
      super.creatorId = value;
    });
  }

  late final _$creditAtom = Atom(name: 'ProjectBase.credit', context: context);

  @override
  int get credit {
    _$creditAtom.reportRead();
    return super.credit;
  }

  @override
  set credit(int value) {
    _$creditAtom.reportWrite(value, super.credit, () {
      super.credit = value;
    });
  }

  late final _$defaultPlasticIdAtom =
      Atom(name: 'ProjectBase.defaultPlasticId', context: context);

  @override
  String get defaultPlasticId {
    _$defaultPlasticIdAtom.reportRead();
    return super.defaultPlasticId;
  }

  @override
  set defaultPlasticId(String value) {
    _$defaultPlasticIdAtom.reportWrite(value, super.defaultPlasticId, () {
      super.defaultPlasticId = value;
    });
  }

  late final _$isAutopayEnabledAtom =
      Atom(name: 'ProjectBase.isAutopayEnabled', context: context);

  @override
  bool get isAutopayEnabled {
    _$isAutopayEnabledAtom.reportRead();
    return super.isAutopayEnabled;
  }

  @override
  set isAutopayEnabled(bool value) {
    _$isAutopayEnabledAtom.reportWrite(value, super.isAutopayEnabled, () {
      super.isAutopayEnabled = value;
    });
  }

  late final _$isClosedAtom =
      Atom(name: 'ProjectBase.isClosed', context: context);

  @override
  bool get isClosed {
    _$isClosedAtom.reportRead();
    return super.isClosed;
  }

  @override
  set isClosed(bool value) {
    _$isClosedAtom.reportWrite(value, super.isClosed, () {
      super.isClosed = value;
    });
  }

  late final _$nameAtom = Atom(name: 'ProjectBase.name', context: context);

  @override
  String get name {
    _$nameAtom.reportRead();
    return super.name;
  }

  @override
  set name(String value) {
    _$nameAtom.reportWrite(value, super.name, () {
      super.name = value;
    });
  }

  late final _$notesAtom = Atom(name: 'ProjectBase.notes', context: context);

  @override
  String get notes {
    _$notesAtom.reportRead();
    return super.notes;
  }

  @override
  set notes(String value) {
    _$notesAtom.reportWrite(value, super.notes, () {
      super.notes = value;
    });
  }

  late final _$planIdAtom = Atom(name: 'ProjectBase.planId', context: context);

  @override
  int get planId {
    _$planIdAtom.reportRead();
    return super.planId;
  }

  @override
  set planId(int value) {
    _$planIdAtom.reportWrite(value, super.planId, () {
      super.planId = value;
    });
  }

  late final _$planIdNextAtom =
      Atom(name: 'ProjectBase.planIdNext', context: context);

  @override
  int get planIdNext {
    _$planIdNextAtom.reportRead();
    return super.planIdNext;
  }

  @override
  set planIdNext(int value) {
    _$planIdNextAtom.reportWrite(value, super.planIdNext, () {
      super.planIdNext = value;
    });
  }

  late final _$timePlanExpiresAtom =
      Atom(name: 'ProjectBase.timePlanExpires', context: context);

  @override
  int get timePlanExpires {
    _$timePlanExpiresAtom.reportRead();
    return super.timePlanExpires;
  }

  @override
  set timePlanExpires(int value) {
    _$timePlanExpiresAtom.reportWrite(value, super.timePlanExpires, () {
      super.timePlanExpires = value;
    });
  }

  late final _$projectIdAtom =
      Atom(name: 'ProjectBase.projectId', context: context);

  @override
  String get projectId {
    _$projectIdAtom.reportRead();
    return super.projectId;
  }

  @override
  set projectId(String value) {
    _$projectIdAtom.reportWrite(value, super.projectId, () {
      super.projectId = value;
    });
  }

  late final _$timeCreatedAtom =
      Atom(name: 'ProjectBase.timeCreated', context: context);

  @override
  int get timeCreated {
    _$timeCreatedAtom.reportRead();
    return super.timeCreated;
  }

  @override
  set timeCreated(int value) {
    _$timeCreatedAtom.reportWrite(value, super.timeCreated, () {
      super.timeCreated = value;
    });
  }

  late final _$customSentAtom =
      Atom(name: 'ProjectBase.customSent', context: context);

  @override
  int get customSent {
    _$customSentAtom.reportRead();
    return super.customSent;
  }

  @override
  set customSent(int value) {
    _$customSentAtom.reportWrite(value, super.customSent, () {
      super.customSent = value;
    });
  }

  late final _$customBoughtAtom =
      Atom(name: 'ProjectBase.customBought', context: context);

  @override
  int get customBought {
    _$customBoughtAtom.reportRead();
    return super.customBought;
  }

  @override
  set customBought(int value) {
    _$customBoughtAtom.reportWrite(value, super.customBought, () {
      super.customBought = value;
    });
  }

  late final _$limitlessSentAtom =
      Atom(name: 'ProjectBase.limitlessSent', context: context);

  @override
  int get limitlessSent {
    _$limitlessSentAtom.reportRead();
    return super.limitlessSent;
  }

  @override
  set limitlessSent(int value) {
    _$limitlessSentAtom.reportWrite(value, super.limitlessSent, () {
      super.limitlessSent = value;
    });
  }

  late final _$limitlessBoughtAtom =
      Atom(name: 'ProjectBase.limitlessBought', context: context);

  @override
  int get limitlessBought {
    _$limitlessBoughtAtom.reportRead();
    return super.limitlessBought;
  }

  @override
  set limitlessBought(int value) {
    _$limitlessBoughtAtom.reportWrite(value, super.limitlessBought, () {
      super.limitlessBought = value;
    });
  }

  late final _$staticSentAtom =
      Atom(name: 'ProjectBase.staticSent', context: context);

  @override
  int get staticSent {
    _$staticSentAtom.reportRead();
    return super.staticSent;
  }

  @override
  set staticSent(int value) {
    _$staticSentAtom.reportWrite(value, super.staticSent, () {
      super.staticSent = value;
    });
  }

  late final _$staticBoughtAtom =
      Atom(name: 'ProjectBase.staticBought', context: context);

  @override
  int get staticBought {
    _$staticBoughtAtom.reportRead();
    return super.staticBought;
  }

  @override
  set staticBought(int value) {
    _$staticBoughtAtom.reportWrite(value, super.staticBought, () {
      super.staticBought = value;
    });
  }

  late final _$subscribersAtom =
      Atom(name: 'ProjectBase.subscribers', context: context);

  @override
  int get subscribers {
    _$subscribersAtom.reportRead();
    return super.subscribers;
  }

  @override
  set subscribers(int value) {
    _$subscribersAtom.reportWrite(value, super.subscribers, () {
      super.subscribers = value;
    });
  }

  late final _$statTokensConsumedAtom =
      Atom(name: 'ProjectBase.statTokensConsumed', context: context);

  @override
  int get statTokensConsumed {
    _$statTokensConsumedAtom.reportRead();
    return super.statTokensConsumed;
  }

  @override
  set statTokensConsumed(int value) {
    _$statTokensConsumedAtom.reportWrite(value, super.statTokensConsumed, () {
      super.statTokensConsumed = value;
    });
  }

  late final _$statTokensPurchasedAtom =
      Atom(name: 'ProjectBase.statTokensPurchased', context: context);

  @override
  int get statTokensPurchased {
    _$statTokensPurchasedAtom.reportRead();
    return super.statTokensPurchased;
  }

  @override
  set statTokensPurchased(int value) {
    _$statTokensPurchasedAtom.reportWrite(value, super.statTokensPurchased, () {
      super.statTokensPurchased = value;
    });
  }

  late final _$loadStaticEditTokenInfoAsyncAction =
      AsyncAction('ProjectBase.loadStaticEditTokenInfo', context: context);

  @override
  Future<void> loadStaticEditTokenInfo(BuildContext context) {
    return _$loadStaticEditTokenInfoAsyncAction
        .run(() => super.loadStaticEditTokenInfo(context));
  }

  late final _$ProjectBaseActionController =
      ActionController(name: 'ProjectBase', context: context);

  @override
  void loadFromAltProject(Project alt) {
    final _$actionInfo = _$ProjectBaseActionController.startAction(
        name: 'ProjectBase.loadFromAltProject');
    try {
      return super.loadFromAltProject(alt);
    } finally {
      _$ProjectBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
creatorId: ${creatorId},
credit: ${credit},
defaultPlasticId: ${defaultPlasticId},
isAutopayEnabled: ${isAutopayEnabled},
isClosed: ${isClosed},
name: ${name},
notes: ${notes},
planId: ${planId},
planIdNext: ${planIdNext},
timePlanExpires: ${timePlanExpires},
projectId: ${projectId},
timeCreated: ${timeCreated},
customSent: ${customSent},
customBought: ${customBought},
limitlessSent: ${limitlessSent},
limitlessBought: ${limitlessBought},
staticSent: ${staticSent},
staticBought: ${staticBought},
subscribers: ${subscribers},
statTokensConsumed: ${statTokensConsumed},
statTokensPurchased: ${statTokensPurchased},
statEditTokensBoughtTotal: ${statEditTokensBoughtTotal},
planExpiresYear: ${planExpiresYear},
planExpiresMonth: ${planExpiresMonth},
timePlanExpiresForDisplay: ${timePlanExpiresForDisplay},
firstBillingPeriodAsString: ${firstBillingPeriodAsString},
firstBillingMonth: ${firstBillingMonth},
firstBillingYear: ${firstBillingYear},
isAutopayEnabledString: ${isAutopayEnabledString},
mustChangePlanNow: ${mustChangePlanNow},
isPlanChanging: ${isPlanChanging},
customRemaining: ${customRemaining},
limitlessRemaining: ${limitlessRemaining},
staticRemaining: ${staticRemaining}
    ''';
  }
}
