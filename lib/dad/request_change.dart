import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

part 'request_change.g.dart';

class RequestChange = RequestChangeBase with _$RequestChange;

abstract class RequestChangeBase with Store {
  @observable
  String developerId = '';
  @observable
  String surferId = '';
  @observable
  bool isConsumed = false;
  @observable
  int timeExpires = 0;

  @observable
  String token = '';

  loadFromApi({
    required BuildContext context,
    required String endpoint,
    required String token,
  }) async {
    if (token.isEmpty) {
      return;
    }
    ApiResponse ares = await King.of(context).lip.api(
      endpoint,
      payload: {'token': token},
    );

    if (ares.isOk) {
      unpackFromApi(ares.body);
    }
  }

  void unpackFromApi(Map<String, dynamic> body) {
    isConsumed = readBool(body, 'is_consumed');
    developerId = readString(body, 'developer_id');
    surferId = readString(body, 'surfer_id');
    timeExpires = readInt(body, 'time_expires');
  }

  @computed
  bool get isExpiredOrConsumed {
    return ((DateTime.now().millisecondsSinceEpoch / 1000) > timeExpires) ||
        isConsumed;
  }
}
