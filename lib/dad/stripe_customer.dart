import 'package:mobx/mobx.dart';

import 'package:pushboy_lib/helpers/parsers.dart';
import 'package:pushboy_lib/king.dart';
import 'package:pushboy_lib/services/lip.dart';

part 'stripe_customer.g.dart';

class Customer = CustomerBase with _$Customer;

abstract class CustomerBase with Store {
  @observable
  String id = '';
  @observable
  String currency = 'usd';
  @observable
  String defaultSourceId = '';
  @observable
  String email = '';

  ObservableMap<String, dynamic> metadata = ObservableMap();
  ObservableMap<String, Source> sources = ObservableMap();

  Future<void> loadFromStripe(King king) async {
    if (king.todd.stripeCustomerId == '') {
      return;
    }
    print(king.todd.stripeCustomerId);
    ApiResponse ares = await king.lip.stripe(
      '/customers/${king.todd.stripeCustomerId}',
      method: HttpMethod.get,
      params: {
        'expand[]': 'sources',
      },
    );
    var body = ares.body;
    unpackFromApi(body);
  }

  void unpackFromApi(Map<String, dynamic> body) {
    id = readString(body, 'id');
    //currency = readString(body, 'currency');
    defaultSourceId = readString(body, 'default_source');
    email = readString(body, 'email');

    metadata.clear();
    metadata.addAll(body['metadata']);

    sources.clear();
    for (final sourceJin in body['sources']['data']) {
      final source = Source.fromJson(sourceJin);
      sources[source.id] = source;
    }
  }

  @computed
  bool get isSourcesEmpty {
    if (sources.isEmpty) {
      return false;
    }
    return true;
  }

  @computed
  List<Source> get nonDefaultSources {
    List<Source> nonDef = [];
    for (final source in sources.values) {
      if (source.id != defaultSourceId) {
        nonDef.add(source);
      }
    }
    return nonDef;
  }

  @computed
  Source get defaultSource {
    return sources[defaultSourceId] ?? Source();
  }
}

class Source {
  Source();
  String id = '';
  String brand = '';
  String cvcCheck = ''; // pass or fail
  int expMonth = 0;
  int expYear = 0;
  String last4 = '';
  String object = '';

  factory Source.fromJson(Map<String, dynamic> jin) {
    final tt = Source();
    tt.id = readString(jin, 'id');
    tt.brand = readString(jin, 'brand');
    tt.cvcCheck = readString(jin, 'cvc_check');
    tt.expMonth = readInt(jin, 'exp_month');
    tt.expYear = readInt(jin, 'exp_year');
    tt.last4 = readString(jin, 'last4');
    tt.object = readString(jin, 'object');
    return tt;
  }

  String get expDate {
    return ('$expMonth/$expYear');
  }
}
