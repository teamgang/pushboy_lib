// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'stripe_customer.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$Customer on CustomerBase, Store {
  Computed<bool>? _$isSourcesEmptyComputed;

  @override
  bool get isSourcesEmpty =>
      (_$isSourcesEmptyComputed ??= Computed<bool>(() => super.isSourcesEmpty,
              name: 'CustomerBase.isSourcesEmpty'))
          .value;
  Computed<List<Source>>? _$nonDefaultSourcesComputed;

  @override
  List<Source> get nonDefaultSources => (_$nonDefaultSourcesComputed ??=
          Computed<List<Source>>(() => super.nonDefaultSources,
              name: 'CustomerBase.nonDefaultSources'))
      .value;
  Computed<Source>? _$defaultSourceComputed;

  @override
  Source get defaultSource =>
      (_$defaultSourceComputed ??= Computed<Source>(() => super.defaultSource,
              name: 'CustomerBase.defaultSource'))
          .value;

  late final _$idAtom = Atom(name: 'CustomerBase.id', context: context);

  @override
  String get id {
    _$idAtom.reportRead();
    return super.id;
  }

  @override
  set id(String value) {
    _$idAtom.reportWrite(value, super.id, () {
      super.id = value;
    });
  }

  late final _$currencyAtom =
      Atom(name: 'CustomerBase.currency', context: context);

  @override
  String get currency {
    _$currencyAtom.reportRead();
    return super.currency;
  }

  @override
  set currency(String value) {
    _$currencyAtom.reportWrite(value, super.currency, () {
      super.currency = value;
    });
  }

  late final _$defaultSourceIdAtom =
      Atom(name: 'CustomerBase.defaultSourceId', context: context);

  @override
  String get defaultSourceId {
    _$defaultSourceIdAtom.reportRead();
    return super.defaultSourceId;
  }

  @override
  set defaultSourceId(String value) {
    _$defaultSourceIdAtom.reportWrite(value, super.defaultSourceId, () {
      super.defaultSourceId = value;
    });
  }

  late final _$emailAtom = Atom(name: 'CustomerBase.email', context: context);

  @override
  String get email {
    _$emailAtom.reportRead();
    return super.email;
  }

  @override
  set email(String value) {
    _$emailAtom.reportWrite(value, super.email, () {
      super.email = value;
    });
  }

  @override
  String toString() {
    return '''
id: ${id},
currency: ${currency},
defaultSourceId: ${defaultSourceId},
email: ${email},
isSourcesEmpty: ${isSourcesEmpty},
nonDefaultSources: ${nonDefaultSources},
defaultSource: ${defaultSource}
    ''';
  }
}
