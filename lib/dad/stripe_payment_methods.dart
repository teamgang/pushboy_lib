import 'package:flutter/material.dart';
import 'package:intl/intl.dart' as intl;
import 'package:mobx/mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

part 'stripe_payment_methods.g.dart';

class PaymentMethods = PaymentMethodsBase with _$PaymentMethods;

abstract class PaymentMethodsBase with Store {
  ObservableMap<String, Plastic> plastics = ObservableMap();

  Future<void> loadFromStripe(King king) async {
    if (king.todd.stripeCustomerId == '') {
      return;
    }
    ApiResponse ares = await king.lip.stripe(
        '/customers/${king.todd.stripeCustomerId}/payment_methods',
        method: HttpMethod.get,
        params: {'type': 'card'});
    var body = ares.body;
    unpackFromStripePaymentMethods(body);
  }

  Future<ApiResponse> deleteOnStripe(King king, String pmId) async {
    if (king.todd.stripeCustomerId == '') {
      return ApiResponse();
    }
    ApiResponse ares = await king.lip.stripe(
      '/payment_methods/$pmId/detach',
      method: HttpMethod.post,
    );
    return ares;
  }

  void unpackFromStripePaymentMethods(Map<String, dynamic> body) {
    plastics.clear();
    for (final plasticJin in body['data']) {
      final plastic = Plastic();
      plastic.unpackFromApi(plasticJin);
      plastics[plastic.id] = plastic;
    }
  }

  @computed
  bool get isPlasticsEmpty {
    if (plastics.isEmpty) {
      return false;
    }
    return true;
  }

  List<Plastic> getNonDefaultPlastics(String defaultPlasticId) {
    List<Plastic> nonDef = [];
    for (final plastic in plastics.values) {
      if (plastic.id != defaultPlasticId) {
        nonDef.add(plastic);
      }
    }
    return nonDef;
  }

  Plastic getPlastic(String plasticId) {
    if (!plastics.containsKey(plasticId)) {
      final plastic = Plastic();
      plastics[plasticId] = plastic;
    }
    return plastics[plasticId] ?? Plastic();
  }
}

//NOTE: we will call 'card' 'plastic' to avoid name collisions
class Plastic = PlasticBase with _$Plastic;

abstract class PlasticBase with Store {
  String id = '';
  String brand = '';
  String country = '';
  int expMonth = 0;
  int expYear = 0;
  String last4 = '';

  void unpackFromApi(Map<String, dynamic> body) {
    id = readString(body, 'id');
    Map<String, dynamic> jinPlastic = body['card'];
    brand = readString(jinPlastic, 'brand');
    country = readString(jinPlastic, 'country');
    expMonth = readInt(jinPlastic, 'exp_month');
    expYear = readInt(jinPlastic, 'exp_year');
    last4 = readString(jinPlastic, 'last4');
  }

  String get brandDisplay {
    return intl.toBeginningOfSentenceCase(brand) ?? brand;
  }

  String get brandImageSrc {
    switch (brand) {
      case 'amex':
        return 'assets/based/amex_72.png';
      case 'cartes_bancaires':
        return 'assets/based/cartes_bancaires_72.png';
      case 'diners_club':
        return 'assets/based/diners_club_72.png';
      case 'discover':
        return 'assets/based/discover_72.png';
      case 'jcb':
        return 'assets/based/jcb_72.png';
      case 'mastercard':
        return 'assets/based/mastercard_72.png';
      case 'unionpay':
        return 'assets/based/unionpay_72.png';
      case 'visa':
        return 'assets/based/visa_72.png';
      default:
        return 'assets/based/loading.webp';
    }
  }

  Image get brandImage {
    return Image.asset(brandImageSrc, package: 'pushboy_lib');
  }

  String get expDate {
    return '${expMonth.pad02}/${expYear.pad02}';
  }
}
