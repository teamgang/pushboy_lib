// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'stripe_payment_methods.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$PaymentMethods on PaymentMethodsBase, Store {
  Computed<bool>? _$isPlasticsEmptyComputed;

  @override
  bool get isPlasticsEmpty =>
      (_$isPlasticsEmptyComputed ??= Computed<bool>(() => super.isPlasticsEmpty,
              name: 'PaymentMethodsBase.isPlasticsEmpty'))
          .value;

  @override
  String toString() {
    return '''
isPlasticsEmpty: ${isPlasticsEmpty}
    ''';
  }
}

mixin _$Plastic on PlasticBase, Store {
  @override
  String toString() {
    return '''

    ''';
  }
}
