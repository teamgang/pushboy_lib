import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

part 'surfer.g.dart';

class Surfer = SurferBase with _$Surfer;

abstract class SurferBase with Store {
  @observable
  String surferId = '';
  @observable
  String email = '';

  Future<void> loadFromApi(
    BuildContext context,
    String surferId,
  ) async {
    if (surferId == '') {
      return;
    }

    var king = King.of(context);
    print(king.todd.surferId);
    ApiResponse ares = await king.lip.api(
      EndpointsV1.surferGetById,
      payload: {'surfer_id': surferId},
    );
    var body = ares.body;
    unpackFromApi(body);
  }

  void unpackFromApi(Map<String, dynamic> body) {
    var temp = readString(body, 'surfer_id');
    if (surferId != temp) {
      surferId = temp;
    }
    temp = readString(body, 'email');
    if (email != temp) {
      email = temp;
    }
  }

  void loadFromAltSurfer(Surfer alt) {
    surferId = alt.surferId;
    email = alt.email;
  }
}
