import 'dart:io';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:hive_flutter/hive_flutter.dart';
import 'package:path_provider/path_provider.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

Future<King> makeDefaultKing() async {
  final directory = await getApplicationDocumentsDirectory();
  print('currently working in ${directory.path}');
  Directory('${directory.path}/hive/chan').create(recursive: true);
  Directory('${directory.path}/hive/net').create(recursive: true);
  await Hive.initFlutter('${directory.path}/hive');

  String? token = await FirebaseMessaging.instance.getToken() ?? '';
  final defaultConf = PbConf(
    firebaseToken: token,
    mockAutoSignIn: true,
    userType: UserType.surfer,
    rootDir: directory.path,
  );

  King king = King(conf: defaultConf);
  await king.initAsyncObjects();

  return king;
}
