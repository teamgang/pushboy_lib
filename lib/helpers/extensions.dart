import 'package:intl/intl.dart' as intl;

import 'package:pushboy_lib/pushboy_lib.dart';

extension ParseCents on int {
  String get uncents {
    //NOTE: add a tiny bit to make it positive
    return (this / 100.0 + .0000001).toStringAsFixed(2);
  }

  String get toUsd {
    return '\$${this.uncents}';
  }

  String get display {
    return intl.NumberFormat.decimalPattern().format(this);
  }

  String get pad02 {
    return intl.NumberFormat('00').format(this);
  }
}

extension UserTypeExtensions on UserType {
  String get refreshEndpoint {
    switch (this) {
      case UserType.developer:
        return EndpointsV1.developerJwtRefresh;
      case UserType.surfer:
        return EndpointsV1.surferJwtRefresh;
      default:
        return 'unhandled UserType case';
    }
  }

  String get registerWithEmailEndpoint {
    switch (this) {
      case UserType.developer:
        return EndpointsV1.developerRegisterWithEmail;
      case UserType.surfer:
        return EndpointsV1.surferRegisterWithEmail;
      default:
        return 'unhandled UserType case';
    }
  }

  String get signInWithAppleEndpoint {
    switch (this) {
      case UserType.developer:
        return EndpointsV1.developerSignInWithApple;
      case UserType.surfer:
        return EndpointsV1.surferSignInWithApple;
      default:
        return 'unhandled UserType case';
    }
  }

  String get signInWithEmailEndpoint {
    switch (this) {
      case UserType.developer:
        return EndpointsV1.developerSignInWithEmail;
      case UserType.surfer:
        return EndpointsV1.surferSignInWithEmail;
      default:
        return 'unhandled UserType case';
    }
  }

  String get signInWithGoogleEndpoint {
    switch (this) {
      case UserType.developer:
        return EndpointsV1.developerSignInWithGoogle;
      case UserType.surfer:
        return EndpointsV1.surferSignInWithGoogle;
      default:
        return 'unhandled UserType case';
    }
  }

  String get signOutEndpoint {
    switch (this) {
      case UserType.developer:
        return EndpointsV1.developerSignOut;
      case UserType.surfer:
        return EndpointsV1.surferSignOut;
      default:
        return 'unhandled UserType case';
    }
  }
}
