import 'package:flutter/material.dart';
import 'package:hive/hive.dart';
import 'package:logger/logger.dart';
import 'package:provider/provider.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class King {
  late Box box;
  PbConf conf;
  late Dad dad;
  late Deep deep;
  late Plogger log;
  late Lip lip;
  final navigatorKey = GlobalKey<NavigatorState>();
  late BuildContext routerContext;
  final Snacker snacker = Snacker();
  late ThemeSelect theme;
  late Todd todd;

  King({required this.conf}) {
    this.theme = ThemeSelect();

    // depends only on conf
    this.lip = Lip(this, pool: conf.apiPool);
    this.log = Plogger(
      level: Level.verbose,
      lip: lip,
      name: 'PushBoiApp',
      platform: conf.platform,
    );

    // depends on 'this'
    this.dad = Dad(this);
    this.deep = Deep(this);
    this.todd = Todd(this);
    todd.userType = conf.userType;

    //if (this.conf.mockAutoSignIn) {
    //this.todd.signInWithEmail(email: '2@2.2', password: 'asdf');
    //}
  }

  initAsyncObjects() async {
    box = await Hive.openBox('box');

    todd.loadLogin();

    deep.initAppLinks();
  }

  static King of(BuildContext context) {
    return Provider.of<King>(context, listen: false);
  }

  // signOutCleanup should destroy all personal information
  void signOutCleanup() {
    //this.pac = Pac();
    this.dad = Dad(this);
  }
}

navPush(BuildContext context, Widget widget) {
  Navigator.push(
      context, MaterialPageRoute(builder: (BuildContext context) => widget));
}

navPushReplacement(BuildContext context, Widget widget) {
  Navigator.push(
      context, MaterialPageRoute(builder: (BuildContext context) => widget));
}
