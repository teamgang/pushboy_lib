import 'package:flutter/foundation.dart' show kIsWeb;
import 'package:universal_io/io.dart';

import 'package:pushboy_lib/services/address_pool.dart';

enum ClockFormat { twelve, twentyFour }

enum UserType { none, developer, surfer }

enum DevicePlatform { none, and }

class PbConf {
  final AddressPool apiPool = AddressPool();

  //final String env = 'dev';
  final String env = 'prod';
  final String logLevel = 'all';
  //src: 'https://via.placeholder.com/350x150',

  final String tzFormat = 'standard';

  final int requestTimeout = 10;

  final int cacheTimeoutMinutes = 3;

  final String stripePublishableKey =
      'sk_test_51JwzllLK9p9uYK5RrSzjpQF8C5fI7uCWd9mZ4Fix5ZNoKHSttTZUigUp4pY1ZVW3hWdz1PpaN6DhU08Wvy39ha9G00ffzbdLuk';
  final String stripeReturnUrl = 'https://pushboi.io/api/v1/stripe/callback';

  bool mockAutoSignIn;
  late String imagesUrl;
  late String loadingImageUrl;
  final UserType userType;
  String firebaseToken;
  late String platform;
  final String rootDir;
  late final String cadDir;
  late final String channelSubDir;
  late final String networkSubDir;

  final String appleTeamId = '3Q3F63NFX6';

  bool debug = true;

  final zerosUuid = '00000000-0000-0000-0000-000000000000';

  PbConf({
    required this.firebaseToken,
    this.mockAutoSignIn = false,
    required this.userType,
    required this.rootDir,
  }) {
    print('Configuration initializaing...');

    if (kIsWeb) {
      platform = 'web';
    } else {
      if (Platform.isAndroid) {
        platform = 'android';
      } else if (Platform.isIOS) {
        platform = 'ios';
      } else if (Platform.isLinux) {
        platform = 'linux';
      } else if (Platform.isWindows) {
        platform = 'windows';
      } else if (Platform.isMacOS) {
        platform = 'macos';
      } else if (Platform.isFuchsia) {
        platform = 'fuchsia';
      }
    }

    if (platform == 'android' || platform == 'ios') {
      cadDir = '$rootDir/cad';
      channelSubDir = '$rootDir/cad/chan';
      networkSubDir = '$rootDir/cad/net';
      //Directory(channelSubDir).deleteSync(recursive: true);
      //Directory(networkSubDir).deleteSync(recursive: true);
      Directory(channelSubDir).createSync(recursive: true);
      Directory(networkSubDir).createSync(recursive: true);
    }

    switch (this.env) {
      case 'dev':
        switch (platform) {
          case 'web':
          case 'ios':
            apiPool.addAddress('http://localhost:8000', 'api');
            apiPool.addAddress('http://localhost:80', 'api');
            imagesUrl = 'http://localhost/img';
            break;

          case 'android':
            //apiPool.addAddress('http://10.0.2.2:8000', 'api');
            apiPool.addAddress('http://10.0.2.2:80', 'api');
            imagesUrl = 'http://10.0.2.2/img';
            break;

          case 'linux':
          case 'windows':
          case 'macos':
          case 'fuchsia':
          default:
            apiPool.addAddress('http://localhost:800', 'api');
            imagesUrl = 'http://localhost/img';
        }
        break;

      case 'lab':
        throw Exception('lab environment not configured');

      case 'prod':
        apiPool.addAddress('https://pushboi.io', 'api');
        imagesUrl = 'https://pushboi.io/img';
        break;

      default:
        throw Exception('ERROR: env could not be found: $env');
    }

    loadingImageUrl = '$imagesUrl/loading.webp';
    print(loadingImageUrl);

    print('API Url Pool: ${apiPool.printfAddresses()}');
    if (apiPool.isEmpty) {
      throw Exception('ERROR: no apiPool are set');
    }
    print('Configuration initialization completed.');
  }

  String fixImageSrc(String src) {
    //if (src.isEmpty) {
    //return loadingImageUrl;
    //}

    if (src.length > 4 && !src.startsWith('http') && !src.startsWith('blob')) {
      src = '$imagesUrl/$src';
    }

    if (debug) {
      src = src.replaceFirst('http://localhost/img', imagesUrl);
      src = src.replaceFirst('http://10.0.0.2/img', imagesUrl);
      src = src.replaceFirst('http://pushboi.io/img', imagesUrl);
      src = src.replaceFirst('https://pushboi.io/img', imagesUrl);
    }

    return src;
  }

  bool get isAndroid {
    return platform == 'android';
  }

  bool get isIos {
    return platform == 'ios';
  }

  bool get isLinux {
    return platform == 'linux';
  }

  bool get isMacOs {
    return platform == 'ios';
  }

  bool get isWeb {
    return platform == 'web';
  }

  String get signInWithAppleClientId {
    if (isAndroid) {
      return 'io.pushboi.and';
    } else if (isIos || isMacOs) {
      return 'io.pushboi.ios';
    } else {
      return 'PlatformNotSupported';
    }
  }

  Uri get signInWithApplePapiCallback {
    if (isWeb) {
      return Uri.parse('https://pushboi.io');
    } else if (isAndroid) {
      return Uri.parse(
          'https://pushboi.io/api/v1/surfer/signIn.withApple.callback.android');
    } else {
      return Uri.parse('https://notset');
    }
  }

  String calling() {
    var data = {'hi': 'okay'};
    return 'intent://callback?$data#Intent;package=YOUR.PACKAGE.IDENTIFIER;scheme=signinwithapple;end';
  }
}
