import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';

import 'package:pushboy_lib/dad/dad.dart';
import 'package:pushboy_lib/services/pov.dart';
import 'package:pushboy_lib/utils/payment_details.dart';

class Misc {
  String id = '';
}

enum WhichColor { error, primary, secondary }

class Args {
  const Args({
    this.amount,
    this.autonav,
    this.bill,
    this.boi,
    this.boiId,
    this.boiSender,
    this.boiTx,
    this.channel,
    this.channelSub,
    this.charge,
    this.developer,
    this.editStatic,
    this.id,
    this.initialTab,
    this.misc,
    this.month,
    this.network,
    this.networkSub,
    this.paymentAttemptId,
    this.paymentDetails,
    this.payment,
    this.plan,
    this.planId,
    this.pov,
    this.project,
    this.stripeUrl,
    this.surfer,
    this.surferId,
    this.requestChange,
    this.token,
    this.receivedNotification,
    this.whichColor,
    this.xfile,
    this.year,
  });
  //

  final String? amount;
  final String? autonav;
  final Bill? bill;
  final Boi? boi;
  final String? boiId;
  final BoiSender? boiSender;
  final BoiTx? boiTx;
  final Channel? channel;
  final ChannelSub? channelSub;
  final Charge? charge;
  final Developer? developer;
  final bool? editStatic;
  final String? id;
  final int? initialTab;
  final Misc? misc;
  final int? month;
  final Network? network;
  final NetworkSub? networkSub;
  final String? paymentAttemptId;
  final PaymentDetails? paymentDetails;
  final Payment? payment;
  final Plan? plan;
  final int? planId;
  final Pov? pov;
  final Project? project;
  final ReceivedNotification? receivedNotification;
  final String? stripeUrl;
  final Surfer? surfer;
  final String? surferId;
  final RequestChange? requestChange;
  final String? token;
  final WhichColor? whichColor;
  final XFile? xfile;
  final int? year;
}

class ProcessedArgs {
  const ProcessedArgs({
    required this.amount,
    required this.autonav,
    required this.bill,
    required this.boi,
    required this.boiId,
    required this.boiSender,
    required this.boiTx,
    required this.channel,
    required this.channelSub,
    required this.charge,
    required this.developer,
    required this.editStatic,
    required this.id,
    required this.initialTab,
    required this.misc,
    required this.month,
    required this.network,
    required this.networkSub,
    required this.paymentAttemptId,
    required this.paymentDetails,
    required this.payment,
    required this.plan,
    required this.planId,
    required this.pov,
    required this.project,
    required this.receivedNotification,
    required this.stripeUrl,
    required this.surfer,
    required this.surferId,
    required this.requestChange,
    required this.token,
    required this.whichColor,
    required this.xfile,
    required this.year,
  });
  //

  final String amount;
  final String autonav;
  final Bill bill;
  final Boi boi;
  final String boiId;
  final BoiSender boiSender;
  final BoiTx boiTx;
  final Channel channel;
  final ChannelSub channelSub;
  final Charge charge;
  final Developer developer;
  final bool editStatic;
  final String id;
  final int initialTab;
  final Misc misc;
  final int month;
  final Network network;
  final NetworkSub networkSub;
  final String paymentAttemptId;
  final PaymentDetails paymentDetails;
  final Payment payment;
  final Plan plan;
  final int planId;
  final Pov pov;
  final Project project;
  final ReceivedNotification receivedNotification;
  final String stripeUrl;
  final Surfer surfer;
  final String surferId;
  final RequestChange requestChange;
  final String token;
  final WhichColor whichColor;
  final XFile xfile;
  final int year;
}

ProcessedArgs getProcessedArgs(RouteSettings settings) {
  Args rawArgs;
  if (settings.arguments == null) {
    rawArgs = const Args();
  } else {
    rawArgs = settings.arguments as Args;
  }

  return ProcessedArgs(
    amount: rawArgs.amount ?? '',
    autonav: rawArgs.autonav ?? '',
    bill: rawArgs.bill ?? Bill(),
    boi: rawArgs.boi ?? Boi(),
    boiId: rawArgs.boiId ?? '',
    boiSender: rawArgs.boiSender ?? BoiSender(),
    boiTx: rawArgs.boiTx ?? BoiTx(),
    channel: rawArgs.channel ?? Channel(),
    channelSub: rawArgs.channelSub ?? ChannelSub(),
    charge: rawArgs.charge ?? Charge(),
    developer: rawArgs.developer ?? Developer(),
    editStatic: rawArgs.editStatic ?? false,
    id: rawArgs.id ?? '',
    initialTab: rawArgs.initialTab ?? 0,
    misc: rawArgs.misc ?? Misc(),
    month: rawArgs.month ?? 0,
    network: rawArgs.network ?? Network(),
    networkSub: rawArgs.networkSub ?? NetworkSub(),
    paymentAttemptId: rawArgs.paymentAttemptId ?? '',
    paymentDetails: rawArgs.paymentDetails ?? PaymentDetails(),
    payment: rawArgs.payment ?? Payment(),
    plan: rawArgs.plan ?? Plan(),
    planId: rawArgs.planId ?? 0,
    pov: rawArgs.pov ?? Pov.developer,
    project: rawArgs.project ?? Project(),
    receivedNotification:
        rawArgs.receivedNotification ?? ReceivedNotification(),
    stripeUrl: rawArgs.stripeUrl ?? '',
    surfer: rawArgs.surfer ?? Surfer(),
    surferId: rawArgs.surferId ?? '',
    requestChange: rawArgs.requestChange ?? RequestChange(),
    token: rawArgs.token ?? '',
    whichColor: rawArgs.whichColor ?? WhichColor.error,
    xfile: rawArgs.xfile ?? XFile('assets/based/loading.webp'),
    year: rawArgs.year ?? 0,
  );
}

class RouteParams {
  RouteParams({
    required this.billId,
    required this.boiId,
    required this.channelId,
    required this.chargeId,
    required this.developerId,
    required this.miscId,
    required this.paymentId,
    required this.paymentAttemptId,
    required this.networkId,
    required this.projectId,
    required this.token,
  });
  final String billId;
  final String boiId;
  final String channelId;
  final String chargeId;
  final String developerId;
  final String miscId;
  final String paymentId;
  final String paymentAttemptId;
  final String networkId;
  final String projectId;
  final String token;
}
