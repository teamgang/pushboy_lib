import 'dart:html';

import 'package:pushboy_lib/pushboy_lib.dart';

RouteParams getRouteParams(
  context, {
  required ProcessedArgs args,
}) {
  var uri = Uri.dataFromString(window.location.href);
  Map<String, String> params = uri.queryParameters;

  final routeParams = RouteParams(
    billId: params['billId'] ?? '',
    boiId: params['boiId'] ?? '',
    channelId: params['channelId'] ?? '',
    chargeId: params['chargeId'] ?? '',
    developerId: params['developerId'] ?? '',
    miscId: params['miscId'] ?? '',
    paymentId: params['paymentId'] ?? '',
    paymentAttemptId: params['paymentAttemptId'] ?? '',
    networkId: params['networkId'] ?? '',
    projectId: params['projectId'] ?? '',
    token: params['token'] ?? '',
  );

  if (routeParams.miscId.isNotEmpty) {
    args.misc.id = routeParams.miscId;
  }

  if (routeParams.billId.isNotEmpty && !args.bill.isLoaded) {
    args.bill.billId = routeParams.billId;
    args.bill.loadFromApi(context, routeParams.billId);
  }

  if (routeParams.boiId.isNotEmpty && !args.boi.isLoaded) {
    args.boi.boiId = routeParams.boiId;
    args.boi.loadFromApi(context, routeParams.boiId);
  }

  if (routeParams.channelId.isNotEmpty && !args.channel.isLoaded) {
    args.channel.channelId = routeParams.channelId;
    args.channel.loadFromApi(context, routeParams.channelId);
  }

  if (routeParams.chargeId.isNotEmpty && !args.charge.isLoaded) {
    args.charge.chargeId = routeParams.chargeId;
    args.charge.loadFromApi(context, routeParams.chargeId);
  }

  if (routeParams.networkId.isNotEmpty && !args.network.isLoaded) {
    args.network.networkId = routeParams.networkId;
    args.network.loadFromApi(context, routeParams.networkId);
  }

  if (routeParams.paymentId.isNotEmpty && !args.payment.isLoaded) {
    args.payment.paymentId = routeParams.paymentId;
    args.payment.loadFromApi(context, routeParams.paymentId);
  }

  if (routeParams.projectId.isNotEmpty && !args.network.isLoaded) {
    args.project.projectId = routeParams.projectId;
    args.project.loadFromApi(context, routeParams.projectId);
  }

  if (routeParams.token.isNotEmpty) {
    args.requestChange.token = routeParams.token;
  }

  return routeParams;
}

Future<void> updateUrlParams(
  ProcessedArgs args,
) async {
  final boi = args.boi;
  final channel = args.channel;
  final charge = args.charge;
  final network = args.network;
  final misc = args.misc;
  final payment = args.payment;
  final project = args.project;
  final requestChange = args.requestChange;

  String title = '';
  if (boi.boiId.isNotEmpty) {
    title = 'Notification: ${boi.title}';
  } else if (channel.channelId.isNotEmpty) {
    title = 'Channel: ${channel.name}';
  } else if (charge.chargeId.isNotEmpty) {
    title = 'Charge';
  } else if (network.networkId.isNotEmpty) {
    title = 'Network: ${network.name}';
  } else if (project.projectId.isNotEmpty) {
    title = 'Project: ${project.name}';
  }

  Map<String, dynamic> params = {};
  if (boi.boiId.isNotEmpty) {
    params['boiId'] = boi.boiId;
  }
  if (channel.channelId.isNotEmpty) {
    params['channelId'] = channel.channelId;
  }
  if (charge.chargeId.isNotEmpty) {
    params['chargeId'] = charge.chargeId;
  }
  if (misc.id.isNotEmpty) {
    params['miscId'] = misc.id;
  }
  if (network.networkId.isNotEmpty) {
    params['networkId'] = network.networkId;
  }
  if (payment.paymentId.isNotEmpty) {
    params['paymentId'] = payment.paymentId;
  }
  if (project.projectId.isNotEmpty) {
    params['projectId'] = project.projectId;
  }
  if (requestChange.token.isNotEmpty) {
    params['token'] = requestChange.token;
  }

  final newUri =
      Uri.parse(Uri.base.toString()).replace(queryParameters: params);

  Future.delayed(
    const Duration(milliseconds: 10),
    () => window.history.replaceState(
      null,
      title,
      newUri.toString(),
    ),
  );
}
