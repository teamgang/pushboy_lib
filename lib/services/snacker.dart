import 'dart:async';
import 'package:mobx/mobx.dart';

part 'snacker.g.dart';

class Snacker = SnackerBase with _$Snacker;

abstract class SnackerBase with Store {
  ObservableList<Snack> snacks = ObservableList<Snack>();

  @action
  void addSnack(Snack snack) {
    snacks.removeWhere((item) => (item.code == snack.code));
    snacks.add(snack);
  }

  @action
  void removeSnack(Snack snack) {
    snacks.removeWhere((item) => (item.code == snack.code));
  }

  @action
  void startTimer(Snack snack) {
    if (snack.timeout > 0) {
      Timer(Duration(milliseconds: snack.timeout), () {
        removeSnack(snack);
      });
    }
  }
}

class Snack {
  Snack(this.code, this.msg, {this.timeout = 0});
  int code;
  String msg;
  int timeout;
}
