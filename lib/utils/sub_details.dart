import 'package:flutter/material.dart';
import 'package:mobx/mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

part 'sub_details.g.dart';

enum SubType { network, channel }

class SubDetails = SubDetailsBase with _$SubDetails;

abstract class SubDetailsBase with Store {
  final String apiUrl = 'https://pushboi.io/s/';
  final Channel channel = Channel();
  final Network network = Network();
  @observable
  String link = '';
  @observable
  String surferId = '';
  @observable
  bool subscribeAsSurfer = false;
  @observable
  bool subscribeToAllChannels = true;

  @action
  void init({required Todd todd}) {
    surferId = todd.surferId;
    if (surferId.isNotEmpty) {
      subscribeAsSurfer = true;
    }
  }

  @computed
  String get subFoundDescription {
    switch (subType) {
      case SubType.network:
        if (network.networkId.isEmpty) {
          return 'Network not found.';
        } else {
          return 'Network found!\n\nName: ${network.name}\n${network.description}';
        }

      case SubType.channel:
        if (network.networkId.isEmpty) {
          return 'Network not found';
        } else {
          if (channel.channelId.isEmpty) {
            return 'Cannot find the channel $channelRoute in the network $networkRoute';
          } else {
            return 'Found channel!\n\'${channel.name}\' in network \'${network.name}\' ';
          }
        }
    }
  }

  @computed
  bool get isLinkValid {
    if (link.startsWith(apiUrl)) {
      return true;
    } else {
      return false;
    }
  }

  @computed
  SubType get subType {
    if (routeParts.length >= 2) {
      return SubType.channel;
    } else {
      return SubType.network;
    }
  }

  @computed
  String get fullRoute {
    String fullRoute = link.replaceAll(apiUrl, '');
    if (fullRoute.endsWith('/')) {
      fullRoute = fullRoute.substring(0, fullRoute.length - 1);
    }
    return fullRoute;
  }

  @computed
  List<String> get routeParts {
    return fullRoute.split('/');
  }

  @computed
  String get networkRoute {
    return routeParts[0];
  }

  @computed
  String get channelRoute {
    if (routeParts.length >= 2) {
      return routeParts[1];
    } else {
      return '';
    }
  }

  @computed
  bool get isReadyToSubscribe {
    switch (subType) {
      case SubType.network:
        if (network.networkId.isEmpty) {
          return false;
        }
        return true;

      case SubType.channel:
        if (network.networkId.isEmpty || channel.channelId.isEmpty) {
          return false;
        }
        return true;
    }
  }

  @action
  Future<void> getNetworkAndChannelDetails(BuildContext context) async {
    network.clear();
    channel.clear();

    if (isLinkValid) {
      switch (subType) {
        case SubType.network:
          network.loadFromApiWithRoute(
            context,
            networkRoute,
          );
          break;
        case SubType.channel:
          network.loadFromApiWithRoute(
            context,
            networkRoute,
          );
          channel.loadFromApiWithRoute(
            context,
            networkRoute: networkRoute,
            channelRoute: channelRoute,
          );
          break;
      }
    }
  }

  Future<ApiResponse> executeSubscribe(King king) async {
    Map<String, dynamic> payload = {
      'channel_id': channel.channelId,
      'instance_id': king.conf.firebaseToken,
      'network_id': network.networkId,
      'platform': king.conf.platform,
      'subscribe_as_surfer': subscribeAsSurfer,
      'subscribe_to_all_channels': subscribeToAllChannels,
    };

    return await king.lip.api(
      EndpointsV1.networkSubAddNew,
      payload: payload,
    );
  }
}
