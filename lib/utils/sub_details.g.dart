// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'sub_details.dart';

// **************************************************************************
// StoreGenerator
// **************************************************************************

// ignore_for_file: non_constant_identifier_names, unnecessary_brace_in_string_interps, unnecessary_lambdas, prefer_expression_function_bodies, lines_longer_than_80_chars, avoid_as, avoid_annotating_with_dynamic

mixin _$SubDetails on SubDetailsBase, Store {
  Computed<String>? _$subFoundDescriptionComputed;

  @override
  String get subFoundDescription => (_$subFoundDescriptionComputed ??=
          Computed<String>(() => super.subFoundDescription,
              name: 'SubDetailsBase.subFoundDescription'))
      .value;
  Computed<bool>? _$isLinkValidComputed;

  @override
  bool get isLinkValid =>
      (_$isLinkValidComputed ??= Computed<bool>(() => super.isLinkValid,
              name: 'SubDetailsBase.isLinkValid'))
          .value;
  Computed<SubType>? _$subTypeComputed;

  @override
  SubType get subType =>
      (_$subTypeComputed ??= Computed<SubType>(() => super.subType,
              name: 'SubDetailsBase.subType'))
          .value;
  Computed<String>? _$fullRouteComputed;

  @override
  String get fullRoute =>
      (_$fullRouteComputed ??= Computed<String>(() => super.fullRoute,
              name: 'SubDetailsBase.fullRoute'))
          .value;
  Computed<List<String>>? _$routePartsComputed;

  @override
  List<String> get routeParts =>
      (_$routePartsComputed ??= Computed<List<String>>(() => super.routeParts,
              name: 'SubDetailsBase.routeParts'))
          .value;
  Computed<String>? _$networkRouteComputed;

  @override
  String get networkRoute =>
      (_$networkRouteComputed ??= Computed<String>(() => super.networkRoute,
              name: 'SubDetailsBase.networkRoute'))
          .value;
  Computed<String>? _$channelRouteComputed;

  @override
  String get channelRoute =>
      (_$channelRouteComputed ??= Computed<String>(() => super.channelRoute,
              name: 'SubDetailsBase.channelRoute'))
          .value;
  Computed<bool>? _$isReadyToSubscribeComputed;

  @override
  bool get isReadyToSubscribe => (_$isReadyToSubscribeComputed ??=
          Computed<bool>(() => super.isReadyToSubscribe,
              name: 'SubDetailsBase.isReadyToSubscribe'))
      .value;

  late final _$linkAtom = Atom(name: 'SubDetailsBase.link', context: context);

  @override
  String get link {
    _$linkAtom.reportRead();
    return super.link;
  }

  @override
  set link(String value) {
    _$linkAtom.reportWrite(value, super.link, () {
      super.link = value;
    });
  }

  late final _$surferIdAtom =
      Atom(name: 'SubDetailsBase.surferId', context: context);

  @override
  String get surferId {
    _$surferIdAtom.reportRead();
    return super.surferId;
  }

  @override
  set surferId(String value) {
    _$surferIdAtom.reportWrite(value, super.surferId, () {
      super.surferId = value;
    });
  }

  late final _$subscribeAsSurferAtom =
      Atom(name: 'SubDetailsBase.subscribeAsSurfer', context: context);

  @override
  bool get subscribeAsSurfer {
    _$subscribeAsSurferAtom.reportRead();
    return super.subscribeAsSurfer;
  }

  @override
  set subscribeAsSurfer(bool value) {
    _$subscribeAsSurferAtom.reportWrite(value, super.subscribeAsSurfer, () {
      super.subscribeAsSurfer = value;
    });
  }

  late final _$subscribeToAllChannelsAtom =
      Atom(name: 'SubDetailsBase.subscribeToAllChannels', context: context);

  @override
  bool get subscribeToAllChannels {
    _$subscribeToAllChannelsAtom.reportRead();
    return super.subscribeToAllChannels;
  }

  @override
  set subscribeToAllChannels(bool value) {
    _$subscribeToAllChannelsAtom
        .reportWrite(value, super.subscribeToAllChannels, () {
      super.subscribeToAllChannels = value;
    });
  }

  late final _$getNetworkAndChannelDetailsAsyncAction = AsyncAction(
      'SubDetailsBase.getNetworkAndChannelDetails',
      context: context);

  @override
  Future<void> getNetworkAndChannelDetails(BuildContext context) {
    return _$getNetworkAndChannelDetailsAsyncAction
        .run(() => super.getNetworkAndChannelDetails(context));
  }

  late final _$SubDetailsBaseActionController =
      ActionController(name: 'SubDetailsBase', context: context);

  @override
  void init({required Todd todd}) {
    final _$actionInfo = _$SubDetailsBaseActionController.startAction(
        name: 'SubDetailsBase.init');
    try {
      return super.init(todd: todd);
    } finally {
      _$SubDetailsBaseActionController.endAction(_$actionInfo);
    }
  }

  @override
  String toString() {
    return '''
link: ${link},
surferId: ${surferId},
subscribeAsSurfer: ${subscribeAsSurfer},
subscribeToAllChannels: ${subscribeToAllChannels},
subFoundDescription: ${subFoundDescription},
isLinkValid: ${isLinkValid},
subType: ${subType},
fullRoute: ${fullRoute},
routeParts: ${routeParts},
networkRoute: ${networkRoute},
channelRoute: ${channelRoute},
isReadyToSubscribe: ${isReadyToSubscribe}
    ''';
  }
}
