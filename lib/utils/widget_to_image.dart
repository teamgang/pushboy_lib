import 'dart:ui' as ui;
import 'dart:typed_data' as typed;
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

Future<List<int>> widgetToImage(GlobalKey key) async {
  final RenderRepaintBoundary boundary =
      key.currentContext!.findRenderObject() as RenderRepaintBoundary;
  final ui.Image image = await boundary.toImage(pixelRatio: 1);
  final typed.ByteData? byteData =
      await image.toByteData(format: ui.ImageByteFormat.png);
  final typed.Uint8List pngBytes = byteData!.buffer.asUint8List();
  return pngBytes;
}
