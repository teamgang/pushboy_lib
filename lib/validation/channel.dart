import 'package:pushboy_lib/validation/validation.dart';

class ValidateChannel {
  static const int abbrevMaxLength = 5;
  static const int abbrevMinLength = 1;
  static const int descriptionMaxLength = 4000;
  static const int descriptionMinLength = 0;
  static const int nameMaxLength = 100;
  static const int nameMinLength = 2;
  static const int notesMaxLength = 100;
  static const int notesMinLength = 0;
  static const int routeMaxLength = 200;
  static const int routeMinLength = 1;
  static const int staticBodyMaxLength = 500;
  static const int staticBodyMinLength = 0;
  static const int staticThumbUrlMaxLength = 500;
  static const int staticThumbUrlMinLength = 0;
  static const int staticTitleMaxLength = 100;
  static const int staticTitleMinLength = 0;
  static const int staticUrlMaxLength = 5000;
  static const int staticUrlMinLength = 0;
  static const int thumbUrlMaxLength = 500;
  static const int thumbUrlMinLength = 0;

  static bool allFields({
    required String abbrev,
    required String description,
    required String name,
    required String notes,
    required String route,
    required String staticBody,
    required String staticThumbUrl,
    required String staticTitle,
    required String staticUrl,
    required String thumbUrl,
  }) {
    if (ValidateChannel.abbrev(abbrev).isNotValid ||
        ValidateChannel.description(description).isNotValid ||
        ValidateChannel.name(name).isNotValid ||
        ValidateChannel.notes(notes).isNotValid ||
        ValidateChannel.route(route).isNotValid ||
        ValidateChannel.staticBody(staticBody).isNotValid ||
        ValidateChannel.staticThumbUrl(staticThumbUrl).isNotValid ||
        ValidateChannel.staticTitle(staticTitle).isNotValid ||
        ValidateChannel.staticUrl(staticUrl).isNotValid ||
        ValidateChannel.thumbUrl(thumbUrl).isNotValid) {
      return false;
    }
    return true;
  }

  static Validation staticBody(String value) {
    var validation = Validation();
    return validation;
  }

  static Validation staticThumbUrl(String value) {
    var validation = Validation();
    return validation;
  }

  static Validation staticTitle(String value) {
    var validation = Validation();
    return validation;
  }

  static Validation staticUrl(String value) {
    var validation = Validation();
    return validation;
  }

  static Validation thumbUrl(String value) {
    var validation = Validation();
    return validation;
  }

  static Validation abbrev(String value) {
    var validation = Validation();
    if (value.length > abbrevMaxLength) {
      validation
          .setError('Abbreviation must be under $abbrevMaxLength characters');
    }
    if (value.length < abbrevMinLength) {
      validation.setError(
          'Abbreviation must be at least $abbrevMinLength characters');
    }
    return validation;
  }

  static Validation description(String value) {
    var validation = Validation();
    if (value.length > descriptionMaxLength) {
      validation.setError(
          'Description must be under $descriptionMaxLength characters');
    }
    if (value.length < descriptionMinLength) {
      validation.setError(
          'Description must be at least $descriptionMinLength characters');
    }
    return validation;
  }

  static Validation name(String value) {
    var validation = Validation();
    if (value.length > nameMaxLength) {
      validation.setError('Name must be under $nameMaxLength characters');
    }
    if (value.length < nameMinLength) {
      validation.setError('Name must be at least $nameMinLength characters');
    }
    return validation;
  }

  static Validation notes(String value) {
    var validation = Validation();
    if (value.length > notesMaxLength) {
      validation.setError('Notes must be under $notesMaxLength characters');
    }
    if (value.length < notesMinLength) {
      validation.setError('Notes must be at least $notesMinLength characters');
    }
    return validation;
  }

  static Validation route(String value) {
    var validation = Validation();
    if (value.length > routeMaxLength) {
      validation.setError('Route  must be under $routeMaxLength characters');
    }
    if (value.length < routeMinLength) {
      validation.setError('Route  must be at least $routeMinLength characters');
    }
    return validation;
  }
}
