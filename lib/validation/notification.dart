import 'package:pushboy_lib/validation/validation.dart';

class ValidateNotification {
  static const int abbrevMaxLength = 3;
  static const int abbrevMinLength = 2;
  static const int bodyMaxLength = 1000;
  static const int bodyMinLength = 0;
  static const int notesMaxLength = 1000;
  static const int notesMinLength = 0;
  static const int thumbUrlMaxLength = 3000;
  static const int thumbUrlMinLength = 0;
  static const int titleMaxLength = 100;
  static const int titleMinLength = 0;
  static const int urlMaxLength = 3000;
  static const int urlMinLength = 0;

  static bool allFields({
    required String abbrev,
    required String body,
    required String notes,
    required String thumbUrl,
    required String title,
    required String url,
  }) {
    if (ValidateNotification.abbrev(abbrev).isNotValid ||
        ValidateNotification.body(body).isNotValid ||
        ValidateNotification.notes(notes).isNotValid ||
        ValidateNotification.thumbUrl(thumbUrl).isNotValid ||
        ValidateNotification.title(title).isNotValid ||
        ValidateNotification.url(url).isNotValid) {
      return false;
    }
    return true;
  }

  static Validation abbrev(String value) {
    var validation = Validation();
    if (value.length > abbrevMaxLength) {
      validation
          .setError('Abbreviation must be under $abbrevMaxLength characters');
    }
    if (value.length < abbrevMinLength) {
      validation.setError(
          'Abbreviation must be at least $abbrevMinLength characters');
    }
    return validation;
  }

  static Validation body(String value) {
    var validation = Validation();
    if (value.length > bodyMaxLength) {
      validation.setError('Body must be under $bodyMaxLength characters');
    }
    if (value.length < bodyMinLength) {
      validation.setError('Body must be at least $bodyMinLength characters');
    }
    return validation;
  }

  static Validation notes(String value) {
    var validation = Validation();
    if (value.length > notesMaxLength) {
      validation.setError('Notes must be under $notesMaxLength characters');
    }
    if (value.length < notesMinLength) {
      validation.setError('Notes must be at least $notesMinLength characters');
    }
    return validation;
  }

  static Validation thumbUrl(String value) {
    var validation = Validation();
    if (value.length > thumbUrlMaxLength) {
      validation
          .setError('Thumb URL  must be under $thumbUrlMaxLength characters');
    }
    if (value.length < thumbUrlMinLength) {
      validation.setError(
          'Thumb URL  must be at least $thumbUrlMinLength characters');
    }
    return validation;
  }

  static Validation title(String value) {
    var validation = Validation();
    if (value.length > titleMaxLength) {
      validation.setError('Name must be under $titleMaxLength characters');
    }
    if (value.length < titleMinLength) {
      validation.setError('Name must be at least $titleMinLength characters');
    }
    return validation;
  }

  static Validation url(String value) {
    var validation = Validation();
    if (value.length > urlMaxLength) {
      validation.setError('URL  must be under $urlMaxLength characters');
    }
    if (value.length < urlMinLength) {
      validation.setError('URL  must be at least $urlMinLength characters');
    }
    return validation;
  }
}
