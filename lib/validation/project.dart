import 'package:pushboy_lib/validation/validation.dart';

class ValidateProject {
  static const int notesMaxLength = 20000;
  static const int nameMaxLength = 140;
  static const int nameMinLength = 2;

  static bool allFields({
    required String notes,
    required String name,
  }) {
    if (ValidateProject.notes(notes).isNotValid ||
        ValidateProject.name(name).isNotValid ||
        ValidateProject.billing().isNotValid) {
      return false;
    }
    return true;
  }

  static Validation billing() {
    var validation = Validation();
    return validation;
  }

  static Validation notes(String value) {
    var validation = Validation();
    if (value.length > notesMaxLength) {
      validation.setError('Notes must be under $notesMaxLength characters');
    }
    return validation;
  }

  static Validation name(String value) {
    var validation = Validation();
    if (value.length > nameMaxLength) {
      validation.setError('Name must be under $nameMaxLength characters');
    }
    if (value.length < nameMinLength) {
      validation.setError('Name must be at least $nameMinLength characters');
    }
    return validation;
  }
}
