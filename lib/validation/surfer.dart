import 'package:pushboy_lib/validation/validation.dart';

class ValidateSurfer {
  static const int emailMaxLength = 200;
  static const int emailMinLength = 5;
  static const int passwordMinLength = 6;

  static bool allFields({
    required String email,
  }) {
    if (ValidateSurfer.email(email).isNotValid) {
      return false;
    }
    return true;
  }

  static Validation email(String value) {
    var validation = Validation();
    if (value.length > emailMaxLength) {
      validation.setError('Email must be under $emailMaxLength characters');
    }
    if (value.length < emailMinLength) {
      validation.setError('Email must be at least $emailMinLength characters');
    }
    return validation;
  }

  static Validation password(String value) {
    var validation = Validation();
    if (value.length < passwordMinLength) {
      validation
          .setError('Password must be at least $passwordMinLength characters');
    }
    return validation;
  }
}
