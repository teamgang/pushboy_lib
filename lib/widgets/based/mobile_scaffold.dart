import 'package:awesome_notifications/awesome_notifications.dart';
import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class MobileScaffold extends StatefulWidget {
  const MobileScaffold({required this.body});
  final Widget body;

  @override
  MobileScaffoldState createState() => MobileScaffoldState();
}

class MobileScaffoldState extends State<MobileScaffold> {
  bool _isAwesomeNotificationsAllowed = false;

  @override
  initState() {
    super.initState();
    _setIsAllowedIfAllowed();
  }

  _setIsAllowedIfAllowed() {
    AwesomeNotifications().isNotificationAllowed().then((isAllowed) {
      setState(() {
        _isAwesomeNotificationsAllowed = isAllowed;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final name = ModalRoute.of(context)?.settings.name;
    final king = King.of(context);

    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Stack(
        children: <Widget>[
          widget.body,
          BasedSnackBar(),
        ],
      ),
      appBar: AppBar(
        automaticallyImplyLeading: true,
        title: const Text('Pushboi'),
        actions: <Widget>[
          _isAwesomeNotificationsAllowed
              ? const SizedBox.shrink()
              : IconButton(
                  icon: const Icon(Icons.warning, color: Colors.red),
                  onPressed: () {
                    var name = ModalRoute.of(context)?.settings.name;
                    if (name != Routes.warnings) {
                      Navigator.of(context).pushNamed(Routes.warnings);
                    }
                  },
                ),
          //

          name == Routes.home
              ? IconButton(
                  icon: const Icon(Icons.refresh, color: Colors.white),
                  onPressed: () {
                    _setIsAllowedIfAllowed();
                    king.dad.networkSubProxy
                        .getAndLoadNetworkSubIdsForInstanceId();
                  },
                )
              : const SizedBox.shrink(),

          IconButton(
            icon: const Icon(Icons.help_outline, color: Colors.white),
            onPressed: () {
              if (name != Routes.help) {
                Navigator.of(context).pushNamed(Routes.help);
              }
            },
          ),
          IconButton(
            icon: const Icon(Icons.library_add, color: Colors.white),
            onPressed: () {
              if (name != Routes.networkSubAdd) {
                Navigator.of(context).pushNamed(Routes.networkSubAdd);
              }
            },
          ),
          IconButton(
            icon: const Icon(Icons.person, color: Colors.white),
            onPressed: () {
              Navigator.of(context).pushNamed(Routes.account);
            },
          ),
        ],
      ),
    );
  }
}

class MobileSliverScaffold extends StatefulWidget {
  const MobileSliverScaffold({
    this.padding = const EdgeInsets.all(0),
    this.slivers = const <Widget>[],
    this.sliverGlue = const SliverToBoxAdapter(child: SizedBox.shrink()),
  });
  final EdgeInsets padding;
  final List<Widget> slivers;
  final Widget sliverGlue;

  @override
  MobileSliverScaffoldState createState() => MobileSliverScaffoldState();
}

class MobileSliverScaffoldState extends State<MobileSliverScaffold> {
  bool _isAwesomeNotificationsAllowed = false;

  @override
  initState() {
    super.initState();
    _setIsAllowedIfAllowed();
  }

  _setIsAllowedIfAllowed() {
    AwesomeNotifications().isNotificationAllowed().then((isAllowed) {
      setState(() {
        _isAwesomeNotificationsAllowed = isAllowed;
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    final name = ModalRoute.of(context)?.settings.name;
    final king = King.of(context);

    return Scaffold(
      resizeToAvoidBottomInset: true,
      body: Stack(
        children: <Widget>[
          CustomScrollView(
            shrinkWrap: true,
            slivers: <Widget>[
              SliverAppBar(
                automaticallyImplyLeading: true,
                title: const Text('PushBoi'),
                actions: <Widget>[
                  _isAwesomeNotificationsAllowed
                      ? const SizedBox.shrink()
                      : IconButton(
                          icon: const Icon(Icons.warning, color: Colors.red),
                          onPressed: () {
                            var name = ModalRoute.of(context)?.settings.name;
                            if (name != Routes.warnings) {
                              Navigator.of(context).pushNamed(Routes.warnings);
                            }
                          },
                        ),
                  //

                  name == Routes.home
                      ? IconButton(
                          icon: const Icon(Icons.refresh, color: Colors.white),
                          onPressed: () {
                            _setIsAllowedIfAllowed();
                            king.dad.networkSubProxy
                                .getAndLoadNetworkSubIdsForInstanceId();
                          },
                        )
                      : const SizedBox.shrink(),

                  IconButton(
                    icon: const Icon(Icons.help_outline, color: Colors.white),
                    onPressed: () {
                      if (name != Routes.help) {
                        Navigator.of(context).pushNamed(Routes.help);
                      }
                    },
                  ),
                  IconButton(
                    icon: const Icon(Icons.library_add, color: Colors.white),
                    onPressed: () {
                      if (name != Routes.networkSubAdd) {
                        Navigator.of(context).pushNamed(Routes.networkSubAdd);
                      }
                    },
                  ),
                  IconButton(
                    icon: const Icon(Icons.person, color: Colors.white),
                    onPressed: () {
                      Navigator.of(context).pushNamed(Routes.account);
                    },
                  ),
                ],
              ),
              //

              ...widget.slivers,
              widget.sliverGlue,
            ],
          ),
          BasedSnackBar(),
        ],
      ),
    );
  }
}
