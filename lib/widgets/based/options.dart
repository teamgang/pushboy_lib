import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class OptionCluster extends StatelessWidget {
  const OptionCluster({required this.title, required this.children});
  final List<Widget> children;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Material(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.stretch,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.fromLTRB(16, 16, 16, 4),
            child:
                Text(this.title, style: const TextStyle(color: Colors.green)),
          ),
          ...this.children,
        ],
      ),
    );
  }
}

class OptionRow extends StatelessWidget {
  const OptionRow(
      {required this.option, required this.value, required this.onTap});
  final Function onTap;
  final String option;
  final String value;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        this.onTap();
      },
      child: Padding(
        padding: const EdgeInsets.fromLTRB(16, 14, 16, 14),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text(this.value, style: Theme.of(context).textTheme.bodyText2),
            const SizedBox(height: 4),
            Text(this.option, style: Theme.of(context).textTheme.bodyText1),
          ],
        ),
      ),
    );
  }
}

class OptionRowStatic extends StatelessWidget {
  const OptionRowStatic({required this.option, required this.onTap});
  final Function onTap;
  final String option;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        this.onTap();
      },
      child: Padding(
        padding: const EdgeInsets.fromLTRB(16, 18, 16, 18),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Text18(this.option),
          ],
        ),
      ),
    );
  }
}

class OptionRowIcon extends StatelessWidget {
  const OptionRowIcon(
      {required this.icon,
      required this.onTap,
      required this.option,
      this.tapKey});
  final IconData icon;
  final Key? tapKey;
  final Function onTap;
  final String option;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      key: this.tapKey,
      onTap: () {
        this.onTap();
      },
      child: Padding(
        padding: const EdgeInsets.fromLTRB(16, 14, 16, 14),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            const SizedBox(height: 4),
            Row(
              children: <Widget>[
                Icon(icon),
                const SizedBox(width: 12),
                Text(this.option, style: Theme.of(context).textTheme.bodyText1),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
