import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class BasedPlaceholderPage extends StatelessWidget {
  const BasedPlaceholderPage({this.text = 'based placeholder page'});
  final String text;

  @override
  Widget build(BuildContext context) {
    return WebScaffold(body: Text(this.text));
  }
}
