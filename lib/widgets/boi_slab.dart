import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';
import 'package:url_launcher/url_launcher.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class BoiSlab extends StatelessWidget {
  const BoiSlab({
    this.index,
    required this.boi,
    this.onTap,
  });
  final int? index;
  final Boi boi;
  final Function? onTap;

  void _launchUrl() async {
    if (!await launchUrl(Uri.parse(boi.url))) {
      throw Exception('Could not launch staticUrl ${boi.url}');
    }
  }

  @override
  Widget build(BuildContext context) {
    switch (boi.layout) {
      case BoiLayout.none:
        return const FailureMsg('No boi layout selected.');

      case BoiLayout.default_:
        return Card(
          elevation: 4,
          child: InkWell(
            onTap: onTap == null ? () => _launchUrl() : () => onTap!(),
            child: Observer(
              builder: (_) => Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  //BasedCircleImage(
                  //altText: boi.channelAbbrev,
                  //colorBackground: boi.colorPrimary,
                  //colorText: boi.colorSecondary,
                  //radius: 30,
                  //src: boi.thumbActive,
                  //),
                  Column(
                    children: <Widget>[
                      BasedFadeInImage(
                        altText: boi.channelAbbrev,
                        colorBackground: boi.colorPrimary,
                        colorText: boi.colorSecondary,
                        height: 80,
                        width: 80,
                        src: boi.thumbSelectedUrl,
                      ),
                    ],
                  ),
                  const SizedBox(width: 4),
                  //

                  Expanded(
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text20(
                          boi.title,
                          maxLines: 2,
                        ),
                        const SizedBox(height: 2),
                        Text14(
                          boi.channelName,
                          maxLines: 2,
                        ),
                        const SizedBox(height: 4),
                        Text14(
                          boi.body,
                          maxLines: 8,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );
      //

      case BoiLayout.bigText:
        return Card(
          elevation: 4,
          child: InkWell(
            onTap: onTap == null ? () => _launchUrl() : () => onTap!(),
            child: Observer(
              builder: (_) => Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Column(
                    children: <Widget>[
                      BasedFadeInImage(
                        altText: boi.channelAbbrev,
                        colorBackground: boi.colorPrimary,
                        colorText: boi.colorSecondary,
                        height: 80,
                        width: 80,
                        src: boi.thumbSelectedUrl,
                      ),
                    ],
                  ),
                  const SizedBox(width: 4),
                  //

                  Expanded(
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Text20(
                          boi.title,
                          maxLines: 2,
                        ),
                        const SizedBox(height: 2),
                        Text14(
                          boi.channelName,
                          maxLines: 2,
                        ),
                        const SizedBox(height: 4),
                        Text14(
                          boi.body,
                          maxLines: 8,
                        ),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
        );

      case BoiLayout.bigPicture:
        return Card(
          elevation: 4,
          child: InkWell(
            onTap: onTap == null ? () => _launchUrl() : () => onTap!(),
            child: Observer(
              builder: (_) => SizedBox(
                height: 300,
                child: Column(
                  //crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Expanded(
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text20(
                            boi.title,
                            maxLines: 2,
                          ),
                          const SizedBox(height: 2),
                          Text14(
                            boi.channelName,
                            maxLines: 2,
                          ),
                          const SizedBox(height: 4),
                          Text14(
                            boi.body,
                            maxLines: 8,
                          ),
                        ],
                      ),
                    ),
                    //

                    BasedFadeInImage(
                      altText: boi.channelAbbrev,
                      colorBackground: boi.colorPrimary,
                      colorText: boi.colorSecondary,
                      height: 200,
                      width: 200,
                      src: boi.thumbSelectedUrl,
                    ),
                    const SizedBox(width: 4),
                    const Text16('ngl this isn\'t accurate'),
                  ],
                ),
              ),
            ),
          ),
        );

      default:
        return Card(
          elevation: 4,
          child: InkWell(
            onTap: onTap == null ? () => _launchUrl() : () => onTap!(),
            child: const FailureMsg('Missing layout information.'),
          ),
        );
    }
  }
}

class StaticBoiSlab extends StatelessWidget {
  const StaticBoiSlab({required this.channel});
  final Channel channel;

  @override
  Widget build(BuildContext context) {
    return Observer(builder: (_) => BoiSlab(boi: channel.tempStaticBoi));
  }
}

class BoiSenderSlab extends StatelessWidget {
  const BoiSenderSlab({required this.boiSender});
  final BoiSender boiSender;

  @override
  Widget build(BuildContext context) {
    return Observer(builder: (_) => BoiSlab(boi: boiSender.tempStaticBoi));
  }
}
