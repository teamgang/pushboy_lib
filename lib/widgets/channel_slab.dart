import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class ChannelSlab extends StatelessWidget {
  const ChannelSlab({
    this.index,
    required this.channel,
    this.onTap,
  });
  final int? index;
  final Channel channel;
  final Function? onTap;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      child: InkWell(
        onTap: onTap != null
            ? () => onTap!()
            : () {
                Navigator.of(context).pushNamed(Routes.channelOverview,
                    arguments: Args(channel: channel));
              },
        child: Padding(
          padding: const EdgeInsets.fromLTRB(6, 12, 10, 12),
          child: Column(children: <Widget>[
            SizedBox(
              height: 60,
              child: Observer(
                builder: (_) => Row(children: <Widget>[
                  BasedCircleImage(
                    altText: channel.abbrev,
                    colorBackground: channel.colorPrimary,
                    colorText: channel.colorSecondary,
                    radius: 30,
                    src: channel.thumbSelectedUrl,
                  ),
                  const SizedBox(width: 4),
                  Expanded(
                    child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          Text(
                            channel.name,
                            overflow: TextOverflow.ellipsis,
                            style: const TextStyle(fontSize: 20),
                          ),
                          const SizedBox(height: 4),
                          Text(
                            channel.description,
                            overflow: TextOverflow.ellipsis,
                            maxLines: 2,
                          ),
                        ]),
                  ),
                ]),
              ),
            ),
            Observer(builder: (_) {
              final channelSub = King.of(context)
                  .dad
                  .channelSubProxy
                  .getChannelSubRef(channelId: channel.channelId);
              return channelSub.isUnsubscribed
                  ? Text('Not subscribed', style: Bast.failure)
                  : const SizedBox.shrink();
            }),
          ]),
        ),
      ),
    );
  }
}
