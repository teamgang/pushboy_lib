import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class ChannelSubSlab extends StatelessWidget {
  const ChannelSubSlab({
    required this.index,
    required this.channelSub,
  });
  final int index;
  final ChannelSub channelSub;

  @override
  Widget build(BuildContext context) {
    return ChannelSlab(
      index: index,
      channel: channelSub.channel,
      onTap: () {
        Navigator.of(context).pushNamed(Routes.channelSubOverview,
            arguments: Args(channelSub: channelSub));
      },
    );
  }
}
