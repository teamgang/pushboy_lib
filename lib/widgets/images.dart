import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class DefaultChannelThumbnail extends StatelessWidget {
  const DefaultChannelThumbnail({
    this.abbrev,
    this.channel,
    this.colorBackground,
    this.colorText,
    this.fontSize = 20,
    this.saveKey,
    this.height = 80,
    this.width = 80,
  });
  final String? abbrev;
  final Channel? channel;
  final Color? colorBackground;
  final Color? colorText;
  final double fontSize;
  final GlobalKey? saveKey;
  final double height;
  final double width;

  @override
  Widget build(BuildContext context) {
    String abbrevOut = channel?.abbrev ?? abbrev ?? '';
    Color colorBackgroundOut =
        channel?.colorPrimary ?? colorBackground ?? Colors.blue;
    Color colorTextOut = channel?.colorSecondary ?? colorText ?? Colors.amber;

    return RepaintBoundary(
      key: saveKey ?? GlobalKey(),
      child: SizedBox(
        height: height,
        width: width,
        child: BasedFadeInImage(
          altText: abbrevOut,
          colorBackground: colorBackgroundOut,
          colorText: colorTextOut,
          fontSize: fontSize,
          height: height,
          width: width,
          src: '',
        ),
      ),
    );
  }
}
