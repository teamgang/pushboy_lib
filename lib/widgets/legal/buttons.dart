import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class AcceptTermsButton extends StatelessWidget {
  const AcceptTermsButton();

  @override
  Widget build(BuildContext context) {
    return Row(children: <Widget>[
      const Text('I agree to the '),
      TextButton(
        child: const Text('Terms & Conditions.'),
        onPressed: () => Navigator.of(context).pushNamed(Routes.developerTerms),
      ),
    ]);
  }
}
