import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class MobilePolicyPage extends StatefulWidget {
  const MobilePolicyPage({required this.audience, required this.policy});
  final String audience;
  final String policy;

  @override
  MobilePolicyPageState createState() => MobilePolicyPageState();
}

class MobilePolicyPageState extends State<MobilePolicyPage> {
  final legalDoc = LegalDoc();

  @override
  initState() {
    super.initState();
    String audience = widget.audience;
    if (audience == 'surfer') {
      audience = 'user';
    }
    legalDoc.loadLatestFromApi(context, audience, widget.policy);
  }

  @override
  Widget build(BuildContext context) {
    return MobileScaffold(
      body: Observer(
        builder: (_) => Markdown(data: legalDoc.fulltext),
      ),
    );
  }
}
