import 'package:flutter/material.dart';
import 'package:flutter_markdown/flutter_markdown.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class WebPolicyPage extends StatefulWidget {
  const WebPolicyPage({required this.audience, required this.policy});
  final String audience;
  final String policy;

  @override
  WebPolicyPageState createState() => WebPolicyPageState();
}

class WebPolicyPageState extends State<WebPolicyPage> {
  final legalDoc = LegalDoc();

  @override
  initState() {
    super.initState();
    var audience = widget.audience;
    if (audience == 'surfer') {
      audience = 'user';
    }
    legalDoc.loadLatestFromApi(context, audience, widget.policy);
  }

  @override
  Widget build(BuildContext context) {
    return WebScaffold(
      body: Observer(
        builder: (_) => Markdown(data: legalDoc.fulltext),
      ),
    );
  }
}
