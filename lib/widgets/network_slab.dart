import 'package:flutter/material.dart';
import 'package:flutter_mobx/flutter_mobx.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class NetworkSlab extends StatelessWidget {
  const NetworkSlab({
    this.index,
    required this.network,
    this.networkSub,
    this.onTap,
  });
  final int? index;
  final Network network;
  final NetworkSub? networkSub;
  final Function? onTap;

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 2,
      child: InkWell(
        onTap: onTap != null
            ? () => onTap!()
            : () {
                Navigator.of(context).pushNamed(Routes.networkOverview,
                    arguments: Args(network: network));
              },
        child: Padding(
          padding: const EdgeInsets.fromLTRB(6, 12, 10, 12),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              SizedBox(
                height: 60,
                child: Observer(
                  builder: (_) => Row(children: <Widget>[
                    BasedCircleImage(
                      altText: network.abbrev,
                      colorBackground: network.colorPrimary,
                      colorText: network.colorSecondary,
                      radius: 30,
                      src: network.thumbSelectedUrl,
                    ),
                    const SizedBox(width: 4),
                    Expanded(
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Text(
                              network.name,
                              overflow: TextOverflow.ellipsis,
                              style: const TextStyle(fontSize: 20),
                            ),
                            const SizedBox(height: 4),
                            Text(
                              network.description,
                              overflow: TextOverflow.ellipsis,
                              maxLines: 2,
                            ),
                          ]),
                    ),
                  ]),
                ),
              ),
              Observer(builder: (_) {
                return networkSub?.isLinkedToSurfer ?? false
                    ? const SizedBox.shrink()
                    : Padding(
                        padding: const EdgeInsets.fromLTRB(0, 8, 0, 0),
                        child: Text('Anonymous subscription',
                            style: Bast.greyItalic),
                      );
              }),
            ],
          ),
        ),
      ),
    );
  }
}
