import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class NetworkSubSlab extends StatelessWidget {
  const NetworkSubSlab({
    this.index,
    required this.networkSub,
  });
  final int? index;
  final NetworkSub networkSub;

  @override
  Widget build(BuildContext context) {
    return NetworkSlab(
        index: index,
        network: networkSub.network,
        networkSub: networkSub,
        onTap: () {
          Navigator.of(context).pushNamed(Routes.networkSubOverview,
              arguments: Args(networkSub: networkSub));
        });
  }
}
