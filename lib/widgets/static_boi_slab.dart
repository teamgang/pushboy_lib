import 'package:flutter/material.dart';

import 'package:pushboy_lib/pushboy_lib.dart';

class StaticBoiSlab extends StatelessWidget {
  const StaticBoiSlab({required this.channel});
  final Channel channel;

  @override
  Widget build(BuildContext context) {
    return BoiSlab(boi: channel.tempStaticBoi);
  }
}
